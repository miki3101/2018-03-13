package �bungen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectInDateiUndWiederAuslesen
{

	// Object einlesen - in Datei schreiben - aus Datei auslesen
	
	public static void main(String[] args)
	{

		try (
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("object.dat"));
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream("object.dat"));)
		{

			String text = "Hallo Datei, dies ist ein Text!";
			
			oos.writeObject(text);
			oos.flush();
			
			System.out.println(ois.readObject());
			
			
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}

}
