package �bungen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;

public class VonConsoleInCSVDateiSchreiben
{
	// von Console einlesen und in csv.Datei schreiben bis STOP

	public static void main(String[] args)
	{
		
		try(
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				Writer out = new FileWriter("noten.csv");
				BufferedWriter bw = new BufferedWriter(out);	
				)
		{
			String line;
			
			while((line = br.readLine()) != null)
			{
				if(line.equals("STOP"))
				{
					return;
				}
				
				writeLine(line, out);
			
			}	
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}

	// Methode zum richtigen einlesen und umwandeln zum Schreiben in die csv - Datei
	private static void writeLine(String line, Writer bw) throws IOException
	{
		for (String string : line.split(" "))
		{
			if(string.length() == 0)
			{
				continue;	// steigt aus der Schleife aus
			}
			if (string.charAt(string.length()-1) == ':')
			{
				bw.write(string.substring(0, string.length()-1));
			} else
			{
				bw.write(string);
			}
			bw.write("; ");
		}
		bw.write("\r\n");
		
	}

}
