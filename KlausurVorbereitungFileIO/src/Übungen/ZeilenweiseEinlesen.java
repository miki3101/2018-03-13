package �bungen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ZeilenweiseEinlesen
{

	// Zeilenweise Tastatureingabe einlesen und auf Konsole ausgeben bis "Stop"
	
	public static void main(String[] args)
	{	
		String line;
		
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in));)
		{
			
			while ((line = br.readLine()) != null)
			{
				if(line.equals("STOP"))
				{
					return;
				}
				System.out.println(line);
				
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		

	}

}
