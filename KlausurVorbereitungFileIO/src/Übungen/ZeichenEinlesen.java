package �bungen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ZeichenEinlesen
{
	
	// von Konsole einlesen und auf Konsole ausgeben bis x eingelesen wird

	public static void main(String[] args)
	{
			
		String line;
		
		try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in));)
		{
			while ((line = br.readLine()) != null)
			{
				if(line.equals("x"))
				{
					return;
				}
				System.out.println(line);
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
	}

}
