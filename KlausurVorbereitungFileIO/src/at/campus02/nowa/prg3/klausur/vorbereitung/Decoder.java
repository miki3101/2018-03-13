package at.campus02.nowa.prg3.klausur.vorbereitung;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Decoder
{

	public static void main(String[] args)
	{

		try (InputStream is = new ByteInverterInputStream(new FileInputStream("secret.dat"));
				OutputStream out = new FileOutputStream("nachricht.txt");

		)
		{

			int b;
			while((b = is.read()) != -1)
			{
				out.write(b);
				out.flush();
			}

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
