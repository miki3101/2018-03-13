package at.campus02.nowa.prg3.klausur.fileio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ASCIIInputStream extends FilterInputStream
{

	protected ASCIIInputStream(InputStream arg0)
	{
		super(arg0);
	}
	
	
	public int read() throws IOException
	{
		int b;
		
		while((b = super.read()) != -1)
		{
			if (b > 127)
			{
				continue;
			}
			return b;
		}
		return b;
	}

}
