package edu.campus02.automat;

import java.util.ArrayList;

public class State
{

	private String name;
	private boolean finalState = false;

	private ArrayList<Transition> transitions = new ArrayList<Transition>();

	public State(String name)
	{
		this.name = name;
	}

	public State(String name, boolean fina)
	{
		this.name = name;
		this.finalState = fina;
	}

	public boolean isFinal()
	{
		return finalState;
	}

	public void addTransition(Transition tran)
	{
		transitions.add(tran);
	}

	public Transition findMatchingTransition(char c)
	{
		for (Transition transition : transitions)
		{
			if (transition.match(c))
				return transition;
		}

		return null;
	}

	public String toString()
	{
		if (transitions.isEmpty())
		{
			return String.format("(%s,%b)", name, finalState);
		} else
		{
			String tran = "";
			for (Transition transition : transitions)
			{
				tran += transition.toString();
			}
			return String.format("(%s,%b,[%s])", name, finalState, tran);

		}
	}

}
