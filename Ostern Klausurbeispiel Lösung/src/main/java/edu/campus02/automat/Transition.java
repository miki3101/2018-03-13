package edu.campus02.automat;

import java.lang.annotation.Target;

public class Transition
{

	private State nextState;
	private String trigger;

	public Transition(String trigger, State target)
	{
		this.trigger = trigger;
		this.nextState = target;
	}

	public State getTarget()
	{
		return nextState;
	}

	public boolean match(char c)
	{
		return trigger.contains(String.valueOf(c));
	}

	public String toString()
	{
		return String.format("[%s]->%s", trigger, nextState);
	}

}
