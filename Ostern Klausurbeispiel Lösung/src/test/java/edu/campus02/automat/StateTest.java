package edu.campus02.automat;

import org.junit.Test;
import static org.junit.Assert.*;

public class StateTest {
	@Test
	public void testConctructor01() {
		State s1 = new State("s1");

		assertEquals("(s1,false)", s1.toString());
	}

	@Test
	public void testConctructor02() {
		State s1 = new State("s1", true);

		assertEquals("(s1,true)", s1.toString());
	}
	
	
	@Test
	public void testAddToString() {
		State s1 = new State("s1", false);
		s1.addTransition(new Transition("c",null));

		assertEquals("(s1,false,[[c]->null])", s1.toString());
	}
	
	@Test
	public void testAddMatch() {
		State s1 = new State("s1", false);
		s1.addTransition(new Transition("c",null));
		s1.addTransition(new Transition("a",null));

		assertEquals("[a]->null", s1.findMatchingTransition('a').toString());
	}
	
	@Test
	public void testAddMatchNull() {
		State s1 = new State("s1", false);
		s1.addTransition(new Transition("c",null));
		s1.addTransition(new Transition("a",null));

		assertEquals(null, s1.findMatchingTransition('x'));
	}
}
