package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo
{

	public static void main(String[] args)
	{
		try(ByteCounterInputStream bcis = new ByteCounterInputStream(new FileInputStream("test.dat"), ' ');)
		{
			int b;
		
			while ((b = bcis.read()) != -1)
			{
			}
			
			System.out.println(bcis.getCount());
			
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
		
	}

}
