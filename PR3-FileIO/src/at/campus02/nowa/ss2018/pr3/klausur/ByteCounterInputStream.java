package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteCounterInputStream extends FilterInputStream
{

	private int counter = 0;
	private char value;
	
	protected ByteCounterInputStream(InputStream in, char value)
	{
		super(in);
		this.value = value;
	}
	
	public int read() throws IOException
	{
		int b = super.read();
		
		if (b == -1)
		{
			return b;
		}
		
		if(b == value)
		{
			counter++;
		}
		
		return b;
	}
	
	
	public int getCount()
	{
		return counter;
	}

}

