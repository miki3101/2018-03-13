package at.campus02.nowa.ss2018.pr3.klausur;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;

class Test {

	private FileInputStream fis;

	@BeforeEach
	void setUp() throws Exception {
		fis = new FileInputStream("test.dat");
	}
	
	private void readAllBytes(InputStream is) throws IOException {
		@SuppressWarnings("unused")
		int b;
		while ((b = is.read()) != -1) {
			// Do nothing
		}
	}

	@org.junit.jupiter.api.Test
	void testCharA() {
		try (ByteCounterInputStream bcis = new ByteCounterInputStream(fis, 'a');) {
			readAllBytes(bcis);
			assertEquals(32, bcis.getCount());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@org.junit.jupiter.api.Test
	void testCharNewline() {
		try (ByteCounterInputStream bcis = new ByteCounterInputStream(fis, '\n');) {
			readAllBytes(bcis);
			assertEquals(10, bcis.getCount());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@org.junit.jupiter.api.Test
	void testCharUmlautU() {
		try (ByteCounterInputStream bcis = new ByteCounterInputStream(fis, '�');) {
			readAllBytes(bcis);
			assertEquals(9, bcis.getCount());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@org.junit.jupiter.api.Test
	void testCharComma() {
		try (ByteCounterInputStream bcis = new ByteCounterInputStream(fis, ',');) {
			readAllBytes(bcis);
			assertEquals(7, bcis.getCount());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@org.junit.jupiter.api.Test
	void testCharAsterisk() {
		try (ByteCounterInputStream bcis = new ByteCounterInputStream(fis, '*');) {
			readAllBytes(bcis);
			assertEquals(0, bcis.getCount());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
