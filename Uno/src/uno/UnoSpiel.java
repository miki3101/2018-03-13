package uno;

import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel
{
	// Ablege Stapel und Karten Stapel erstellt
	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>(); 	// verwaltet den Ablagestapel
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();		// verwaltet den Kartenstapel
	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();	// verwaltet die Spieler

	// Konstruktor
	public UnoSpiel()
	{
		// wir haben 2 leere Kartenstapel und die bef�llen man jetzt

		for (int index = 0; index < 2; index++) // damit ich von jeder Karte 2 St�ck habe
		{
			for (int zahlenwert = 0; zahlenwert < 10; zahlenwert++) // damit er das 10 rote, blaue, gr�ne und gelbe
																	// hinzuf�gt
			{
				kartenStapel.add(new Karte(Farbe.rot, zahlenwert));		// f�gt eine rote Karte mit einem best. Zahlenwert hinzu
				kartenStapel.add(new Karte(Farbe.blau, zahlenwert));	// f�gt eine blaue Karte mit einem best. Zahlenwert hinzu
				kartenStapel.add(new Karte(Farbe.gruen, zahlenwert));	// f�gt eine gr�ne Karte mit einem best. Zahlenwert hinzu
				kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));	// f�gt eine gelbe Karte mit einem best. Zahlenwert hinzu
			}
		}

		Collections.shuffle(kartenStapel); // mischt die Karten (statisch weil, ich kein Objekt daf�r brauche)
	}

	public Karte abheben() // liefert eine Karte zur�ck
	{

		
		if (kartenStapel.size() == 0)
		{
			Karte ablagestapelKarte = ablageStapel.remove(0); // die letzte karte merken (wegnehmen)
			
			Collections.shuffle(ablageStapel); // restlichen Stapel mischen
			
			kartenStapel.addAll(ablageStapel); // alle vom ablagestapel in den kartenstapel einf�gen (zurzeit w�ren alle karten in beiden stapeln)
			
			ablageStapel.clear();			     // Liste ist leeren und
			ablageStapel.add(ablagestapelKarte); // oberste karte wieder drau legen (gemerkte karte)
			
		}

		return kartenStapel.remove(0); // hebe oberste karte vom kartenstapel ab
	}
	
	public void karteAblegen(Karte ablegeKarte)
	{
		ablageStapel.add (0, ablegeKarte); // Karte an der Stelle 0 einf�gen
	}
	

	public void mitspielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler); // mit dieser Methode f�gt man einen neuen Spieler, der Liste Mitspieler hinzu
	}

	public void austeilen()
	{
		for (int index = 0; index < 7; index++) // jeder Spieler zieht 7 mal eine Karte
		{
			for (Spieler sp : mitspieler) // jeder Spieler kommt einmal dran
			{
				Karte karte = abheben(); // ich sage das der Spieler eine Karte abhebt
				sp.aufnehmen(karte); // hab ich in der Klasse Spieler definiert / Spieler nimmt karte in die Hand
			}
		}
		Karte temp = abheben(); // ich hebe noch eine Karte ab
		ablageStapel.add(temp); // ich lege eine Karte auf den Ablagestapel
		
		System.out.println("Erste Karte ist: " + temp); // gibt die erste Karte vom AblageStapel aus
	}
	
	public boolean spielzug()
	{//wer ist dran
		Spieler aktuellerSpieler = mitspieler.remove(0); // man nimmt einen Spieler aus der Liste heraus (der gerade dran ist)
		System.out.println("am Zug ist: " + aktuellerSpieler);
		
		Karte gefundeneKarte = aktuellerSpieler.passendeKarte(ablageStapel.get(0)); // aktuelle Spieler vergleicht ob er eine passende Karte zum ablage Stapel (an stelle 0) hat
		
		if (gefundeneKarte !=null) 										// wenn die gefundene Karte nicht gleich null ergibt (also passt)
		{
			System.out.println("Karte ablegen: "+ gefundeneKarte);
			karteAblegen(gefundeneKarte);								// lege ich die Karte ab
		}else
		{
			Karte abgehobeneKarte = abheben();							// 
			
			if (abgehobeneKarte.match(ablageStapel.get(0)))				// wenn die abgehobene karte zum ablagestapel passt
			{
				karteAblegen(abgehobeneKarte);							// leg sie am Stapel
				System.out.println("Karte passt und ablegen:"+ abgehobeneKarte);
			}
			else
			{
				System.out.println("Karte aufnehmen");				// ansonsten tu sie in die handkarten
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
			}
			
		}
		
		if (aktuellerSpieler.anzahlDerHandkarten()==0)			// wenn der aktuelle Spieler keine Handkarten mehr hat
		{
			System.out.println("Gewonnen hat: "+ aktuellerSpieler);		// Spiel aus
			return false;
		}
		// Am Ende vom Spielzug
		
		mitspieler.add(aktuellerSpieler); 	// man f�gt den Spieler (Objekt) der gerade dran war, wieder in die Liste Spieler an letzter Stelle ein
		
		return true;
	}
	
	
}
