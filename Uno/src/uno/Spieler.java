package uno;

import java.util.ArrayList; // muss importiert werden um die ArrayList verwenden zu k�nnen

public class Spieler
{
	private String name;
	private ArrayList<Karte> handkarten = new ArrayList<Karte>(); //seine Handkarten werden als Liste verwaltet
	
	public Spieler(String name) // ich kann dem Spieler einen Namen geben
	{
		this.name = name; // ich kann die variable "name" auch woanders verwenden
	}
	
	public void aufnehmen(Karte karte)
	{
		handkarten.add(karte); // Spieler nimmt Karte auf
	}
	
	public String toString()
	{
		return name + " " + handkarten; // zeigt mir den Namen des Spielers und seine Handkarten
	}
	
	public Karte passendeKarte(Karte vergleich)
	{
		for (Karte karte : handkarten) // geh alle meine Handkarten durch
		{
			if(karte.match(vergleich)) // wenn die abgehobene Karte gleich ist wie die Karte
			{
				handkarten.remove(karte); // legt die karte wieder weg aus seinen Handkarten
				
				if(handkarten.size()==1)
				{
					System.out.println("UNO");
				}
				
				return karte;
			}
		}
		
		return null;
	}
	
	public int anzahlDerHandkarten()
	{
		return handkarten.size();			// liefert die Anzahl der Karten in der Liste Handkarten (size = wieviel Eintr�ge in der Liste sind)
	}										// w�r das gleiche wie int result = handkarten.size(); return result;
	
}
