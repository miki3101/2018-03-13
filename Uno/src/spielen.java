

import uno.Farbe;
import uno.Karte;

public class spielen
{
	public static void main(String[] args)
	{
		Karte Karte1 = new Karte(Farbe.blau, 4); // neue karte erstellt ("Farbe", Zahl)
		Karte Karte2 = new Karte(Farbe.gelb, 2);
		Karte Karte3 = new Karte(Farbe.blau, 5);
		Karte Karte4 = new Karte(Farbe.rot, 3);

		System.out.println(Karte1); // gib mir den Wert der ersten Karte aus
		System.out.println(Karte1.match(Karte3)); // wende die Methode match zwischen Karte 1 und Karte 2 an
		System.out.println(Karte2.match(Karte2));
		System.out.println(Karte1.match(Karte4));

		System.out.println(Karte1.compare(Karte2)); // farbe ist nicht gleich, wird reihenfolge der Farbe wird
													// ausgegeben
		System.out.println(Karte1.compare(Karte3)); // farbe ist gleich, reihenfolge der zahl wird ausgegeben
	}
}
