

import uno.Farbe;
import uno.Karte;
import uno.Spieler;
import uno.UnoSpiel;

public class UnoApp
{
	public static void main(String[] args)
	{
		UnoSpiel meinSpiel = new UnoSpiel(); // ich mache ein neues UnoSpiel
		
		Karte abgehobeneKarte = meinSpiel.abheben(); //in diesem Spiel verwende ich die Methode abheben
		
		Spieler sp1 = new Spieler("Mama"); // f�ge neuen Spieler hinzu
		Spieler sp2 = new Spieler("Alex"); 
		Spieler sp3 = new Spieler("Schlagi");
		
		meinSpiel.mitspielen(sp1); // ich f�ge die Spieler dem Spiel hinzu (dieser Spieler spielt mit)
		meinSpiel.mitspielen(sp2);
		meinSpiel.mitspielen(sp3);
		
		meinSpiel.austeilen(); // jeder Spieler erh�lt 7 Karten vom Stapel
		
//		System.out.println(sp1.passendeKarte(new Karte(Farbe.rot, 1))); // man gibt dem neuen Spieler eine Karte und schaut ob sie passt
																		// wenn er eine Handkarte hat, die passt, gibt er sie aus
																		// wenn nicht, gibt er den Wert null aus
		
		
		
		int z�hler = 0;
		while (meinSpiel.spielzug())					// spielt so lange, bis es keinen Spielzug mehr gibt
		{
			
			z�hler++;									// z�hlt die Spielz�ge mit
		}
		System.out.println("Das Spiel dauerte: " + z�hler+ " Z�ge");
	}

}
