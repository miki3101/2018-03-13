package Warenkorb;

import java.util.Comparator;

public class CartTotalItems implements Comparator<Cart>
{

	public int compare(Cart c1, Cart c2)
	{
		if(c1.getTotalItems() < c2.getTotalItems())
			return -1;
		if(c1.getTotalItems() > c2.getTotalItems())
			return 1;
		
		return 0;
	}

}
