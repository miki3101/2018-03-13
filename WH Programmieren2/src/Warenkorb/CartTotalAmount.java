package Warenkorb;

public abstract class CartTotalAmount implements Comparable<Cart>
{

	public double sortTotalAmount(Cart c1, Cart c2)
	{	
		
			if(c1.getTotalAmount() < c2.getTotalAmount())
				return -1;
			if(c1.getTotalAmount() > c2.getTotalAmount())
				return 1;
			return 0;
		
		
	}

}
