package Warenkorb;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoApp
{

	public static void main(String[] args)
	{
		List<Cart> demo = new ArrayList<>();
		
		Cart c1 = new Cart("Franz", 33, 270, 1400.00);
		Cart c2 = new Cart("Alex", 21, 100, 1200.00);
		Cart c3 = new Cart("Max", 10, 820, 5400.00);
		Cart c4 = new Cart("Susi", 56, 600, 8400.00);
		
		demo.add(c1);
		demo.add(c2);
		demo.add(c3);
		demo.add(c4);
		
		System.out.println("Ohne Sortierung:");
		for (Cart cart : demo)
		{
			System.out.println(cart);
		}
		
		Collections.sort(demo);
		System.out.println("----------\n");
		System.out.println("Sortiert nach Total Amount");
		
		for (Cart cart : demo)
		{
			System.out.println(cart);
		}
		
		Collections.sort(demo, new CartUsername());
		System.out.println("----------\n");
		System.out.println("Sortiert nach Username: ");
		
		for (Cart cart : demo)
		{
			System.out.println(cart);
		}
		
		Collections.sort(demo, new CartTotalItems());
		System.out.println("----------\n");
		System.out.println("Sortiert nach Total Items: ");
		
		for (Cart cart : demo)
		{
			System.out.println(cart);
		}
	}
	
	public void anonymeComparator(ArrayList<Cart> demo)
	{
		double d = 0;
		double erste = 0;
		
		for (Cart cart : demo)
		{
		
			d = cart.getTotalAmount() / cart.getTotalItems();
			
		}
	}

}
