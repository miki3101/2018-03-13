package Konto;

public class Konto
{
	String inhaber;
	double kontostand = 0;
	
	public Konto(String inhaber, double kontostand)
	{
		this.inhaber = inhaber;
		this.kontostand = kontostand;
	}
	
	public void einzahlen(double wert)
	{
		System.out.println(inhaber + ", hat" + wert + " eingezahlt");
		kontostand += wert;
	}
	
	public void auszahlen(double wert)
	{
		System.out.println(inhaber + ", hat " + wert + " abgehoben");
		kontostand -= wert;
	}
	
	public String toString()
	{
		return inhaber + ", Konto, aktueller Kontostand: " + kontostand; 
	}
}
