package Konto;

public class JugendGiroKonto extends Girokonto
{
	private double buchungslimit;

	public JugendGiroKonto(String inhaber, double kontostand, double limit, double buchungslimit)
	{
		super(inhaber, kontostand, limit);
		this.buchungslimit = buchungslimit;
	}

	public void einzahlen(double wert)
	{
		super.einzahlen(wert);
	}

	public void auszahlen(double wert)
	{
		if(wert<buchungslimit && wert<limit)
		super.auszahlen(wert);
		else {
			System.out.println("Es tut mir leid " + inhaber + ", " + wert + " übersteigt dein Limit, max. Betrag " + buchungslimit + ", dein Kontostand bleibt unverändert");
		}
	}

	public String toString()
	{
		return inhaber + ", JugendGiroKonto, aktueller Kontostand: " + kontostand;
	}
		
}
