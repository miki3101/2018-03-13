package Konto;

public class Sparkonto extends Konto
{

	public Sparkonto(String inhaber, double kontostand)
	{
		super(inhaber, kontostand);
	}

	
	public void einzahlen(double wert)
	{
		super.einzahlen(wert);
	}

	public void auszahlen(double wert)
	{
		if(kontostand-wert > 0)
		super.auszahlen(wert);
		else {
			System.out.println("Es tut mir leid " + inhaber + ", du hast nur " + kontostand + " zur Verf�gung");
		}
	}


	@Override
	public String toString()
	{
		return inhaber + ", Sparkonto, aktueller Kontostand: " + kontostand;
	}
	
	
}
