package Konto;

public class Girokonto extends Konto
{
	protected double limit;
	
	public Girokonto(String inhaber, double kontostand, double limit)
	{
		super(inhaber, kontostand);
		this.limit = limit;	
	}

	public void einzahlen(double wert)
	{
		
		super.einzahlen(wert);
	}

	public void auszahlen(double wert)
	{
		if(wert<limit)
			super.auszahlen(wert);
		else{
			System.out.println("Es tut mir leid " + inhaber +", "+ wert + " übersteigt dein Limit, " +"max. Betrag: " + limit + ", dein Kontostand bleibt unverändert");
		}
	}

	@Override
	public String toString()
	{
		return inhaber+ ", Girokonto, aktueller Kontostand: " + kontostand;
	}
	
	

	
	
	
}
