package Konto;

public class Demo
{

	public static void main(String[] args)
	{
		Konto k = new Konto("Michi", 3800);
		Girokonto gk = new Girokonto("Max", 2000, 500);
		Sparkonto sk = new Sparkonto("Susi", 1200);
		JugendGiroKonto jgk = new JugendGiroKonto("Alex", 180, 100, 50);
		
		System.out.println(k);
		System.out.println(gk);
		System.out.println(sk);
		System.out.println(jgk);
		System.out.println("---------");
		
		gk.auszahlen(200);
		gk.auszahlen(800);
		sk.auszahlen(1770);
		sk.auszahlen(470);
		jgk.auszahlen(200);
		jgk.auszahlen(40);
		k.auszahlen(4000);
		System.out.println("---------");
		System.out.println(k);
		System.out.println(gk);
		System.out.println(sk);
		System.out.println(jgk);
		
		gk.auszahlen(400);
		gk.auszahlen(400);
		gk.auszahlen(400);
		gk.auszahlen(400);
		System.out.println(gk);
		gk.auszahlen(400);
		gk.auszahlen(400);
		gk.auszahlen(2000);
		System.out.println(gk);
		jgk.auszahlen(40);
		jgk.auszahlen(40);	jgk.auszahlen(40);	jgk.auszahlen(40);
		System.out.println(jgk);
		jgk.auszahlen(40);
		jgk.auszahlen(40);
		jgk.auszahlen(40);
		jgk.auszahlen(40);
		System.out.println(jgk);
		
		
		

	}

}
