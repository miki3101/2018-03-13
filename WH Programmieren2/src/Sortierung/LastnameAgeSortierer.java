package Sortierung;

import java.util.Comparator;

public class LastnameAgeSortierer implements Comparator<Person>
{
	@Override
	public int compare(Person o1, Person o2)
	{
		if(o1.getLastname().compareTo(o2.getLastname()) < 0)
			return -1;
		if(o1.getLastname().compareTo(o2.getLastname()) > 0)
			return 1;
		
		AgeSortierer as = new AgeSortierer();
		
		return as.compare(o1, o2);
		
	}

}
