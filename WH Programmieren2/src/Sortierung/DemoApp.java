package Sortierung;

import java.util.Arrays;

public class DemoApp
{
	public static void main(String[] args)
	{
		int [] demoArray = new int [8];
		
		demoArray[0] = 3;
		demoArray[1] = 4;
		demoArray[2] = 1;
		demoArray[3] = 9;
		demoArray[4] = 0;
		demoArray[5] = 2;
		demoArray[6] = 7;
		demoArray[7] = 1;
		
		printArray(demoArray);
		Arrays.sort(demoArray);
		printArray(demoArray);
		
		
		Person[] personen = new Person[5];
		
		personen[0] = new Person("Susi", "Sorglos", 12, 29);
		personen[1] = new Person("Max", "Muster", 7, 32);
		personen[2] = new Person("Wastl", "Wurst", 19, 42);
		personen[3] = new Person("Mizi", "Muster", 37, 19);
		personen[4] = new Person("John", "Schuster", 3, 58);
		
		System.out.println("-------------\n");
		
		printArray(personen);
		Arrays.sort(personen);
		printArray(personen); // wenn kein 2. Parameter �bergeben wird, wird die default Sortiermethode verwendet
		
		Arrays.sort(personen, new AgeSortierer()); // wenn 2. parameter �bergeben wird, wird mit diesen Regeln sortiert
		printArray(personen);
		
	
		
	}
	public static void printArray(Person[] arr)
	{
		for (Person i : arr)
		{
			System.out.print(i + ", ");
		}
		System.out.println();
	}

	public static void printArray(int[] arr)
	{
		for (int i : arr)
		{
			System.out.print(i + ", ");
		}
		System.out.println();

	}

}