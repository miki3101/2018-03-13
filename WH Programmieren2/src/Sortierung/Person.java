package Sortierung;

public class Person implements Comparable<Person>
{
	private String firstname;
	private String lastname;
	private int number;
	private int age;
	
	
	public Person(String firstname, String lastname, int number, int age)
	{
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.number = number;
		this.age = age;
	}
	
	
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + number;
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (firstname == null)
		{
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (lastname == null)
		{
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (number != other.number)
			return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return firstname + " " + lastname + ", " + number + ", "  + age + "\n";
	}
	@Override
	public int compareTo(Person other)
	{
		if(lastname.compareTo(other.lastname)>0)
		return 1;
		if(lastname.compareTo(other.lastname)<0)
			return -1;
		if(firstname.compareTo(other.firstname)>0)
			return 1;
		if(firstname.compareTo(other.firstname)<0)
			return -1;
		return 0;
	}
	public String getFirstname()
	{
		return firstname;
	}
	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}
	public String getLastname()
	{
		return lastname;
	}
	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}
	public int getNumber()
	{
		return number;
	}
	public void setNumber(int number)
	{
		this.number = number;
	}
	public int getAge()
	{
		return age;
	}
	public void setAge(int age)
	{
		this.age = age;
	}
	
	
	
	
}
