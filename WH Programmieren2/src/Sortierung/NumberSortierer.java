package Sortierung;

import java.util.Comparator;

public class NumberSortierer implements Comparator<Person>
{

	@Override
	public int compare(Person o1, Person o2)
	{
		if(o1.getNumber() < o2.getNumber())
			return -1;
		if(o1.getNumber() > o2.getNumber())
			return 1;
		return 0;
	}
	
}
