package Sortierung;

import java.util.Comparator;

public class AgeSortierer implements Comparator<Person>
{

	@Override
	public int compare(Person objectA, Person objectB)
	{
		if(objectA.getAge()< objectB.getAge())
			return -1;
		if(objectA.getAge()> objectB.getAge())
			return 1;
			
		return 0;
	}
	
}
