package Land;

import java.util.ArrayList;

public class Bundesstaat implements Land
{
	private ArrayList<Land> laender = new ArrayList<>();

	public double getBruttoSozialProdukt()
	{
		System.out.println("BSP aller Bundeslšnder: ");
		double alle = 0;
		
		for (Land land : laender)
		{
			alle += land.getBruttoSozialProdukt();
		}
		
		return alle;
	}
	
	public void addLand(Land land)
	{
		laender.add(land);
	}
	
	public String toString()
	{
		return "Bundeslšnder: \n" + laender;
	}

}
