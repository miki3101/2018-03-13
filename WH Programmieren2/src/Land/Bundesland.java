package Land;

public class Bundesland implements Land
{
	private String name;
	private double bsp;
	
	public Bundesland(double bsp, String name)
	{
		super();
		this.bsp = bsp;
		this.name = name;
	}
	
	public double getBruttoSozialProdukt()
	{
		return bsp;
	}
	
	public String toString()
	{
		return name + " " + bsp;
	}

}
