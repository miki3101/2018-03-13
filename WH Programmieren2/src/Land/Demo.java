package Land;

public class Demo
{

	public static void main(String[] args)
	{
		Bundesland b1 = new Bundesland(1200.00, "Steiermark");
		Bundesland b2 = new Bundesland(1400.00, "Vorarlberg");
		Bundesland b3 = new Bundesland(1300.00, "Oberösterreich");
		Bundesland b4 = new Bundesland(1200.00, "Kärnten");
		Bundesland b5 = new Bundesland(1700.00, "Burgenland");
		Bundesland b6 = new Bundesland(1900.00, "Tirol");
		Bundesland b7 = new Bundesland(2000.00, "Salzburg");
		
		Bundesstaat bs = new Bundesstaat();
		bs.addLand(b1);
		bs.addLand(b2);
		bs.addLand(b3);
		bs.addLand(b4);
		bs.addLand(b5);
		bs.addLand(b6);
		bs.addLand(b7);
		
		System.out.println(bs);
		System.out.println(bs.getBruttoSozialProdukt());
		
		

	}

}
