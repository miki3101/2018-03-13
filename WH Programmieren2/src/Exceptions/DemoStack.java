package Exceptions;

public class DemoStack
{

	public static void main(String[] args)
	{
		Stack stack = new Stack(5);

		try
		{
			stack.push(new Note("27.04.2018", "Note 1", " 1 - Note Demo"));
			stack.push(new Note("27.04.2018", "Note 2", " 2 - Note Demo"));
			stack.push(new Note("27.04.2018", "Note 3", " 3 - Note Demo"));

			System.out.println(stack.pop()); // Element 3
			System.out.println(stack.pop()); // Element 2

			stack.push(new Note("27.04.2018", "Note 4", " 4 - Note Demo"));
			stack.push(new Note("27.04.2018", "Note 5", " 5 - Note Demo"));
			stack.push(new Note("27.04.2018", "Note 6", " 6 - Note Demo"));

			System.out.println(stack.pop()); // Element 6

			stack.push(new Note("27.04.2018", "Note 7", " 7 - Note Demo"));
			stack.push(new Note("27.04.2018", "Note 8", " 8 - Note Demo"));
			
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println(stack.pop());
			System.out.println("Vor Fehler");
			System.out.println(stack.pop());
			System.out.println("Nach Fehler"); // wird nicht mehr ausgeführt
				
			stack.push(new Note("27.04.2018", "Note 9", " 9 - Note Demo"));
			System.out.println("Nach Fehler"); // wird nicht mehr ausgeführt
		} 
		catch (StackFullException sfe)
		{
			sfe.printStackTrace();
		}
		catch(StackEmptyException see)
		{
			see.printStackTrace();
		}
		finally // zB Datei muss geschlossen werden
		{// geht immer hier rein, auch wenn kein Fehler passiert
			System.out.println("Wir sind im Finally Block");
		}
	}
}
