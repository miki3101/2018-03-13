package Exceptions;

public class Stack
{
	private int pointer;
	private Note[] notes;

	public Stack(int size)
	{
		notes = new Note[size];
		pointer = 0;
	}

	public void push(Note note) throws StackFullException
	{
		// mit 1. push - pointer = 1
		if (pointer < notes.length)
		{
			notes[pointer++] = note;
		} else
		{ // Stack ist voll, keine weiteren Elemente mehr m�glich
			throw new StackFullException("Stack ist voll, max. Platz f�r " + notes.length + " Elemente");
		}

	}

	public Note pop() throws StackEmptyException
	{
		// pointer -1 um auf Index 0 zuzugreifen
		if(pointer!=0)
		{
		return notes[--pointer];
		}
		else {
			throw new StackEmptyException("Stack ist leer");
		}
		
	}
}
