package Tiere;

public class FlyingDog extends Dog implements Fly
{
	public FlyingDog(String name, String breed, double weight)
	{
		super(name, breed, weight);
		
	}

	public void fly()
	{
		System.out.println(name + "fliegt");
	} 
	


}
