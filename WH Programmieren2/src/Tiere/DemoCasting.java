package Tiere;

public class DemoCasting
{
	public static void main(String[] args)
	{
		Dog dog1 = new Dog("Wuffi", "Sch�fer", 4000);
		Dog dog2 = new Dog("Bello", "Dackel", 3000);
		Cat cat1 = new Cat("Schnurli", 2500, "besonders liebevoll");
		Cat cat2 = new Cat("Miezi", 2500, "besonders liebevoll");
		
		dog1.makeNoise();
		dog2.makeNoise();
		cat1.makeNoise();
		cat2.makeNoise();
		
		System.out.println("----------");
		
		//Up-Casting
		Animal a = dog1;
		a.makeNoise();
		System.out.println("--------");
		
		//Down-Casting
		Dog xyz = (Dog) a;  // es wird kein neues Objekt erstellt
		xyz.getName();
		xyz.makeNoise();
		
		System.out.println("Erkl�rung Down-Cast, wieso hin und wieder sinnvoll: ");
		doSomething(a);
		
		System.out.println("----------");
		
		//vertikales casting
		int x = 10;
		int y = 4;
		int ergebnis = x/y;
		System.out.println(ergebnis);
		System.out.println(x/(double) y);
		
		FlyingDog fd = new FlyingDog("fliegender Hund", "Superflieger", 400);
		fd.fly();
		Dog fdDog = fd;
		Fly f = fd;
		f.fly();
		
	}

	public static void doSomething(Animal a)
	{
		a.makeNoise();
		System.out.println("-------------");
		System.out.println(a.getClass());
		
		if(a instanceof Dog)
		{
			Dog newDog = (Dog) a;
			System.out.println("Das Gewicht von Dog ist: " + newDog.getWeight());
		}
		else if(a instanceof Cat)
		{
			Cat newCat = (Cat) a;
			System.out.println("Das Gewicht von Cat ist: " + newCat.getWeight());
		}
		
	}
}