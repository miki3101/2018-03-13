package Tiere;

import java.util.ArrayList;
import java.util.HashMap;

public class DemoTransportbox
{
	public static void main(String[] args)
	{
		Dog dog1 = new Dog("Niko", "Sch�fer", 4000);
		Cat cat1 = new Cat("Fauchi", 2500, "besonders liebevoll");
		Box b = new Box(10, 10, 10);
		
		Animal a = new Dog("Bello", "Bernhardiner", 2000);
		
		System.out.println("Volumen von Box: " + b.calcVolume());
		
		Transportbox<Dog> tb6 = new Transportbox<Dog>(10, 10, 10, "red", 99.99, dog1);
		
		System.out.println("Volumen von Transportbox: " + tb6.calcVolume());
		System.out.println("-------------");
		
		Transportbox<Cat> tb0 = new Transportbox<Cat>(10, 10, 10, "bluecat", 99.99, cat1);
		Transportbox<Dog> tb1 = new Transportbox<Dog>(10, 10, 10, "blue", 99.99, dog1);
		Transportbox<Dog> tb2 = new Transportbox<Dog>(10, 10, 10, "green", 99.99, dog1);
		Transportbox<Dog> tb3 = new Transportbox<Dog>(10, 10, 10, "white", 99.99, dog1);
		Transportbox<Dog> tb4 = new Transportbox<Dog>(10, 10, 10, "black", 99.99, dog1);

		ArrayList<Transportbox> listeBoxen = new ArrayList<>();
		
		listeBoxen.add(tb6);
		listeBoxen.add(tb0);
		listeBoxen.add(tb1);
		listeBoxen.add(tb2);
		listeBoxen.add(tb3);
		listeBoxen.add(tb4);
		
		System.out.println("Liste der Boxen: ");
		for (Box tb : listeBoxen)
		{
			System.out.println(tb.calcVolume());
		
		}
		System.out.println("-------------");
		System.out.println(getCountPerColour(listeBoxen));
		
	}
		
		public static HashMap<String,Integer> getCountPerColour(ArrayList<Transportbox> listeBoxen)
		{
			HashMap<String, Integer> result = new HashMap<>();
			
			for (Transportbox<Dog> transportbox : listeBoxen)
			{
				if(result.containsKey(transportbox.getColour()))
				{
					int count = result.get(transportbox.getColour());
					count++;
					result.put(transportbox.getColour(), count);
				}
				else
				{
					result.put(transportbox.getColour(), 1);
				}
			}
			
			return result;
		}
		
	}

