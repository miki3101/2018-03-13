package Tiere;

public class Dog extends Animal
{
	protected String name;
	private String breed;
	private double weight;
	
	
	public Dog(String name, String breed, double weight)
	{
		super();
		this.name = name;
		this.breed = breed;
		this.weight = weight;
	}

	public String toString()
	{
		return "Dog [name=" + name + ", breed=" + breed + ", weight=" + weight + "]";
	}
	
	public void makeNoise()
	{
		System.out.println(name + " macht: Wuff");
	}

	public String getName()
	{
		return name;
	}

	public String getBreed()
	{
		return breed;
	}

	public double getWeight()
	{
		return weight;
	}

	
	
	
}
