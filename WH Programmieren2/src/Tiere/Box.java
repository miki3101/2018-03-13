package Tiere;

public class Box
{
	protected int length;
	protected int height;
	protected int width;
	
	
	public Box(int length, int height, int width)
	{
		super();
		this.length = length;
		this.height = height;
		this.width = width;
	}

	public int calcVolume()
	{
		return length * height * width;	
	}
	
	

	public String toString()
	{
		return "Box [length=" + length + ", height=" + height + ", width=" + width + "]";
	}
	
	
}
