package Tiere;

public class Transportbox<T> extends Box			// T f�r Tier in der Box (T - Platzhalter)
{
	private String colour;
	private double price;
	private T content;
	
	
	public Transportbox(int length, int height, int width, String colour, double price, T content)
	{
		super(length, height, width);
		this.colour = colour;
		this.price = price;
		this.content = content;
	}
	
	public int calcVolume()
	{
		return (int) (this.height * this.length * this.width * 0.7);
	}

	
	
	public String getColour()
	{
		return colour;
	}

	public double getPrice()
	{
		return price;
	}

	public String toString()
	{
		return "Transportbox [colour=" + colour + ", price=" + price + "]";
	}
	
	

	
	
}
