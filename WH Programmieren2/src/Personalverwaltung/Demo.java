package Personalverwaltung;

import java.util.Arrays;

public class Demo
{

	public static void main(String[] args)
	{
		Employee[] arr = new Employee [4];
		
		arr[0] = new Employee(3, "Maxi", 4000.00, "CEO");
		arr[1] = new Employee(9, "Susi", 3500.00, "DEV");
		arr[2] = new Employee(1, "John", 2700.00, "DEV");
		arr[3] = new Employee(5, "John", 2000.00, "Marketing");
		
		printArray(arr);
		Arrays.sort(arr, new EmpNumber());
		printArray(arr);
	
		Arrays.sort(arr, new NameDevelopment());
		printArray(arr);
		Arrays.sort(arr, new EmpNumber());

	}

	public static void printArray(Employee[] arr)
	{
		for (Employee i : arr)
		{
			System.out.print(i);
		}
		System.out.println();
	}
}
