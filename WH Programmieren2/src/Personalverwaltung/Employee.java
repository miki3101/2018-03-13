package Personalverwaltung;

public class Employee
{
	private int empNumber;
	private String name;
	private double salary;
	private String departement;
	
	
	public Employee(int empNumber, String name, double salary, String departement)
	{
		super();
		this.empNumber = empNumber;
		this.name = name;
		this.salary = salary;
		this.departement = departement;
	}

	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((departement == null) ? 0 : departement.hashCode());
		result = prime * result + empNumber;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(salary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (departement == null)
		{
			if (other.departement != null)
				return false;
		} else if (!departement.equals(other.departement))
			return false;
		if (empNumber != other.empNumber)
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(salary) != Double.doubleToLongBits(other.salary))
			return false;
		return true;
	}

	public String toString()
	{
		return "Employee empNumber: " + empNumber + ", name: " + name + ", salary: " + salary + ", departement: "
				+ departement + "\n";
	}

	public int getEmpNumber()
	{
		return empNumber;
	}

	public void setEmpNumber(int empNumber)
	{
		this.empNumber = empNumber;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public double getSalary()
	{
		return salary;
	}

	public void setSalary(double salary)
	{
		this.salary = salary;
	}

	public String getDepartement()
	{
		return departement;
	}

	public void setDepartement(String departement)
	{
		this.departement = departement;
	}
}
