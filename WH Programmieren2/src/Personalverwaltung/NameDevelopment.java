package Personalverwaltung;

import java.util.Comparator;

public class NameDevelopment implements Comparator<Employee>
{

	public int compare(Employee o1, Employee o2)
	{
		int result = o1.getName().compareTo(o2.getName());
		
		if(result == 0)
			result = o1.getDepartement().compareTo(o2.getDepartement());
		
		return result;
	}

}
