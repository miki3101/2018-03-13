package Eventkalender;

import java.util.ArrayList;
import java.util.HashMap;

public class EventKalender
{
	private ArrayList<Event> events = new ArrayList<>();
	
	public void addEvent(Event e)
	{
		events.add(e);
	}
	
	public Event getByTitel(String titel)
	{
		Event x = new Event();
		
		for (Event event : events)
		{
			if(event.getTitel()==titel)
			x = event;
		}
		return x;
	}
	
	public ArrayList<Event> getByOrt(String ort)
	{
		ArrayList<Event> l = new ArrayList<>();
		
		for (Event event : events)
		{
			if(event.getOrt() == ort)
				l.add(event);
		}
		return l;
	}
	
	public ArrayList<Event> getByEintrittspreis(double min, double max)
	{
		ArrayList<Event> preis = new ArrayList<>();
		
		for (Event event : events)
		{
			if(event.getPreis()> min && event.getPreis()< max)
				preis.add(event);
		}
		
		return preis;
	}
	
	public Event getMostExpensiveByOrt(String ort)
	{
		Event teuer = new Event();
		
		for (Event event : events)
		{
			if(event.getOrt() == ort && event.getPreis()> teuer.getPreis())
				teuer = event;
		}
		return teuer;
	}
	
	public double getAvgPreisByOrt(String ort)
	{
		ArrayList<Event> x = new ArrayList<>();
		double ds = 0;
		
		for (Event event : events)
		{
			if(event.getOrt() == ort)
				x.add(event);
		}
		for (Event event : x)
		{
			ds = event.getPreis() / x.size();
		}
		return ds;
	}
	
	public HashMap<String, Integer> getCountEventsByOrt()
	{
		HashMap<String, Integer> x = new HashMap<>();
		
		for (Event event : events)
		{
			if(x.containsKey(event.getOrt()))
			{
			int zahl = x.get(event.getOrt());
			zahl++;
			x.put(event.getOrt(), zahl);
			}else {
				x.put(event.getOrt(), 1);
			}
		}
		
		return x;
	}
	
	public HashMap<String, Double> getSumPriceEventsByOrt()
	{
		HashMap<String, Double> x = new HashMap<>();
		
		for (Event event : events)
		{
			if(x.containsKey(event.getOrt()))
			{
				double zahl = x.get(event.getOrt());
				zahl+= event.getPreis();
			x.put(event.getOrt(), zahl);
			}
			else {
				x.put(event.getOrt(), event.getPreis());
			}
			
			}
				
		return x;
	}

	@Override
	public String toString()
	{
		return "EventKalender: \n" + events;
	}
	
	
	
}
