package LogisticManager;

public class Car implements Moveable
{
	protected String typ;
	protected int weight;
	private String colour;
	
	
	public Car(String typ, String colour, int weight)
	{
		super();
		this.typ = typ;
		this.colour = colour;
		this.weight = weight;
	}


	public String toString()
	{
		return "Car [typ=" + typ + ", color=" + colour + ", weight=" + weight + "]";
	}


	
	public void move(String destination)
	{
		System.out.println(colour + " "+ typ + " will be moved to " + destination);
		
	}
	
	
}
