package LogisticManager;

import java.util.ArrayList;

public class LogisticManager
{
	ArrayList<Moveable> liste = new ArrayList<>();
	
	
	public void addObject(Moveable a)
	{
		liste.add(a);
	}
	
	
	public void moveAll(String destination)
	{
		for (Moveable moveable : liste)
		{
			moveable.move(destination);
		}
	}
}

