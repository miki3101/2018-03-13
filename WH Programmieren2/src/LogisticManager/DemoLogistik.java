package LogisticManager;

public class DemoLogistik
{

	public static void main(String[] args)
	{
		Car car1 = new Car("BMW", "Black", 5000);
		Car car2 = new Car("Audi", "White", 4500);
		
		Shirt s1 = new Shirt("Levis", 36, "Yellow");
		Shirt s2 = new Shirt("MarcoPolo", 36, "Blue");
		
		LogisticManager m1 = new LogisticManager();
		
		m1.addObject(car1);
		m1.addObject(car2);
		m1.addObject(s1);
		m1.addObject(s2);
		
		m1.moveAll("Graz");
		
		

	}

}
