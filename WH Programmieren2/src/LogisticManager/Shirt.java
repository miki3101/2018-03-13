package LogisticManager;

public class Shirt implements Moveable
{
	protected String brand;
	protected int size;
	protected String colour;
	
	
	public Shirt(String brand, int size, String colour)
	{
		super();
		this.brand = brand;
		this.size = size;
		this.colour = colour;
	}

	public String toString()
	{
		return "Shirt [brand=" + brand + ", size=" + size + ", color=" + colour + "]";
	}

	@Override
	public void move(String destination)
	{
		System.out.println(colour +" "+ brand + " will be moved to " + destination);
		
	}
	
	
	
}
