package Mitarbeiter;

public class DemoMitarbeiter
{

	public static void main(String[] args)
	{
		FixCommissionEmployee e1 = new FixCommissionEmployee("Huber", "Max", "Software", 2000.00, 500);
		FixCommissionEmployee e2 = new FixCommissionEmployee("Hubertus", "Martin", "Software", 2500.00, 100);
		PercentCommissionEmployee e3 = new PercentCommissionEmployee("Maier", "Maria", "Software", 3000.00, 5);
		PercentCommissionEmployee e4 = new PercentCommissionEmployee("Schiller", "Sandra", "Marketing", 1500.00, 7);
		FixCommissionEmployee e5 = new FixCommissionEmployee("Sorglos", "Susi", "Marketing", 1700.00, 400);
	
		EmployeeManager m1 = new EmployeeManager();
		
		m1.addEmployee(e1);
		m1.addEmployee(e2);
		m1.addEmployee(e3);
		m1.addEmployee(e4);
		m1.addEmployee(e5);
		
		System.out.println("Gesamtgehalt: " + m1.calcTotalSalary());
		System.out.println("Gehalt pro Abteilung: " + m1.getSalaryByDepartment());
		System.out.println("Anzahl der Mitarbeiter pro Abteilung: " + m1.anzahlMitarbeiter());
		
		
	}

}
