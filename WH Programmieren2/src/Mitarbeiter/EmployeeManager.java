package Mitarbeiter;

import java.util.ArrayList;
import java.util.HashMap;

public class EmployeeManager
{
	protected ArrayList<Employee> liste = new ArrayList<>();
	
	
	public void addEmployee(Employee a)
	{
		liste.add(a);
	}
	
	public HashMap<String, Double> getSalaryByDepartment()
	{
		HashMap<String, Double> result = new HashMap<>();
		
		for (Employee employee : liste)
		{
			if(result.containsKey(employee.getDepartment()))
					{
						double salary = result.get(employee.department) + employee.getFullSalary(); // auslesen + bearbeiten
						result.put(employee.getDepartment(), salary); // ablegen
					}
			else
			{
				result.put(employee.getDepartment(), employee.getFullSalary());
			}
		}
		
		return result;
	}
	
	public HashMap<String, Integer> anzahlMitarbeiter()
	{
		HashMap<String, Integer> anzahl = new HashMap<>();
		
		for (Employee employee : liste)
		{
			if(anzahl.containsKey(employee.getDepartment())) 
			{
				int count = anzahl.get(employee.department);  // auslesen
				count++;								   	 // bearbeiten
				anzahl.put(employee.getDepartment(), count);// ablegen
			}
			else
			{
				anzahl.put(employee.getDepartment(), 1);
			}
		}
	
		return anzahl;
	}

	public double calcTotalSalary()
	{
		double totalSalary = 0;
		
		for (Employee employee : liste)
		{
		 totalSalary += employee.getFullSalary();
		}
		return totalSalary;
	}

	@Override
	public String toString()
	{
		return "EmployeeManager [liste=" + liste + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((liste == null) ? 0 : liste.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeManager other = (EmployeeManager) obj;
		if (liste == null)
		{
			if (other.liste != null)
				return false;
		} else if (!liste.equals(other.liste))
			return false;
		return true;
	}
	
}
