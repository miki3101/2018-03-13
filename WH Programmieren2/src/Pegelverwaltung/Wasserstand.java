package Pegelverwaltung;

public class Wasserstand
{
	private int id;
	private String gewaesserName;
	private double messWert;
	private double messWertFuerAlamierung;
	private long zeitpunkt;
	
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getGewaesserName()
	{
		return gewaesserName;
	}
	public void setGewaesserName(String gewaesserName)
	{
		this.gewaesserName = gewaesserName;
	}
	public double getMessWert()
	{
		return messWert;
	}
	public void setMessWert(double messWert)
	{
		this.messWert = messWert;
	}
	public double getMessWertFuerAlamierung()
	{
		return messWertFuerAlamierung;
	}
	public void setMessWertFuerAlamierung(double messWertFuerAlamierung)
	{
		this.messWertFuerAlamierung = messWertFuerAlamierung;
	}
	public long getZeitpunkt()
	{
		return zeitpunkt;
	}
	public void setZeitpunkt(long zeitpunkt)
	{
		this.zeitpunkt = zeitpunkt;
	}
	
	public String toString()
	{
		return "ID: " + id + ", Gew�sser Name: " + gewaesserName + ", Messwert: " + messWert
				+ ", Alamierung ab: " + messWertFuerAlamierung + ", Zeitpunkt: " + zeitpunkt + "\n";
	}
	
	
	
	
}
