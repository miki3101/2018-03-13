package Pegelverwaltung;
import java.util.ArrayList;

public class WasserstandManager
{
	private ArrayList<Wasserstand> ws = new ArrayList<>();
	
	
	public  void addWasserstand(Wasserstand w)
	{
		ws.add(w);
	}
	
	public Wasserstand findById(int id)
	{
		Wasserstand x = new Wasserstand();
		
		for (Wasserstand wasserstand : ws)
		{
			if(wasserstand.getId() == id);
			x = wasserstand;
		}
		
		return x;
	}
	
	public ArrayList<Wasserstand> findAllByGewaesser(String gewaesserName)
	{
		ArrayList<Wasserstand> name = new ArrayList<>();
		
		for (Wasserstand wasserstand : name)
		{
			if(wasserstand.getGewaesserName() == gewaesserName)
				name.add(wasserstand);
		}
		return name;
	}
	
	public Wasserstand findLastWasserstand(String gewaesserName)
	{
		Wasserstand x = new Wasserstand();
		
		for (Wasserstand wasserstand : ws)
		{
			if(wasserstand.getGewaesserName() == gewaesserName)
				x = wasserstand;
			if(x.getZeitpunkt()<=wasserstand.getZeitpunkt())
				x = wasserstand;
		}
		
		return x;
	}
	
	public ArrayList<Wasserstand> findForAlamierung()
	{
		ArrayList<Wasserstand> alarm = new ArrayList<>();
		
		for (Wasserstand wasserstand : ws)
		{
			if(wasserstand.getMessWert()>= wasserstand.getMessWertFuerAlamierung())
				alarm.add(wasserstand);
		}
		return alarm;
	}
	
	public double calcMittlererWasserstand(String gewaesserName)
	{
		ArrayList<Wasserstand> name = new ArrayList<>();
		double mittel = 0;
		
		for (Wasserstand wasserstand : ws)
		{
			if(wasserstand.getGewaesserName() == gewaesserName)
				name.add(wasserstand);
		}
		for (Wasserstand wasserstand : name)
		{
			mittel = wasserstand.getMessWert() / name.size();
		}
		
		return mittel;
	}
	
	public ArrayList<Wasserstand> findByZeitpunkt (int von, int bis, String gewaesserName)
	{
		ArrayList<Wasserstand> zp = new ArrayList<>();
		
		for (Wasserstand wasserstand : ws)
		{
			if(wasserstand.getGewaesserName() == gewaesserName && wasserstand.getZeitpunkt() > von && wasserstand.getZeitpunkt()<bis)
				zp.add(wasserstand);
		}
		
		return zp;
	}

	public String toString()
	{
		return "Wasserstände: \n" + ws;
	}
	
	
}
