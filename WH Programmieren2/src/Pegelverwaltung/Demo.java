package Pegelverwaltung;

public class Demo
{

	public static void main(String[] args)
	{
		Wasserstand w1 = new Wasserstand();
		w1.setGewaesserName("Schwarzl");
		w1.setId(1);
		w1.setMessWert(5.2);
		w1.setMessWertFuerAlamierung(6.0);
		w1.setZeitpunkt(3500);
		
		Wasserstand w2 = new Wasserstand();
		w2.setGewaesserName("Hilmteich");
		w2.setId(2);
		w2.setMessWert(4.3);
		w2.setMessWertFuerAlamierung(6.0);
		w2.setZeitpunkt(4700);
		
		Wasserstand w3 = new Wasserstand();
		w3.setGewaesserName("Gr�ner See");
		w3.setId(3);
		w3.setMessWert(8.8);
		w3.setMessWertFuerAlamierung(4.2);
		w3.setZeitpunkt(2600);
		
		WasserstandManager mg = new WasserstandManager();
		
		mg.addWasserstand(w1);
		mg.addWasserstand(w2);
		mg.addWasserstand(w3);
		
		System.out.println(mg);
		System.out.println(mg.findForAlamierung());
	

	}

}
