package Figuren;

public class IPhone8 extends BaseMobilePhone implements Figure
{
	private String logo;
	private double width;
	private double height;
	private double reductionRoundedCorners;

	@Override
	public double getPerimeter()
	{
		return 2* (width + height) - reductionRoundedCorners;
	}

	public IPhone8(String logo, double width, double height, double reductionRoundedCorners)
	{
		super();
		this.logo = logo;
		this.width = width;
		this.height = height;
		this.reductionRoundedCorners = reductionRoundedCorners;
	}

	@Override
	public double getArea()
	{
		return width * height - reductionRoundedCorners;
	}
}
