package Figuren;

import java.util.ArrayList;
import java.util.HashMap;

public class FigureManager
{
	private ArrayList<Figure> figures = new ArrayList<>();
	
	public void add(Figure f)
	{
		figures.add(f);
	}
	
	public double getMaxPerimeter()
	{
		double max = 0;
		
		for (Figure figure : figures)
		{
			if(figure.getPerimeter()>max)
				max = figure.getPerimeter();
		}
		return max;
	}
	
	public double getAverageAreaSize()
	{
		double sum = 0;
		
		for (Figure figure : figures)
		{
			sum+= figure.getArea();
		}
	
	return sum/figures.size();
	}
	
	public HashMap<String, Double> getAreaSizeByCategories()
	{
		HashMap<String, Double> erg = new HashMap<>();
		
		erg.put("klein", 0.0);
		erg.put("mittel", 0.0);
		erg.put("gro�", 0.0);
		
		for (Figure figure : figures)
		{
			if(figure.getArea()< 1000)
			{
				double value = erg.get("klein");
				value += figure.getArea();
				erg.put("klein", value);
			}
			else if(figure.getArea()< 5000)
			{
				double value = erg.get("mittel");
				value += figure.getArea();
				erg.put("mittel", value);
			}
			else
			{
				double value = erg.get("gro�");
				value += figure.getArea();
				erg.put("gro�", value);
			}
		}
		
		return erg;
	}
}
