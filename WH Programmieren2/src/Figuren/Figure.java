package Figuren;

public interface Figure
{
	double getPerimeter();
	double getArea();
}

