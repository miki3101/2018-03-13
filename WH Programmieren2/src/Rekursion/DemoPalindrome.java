package Rekursion;

public class DemoPalindrome
{

	public static void main(String[] args)
	{
		System.out.println(palindrome("Rentner"));
		System.out.println("------");
		System.out.println(palindrome("Rentxner"));
		System.out.println("------");
		System.out.println(palindrome("Reliefpfeiler"));

	}

	public static boolean palindrome(String p)
	{		
		if (p.length() <= 1)
		{
			return true;
		}
			if (p.toLowerCase().charAt(0) != p.toLowerCase().charAt(p.length()-1)) // index, daher l�nge -1
			{
				return false;
			}
			
			System.out.println(p);
		
		return palindrome(p.toLowerCase().substring(1, p.toLowerCase().length()-1));
	}
}
