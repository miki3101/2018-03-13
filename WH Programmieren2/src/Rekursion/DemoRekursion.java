package Rekursion;

public class DemoRekursion
{
	public static void main(String[] args)
	{
		System.out.println(quersumme(8, 1));
	}
	
	public static int quersumme(int wert, int iteration)
	{
		if(wert==0)
		
			return 0;
		
		System.out.println(iteration + ": " + wert);

		return wert + quersumme(wert -1, ++iteration);
	}

}
