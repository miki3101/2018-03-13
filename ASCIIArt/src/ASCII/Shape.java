package ASCII;

import java.util.Arrays;

public class Shape
{
	protected int size;
	protected char shape [][];
	
	
	public Shape(int size)
	{
		this.size = size;
		this.shape = new char [size][size];
		
	}


	public String toString()
	{
		String out = "";
	
		int x = size/3;
		
	
		for(int s = 0; s < shape.length; s++)
		{		
			for (int j = 0; j < shape.length; j++)
			{
				out += shape[s][j];	
			}
			out += "\n";						
		}
		
		
		return out;
	}
	
	
}
