import Personen.Datei;
import Personen.Verzeichnis;

public class VerzeichnisApp
{

	public static void main(String[] args)
	{
		Datei d1 = new Datei("test1.txt", 100);
		Datei d2 = new Datei("test2.txt", 100);
		Datei d3 = new Datei("test3.txt", 100);
		Datei d4 = new Datei("test4.txt", 100);
		
		System.out.println(d1);
		
		Verzeichnis root = new Verzeichnis("c:/");
		
		root.addDatei(d1);
		root.addDatei(d2);
		root.addDatei(d3);
		
		System.out.println(root);
		
		
		Verzeichnis v1 = new Verzeichnis("Dateien");
		v1.addDatei(d4);
		root.addVerzeichnis(v1);
		
		System.out.println(root);
		System.out.println(root.dateigroesse());
		
		
	}

}
