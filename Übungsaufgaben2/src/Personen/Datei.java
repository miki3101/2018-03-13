package Personen;

public class Datei
{

	private String name;
	private double groesse;
	
	
	public Datei (String name, double groesse)
	{
		this.name = name;
		this.groesse = groesse;
	}
	public double getgroesse()
	{
		return groesse;
	}
	
	
	public String toString()
	{
		
		return name + " " + groesse;
	}

}
