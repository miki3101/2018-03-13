package Personen;
import java.util.ArrayList;

public class Verzeichnis
{

	private String name;
	private ArrayList<Datei> dateien = new ArrayList ();
	private ArrayList<Verzeichnis> verzeichnisse = new ArrayList<>();
	
	
	public Verzeichnis (String name)
	{
		this.name = name;
		
	}
	public double dateigroesse()
	{
		double inhalt = 0;
		for (Datei datei : dateien)
		{
			inhalt += datei.getgroesse();
		}
		for (Verzeichnis verzeichnis : verzeichnisse)
		{
			inhalt += verzeichnis.dateigroesse();	
		}
		
			
		return inhalt;
	}
	public ArrayList<Datei> getVerzeichnis()
	{
		return dateien;
	}
	public void addVerzeichnis (Verzeichnis neuesVerzeichnis)
	{
		verzeichnisse.add(neuesVerzeichnis);
	}
	public void addDatei (Datei neueDatei)
	{
		dateien.add(neueDatei);
	}
	public String toString()
	{
		return name + " "+ dateien + " " + verzeichnisse;  
	}
	
}
