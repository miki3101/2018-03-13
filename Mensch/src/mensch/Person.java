package mensch;

public class Person
{
	private String vorname;
	private String nachname;
	private int alter;
	private String kinder;
	private String adresse;
	private int plz;
	private String ort;
	
	public Person() {}
	
	public Person(String vorname, String nachname, String adresse, int plz, String ort)
	{
		this.vorname = vorname;
		this.nachname = nachname;	
		this.adresse = adresse;
		this.plz = plz;
		this.ort = ort;
	}
	
	public Person(String vorname, String nachname, int alter, String kinder, String adresse, int plz, String ort)
	{
		this.vorname = vorname;
		this.nachname = nachname;
		this.alter = alter;
		this.kinder = kinder;
		this.adresse = adresse;
		this.plz = plz;
		this.ort = ort;
	}
	
	public String getNachname()
	{
		return nachname;
	}

	public void setNachname(String nachname)
	{
		this.nachname = nachname;
	}
	
	public String getVorname()
	{
		return vorname;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}
	
	

	public int getPlz()
	{
		return plz;
	}

	public void setPlz(int plz)
	{
		this.plz = plz;
	}

	public String getOrt()
	{
		return ort;
	}

	public void setOrt(String ort)
	{
		this.ort = ort;
	}

	public String getAdresse()
	{
		return adresse;
	}

	public void setAdresse(String adresse)
	{
		this.adresse = adresse;
	}

	public String toString ()
	{
		if (alter < 1)
		{
			return vorname+" "+nachname+" \n"+plz+" "+ort+", "+adresse+" \n";
		}
		
		return vorname+" "+nachname+" \n"+alter+" Jahre alt \n"+ kinder+" Kinder \n"+plz+" "+ort+", "+adresse+" \n";
	}

}
