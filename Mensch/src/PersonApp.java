import mensch.Person;

public class PersonApp
{

	public static void main(String[] args)
	{
		Person person1 = new Person("Max", "Mustermann", 39, "keine", "Musterstraße 1", 8000, "Musterhausen");
		Person person2 = new Person("Maria", "Müller", 38, "2", "Müllerstraße 5", 8000, "Musterhausen");
		Person person3 = new Person();
		
		person3.setNachname("Koller");
		person3.setVorname("Hansi");
		person3.setAdresse("Hausstraße 1");
		person3.setPlz(9000);
		person3.setOrt("Hintertupfing");
		
			
		System.out.println(person1);
		System.out.println(person2);
		System.out.println(person3);
		

	}

}
