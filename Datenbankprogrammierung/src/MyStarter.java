import java.util.HashMap;

public class MyStarter
{
	public static void main(String[] args)
	{
		MyDBHelper helper = new MyDBHelper();
		
		Student a = new Student("Susi", "Sorglos", 5600.00, false, "rosa");

		helper.init();
		
		helper.printAllUrlaube();
		System.out.println("---------\n");	
		
		helper.printAllStudents();
		System.out.println("----------\n");
		
		helper.printAllUrlaubeWithPreisGreaterThanOrderByOrt(1000.00);
		System.out.println("---------\n");
		
		helper.printDetailsForStudentWithID(1);
		System.out.println("--------------\n");
		
		helper.printDetailsForStudentWithID(4);
		System.out.println("--------------\n");
		
		helper.printDetailsForStudentWithIDWithPreparedStatement(2);
		System.out.println("---------------\n");
		
		helper.printAllUrlaubeForStudentWithIdAndPriceGreaterThan(1, 700.00);
		System.out.println("----------------\n");
		
		helper.printStudendAndUrlaube(1, 700.00);
		System.out.println("----------------\n");
		
		helper.printStudendAndUrlaube(4, 700.00);
		System.out.println("----------------\n");
		
		helper.printStudendAndUrlaube(5, 700);
		System.out.println("------------\n");
		
		helper.printUrlaubsuebersichtForAllStudents();
		System.out.println("--------------\n");
		
		helper.printZusammenfassungProStudent();
		System.out.println("------------\n");
		
		helper.urlaubsortStatistik();
		System.out.println("--------------\n");
		
		System.out.println("Student mit gew�nschter ID:");
		System.out.println(helper.getStudent(1));
		System.out.println("--------------\n");
		
		int update = helper.updateStudent(1, 9800.00);
		System.out.println(helper.getStudent(1));
		System.out.println("Es wurde " + update + " Zelle ver�ndert");
		System.out.println("----------\n");
		
		int newstudentID = helper.addNewStudent(a);
		System.out.println("Student wurde hinzugef�gt, ID mit der Nr " + newstudentID + " wurde �bergeben");
		System.out.println("--------\n");
		
		int updatestudent = helper.updateStudentWhereFirstnameLike("Michi", 20);
		System.out.println(updatestudent + " Studenten haben Credits erhalten");
		System.out.println("--------\n");
		
		int delete = helper.deleteStudent(50);
		System.out.println(delete + " Student wurde gel�scht");
		System.out.println("--------\n");
		
		System.out.println("Spaltennamen der Tabelle Student:");
		helper.printColumnNamesForTableStudentFromMetadata();
		System.out.println("-----------\n");
		
		System.out.println(helper.getColumnNamesForTableStudentFromMetadata());
		System.out.println("-----------\n");
		
		// sch�nere Ausgabe f�r eine HashMap
		HashMap<String, String> resultMap = helper.getColumnNamesForTableStudentFromMetadata();
		resultMap.forEach((x,y)->System.out.printf("%s %s %n", x, y)); 
		System.out.println("----------\n");
		
		System.out.println("Details eines Studenten inkl. Urlaubsw�nsche: \n" + helper.getStudentWithUrlaubsDetails(1));
		System.out.println("--------\n");
		
		
		
		helper.close();
		
	}

}