import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class MyDBHelper
{

	private Connection con = null;

	public void init()
	{

		try
		{
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			// 2. get Connection to database
			con = DriverManager.getConnection("jdbc:ucanaccess://"
					+ "C://Users//michaela.leskoschek//Documents//Nowa//Datenbanken Programmieren/Urlaubsverwaltung.accdb");

			System.out.println("Sie sind mit der Datenbank verbunden \n");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
				System.out.println("Verbindung zur Datenbank wurde getrennt");

			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	public void printAllUrlaubeWithPreisGreaterThanOrderByOrt(double preis)
	{
		try
		{
			Statement stmt = con.createStatement(); // Statement erzeugen
			String query = "SELECT Ort, Preis FROM Urlaubswunsch WHERE Preis > " + preis + " ORDER BY Ort";
			ResultSet rs = stmt.executeQuery(query);

			System.out.println("Urlaubsziele die mehr kosten als: � " + preis);

			while (rs.next())
			{
				System.out.printf("Ort: %s \t Preis: � %.2f \n", rs.getString(1), rs.getDouble(2));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void urlaubsortStatistik()
	{
		System.out.println("Welcher Urlaubswunsch kommt wie oft vor");
		System.out.println("Ort \t \t Anzahl\n");

		String query = "Select ort, count(ort) from Urlaubswunsch group by ort"
				+ " having count(*) > 0 order by count(*) desc";

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				System.out.printf("%s \t %d \n", rs.getString(1), rs.getInt(2));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printZusammenfassungProStudent()
	{
		System.out.println("Urlaubsw�nsche aller Studenten");
		System.out.println("Name \t Durchschnitts-Preis \t Anzahl der W�nsche \n");

		String query = "Select student.vorname, avg(Urlaubswunsch.Preis), count(Urlaubswunsch.StudentID)"
				+ " FROM student left join urlaubswunsch on Student.studentID = urlaubswunsch.studentID" // um alle
																											// Studenten
																											// auszugeben
				+ " GROUP BY vorname order by vorname"; // auch die, die keinen Urlaubswunsch haben

		try
		{
			PreparedStatement stmt = con.prepareStatement(query); // Statement erzeugen
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				System.out.printf("%s \t � %.2f \t \t %d \n", rs.getString(1), rs.getDouble(2), rs.getInt(3));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printUrlaubsuebersichtForAllStudents()
	{
		String query = "SELECt * FROM Student";

		System.out.println("�bersicht der Urlaubsw�nsche aller Studenten: ");

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				printStudendAndUrlaube(rs.getInt(1), 0);
				System.out.println("\n");
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printStudendAndUrlaube(int id, double preis)
	{
		String query = "SELECT vorname, nachname FROM Student WHERE studentID = ?";

		System.out.println("Urlaubsw�nsche von Student " + id);

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setDouble(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				System.out.printf("%s %s \n", rs.getString(1), rs.getString(2));
			} else
			{
				System.out.println("Student nicht gefunden");
			}

			String queryUrlaub = "SELECT Ort, preis FROM Urlaubswunsch WHERE preis > ? and studentID= ?";

			try
			{
				PreparedStatement stmtFuerUrlaub = con.prepareStatement(queryUrlaub);
				stmtFuerUrlaub.setDouble(1, preis);
				stmtFuerUrlaub.setInt(2, id);
				ResultSet res = stmtFuerUrlaub.executeQuery();

				int counter = 0;
				while (res.next())
				{
					counter++;
					System.out.printf("%s \t � %.2f \n", res.getString(1), res.getDouble(2));
				}
				if (counter == 0)
				{
					System.out.println("Dieser Student hat keine Urlaubsw�nsche");
				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printAllUrlaubeForStudentWithIdAndPriceGreaterThan(int id, double preis)
	{
		String query = "SELECT Ort, Preis FROM Urlaubswunsch WHERE preis > ? AND studentID = ?";

		System.out.println("Urlaubsziele von Student " + id + " die mehr kosten als � " + preis);

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setDouble(1, preis);
			stmt.setInt(2, id);

			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				System.out.printf("%s \t  � %.2f \n", rs.getString(1), rs.getDouble(2));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printDetailsForStudentWithIDWithPreparedStatement(int id)
	{
		String query = "SELECT vorname, nachname From Student " + "WHERE studentID = ?";

		System.out.println("Name des Studenten mit der ID: " + id);

		try
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if (rs.next())
			{
				System.out.printf("%s %s \t \n", rs.getString(1), rs.getString(2));
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printAllStudents()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Vorname, Nachname,  Credits, Inskribiert FROM Student";
			ResultSet rs = stmt.executeQuery(query);

			System.out.println("Namen aller Studenten: ");

			while (rs.next())
			{
				String vorname = rs.getString(1);
				String nachname = rs.getString(2);
				double credits = rs.getDouble(3);
				boolean inskribiert = rs.getBoolean(4);

				System.out.printf("%s %s\t Guthaben: � %.2f \t inskribiert: %s \n", vorname, nachname, credits,
						inskribiert);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printDetailsForStudentWithID(int id)
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT StudentID, Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student WHERE StudentID = "
					+ id;
			ResultSet rs = stmt.executeQuery(query);

			if (rs.next() == true)
			{
				System.out.println("Datenblatt von Student: " + id);
				System.out.printf("%d %s %s \t Guthaben: � %.2f \t inskribiert: %s  Lieblingsfarbe: %s \n",
						rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getBoolean(5),
						rs.getString(6));
			} else
			{
				System.out.println("Student mit der ID " + id + " wurde nicht gefunden");
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void printAllUrlaube()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Ort, Preis, Preis * 1.1 FROM Urlaubswunsch";
			ResultSet rs = stmt.executeQuery(query);

			System.out.println("Alle Urlabusorte inkl. Preis: ");

			while (rs.next())
			{
				String ort = rs.getString(1);
				double preis = rs.getDouble(2);
				double preisPlus10Prz = rs.getDouble(3);

				System.out.printf("%s \t  � %.2f \n", ort, preis);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	// Credits von einem bestimmten Studenten sollen ver�ndert werden (2, 2200)
	public int updateStudent(int id, double newcredit)
	{
		int affectedrows = 0;
		try
		{
			String update = "Update Student set credits = ? where studentid = ?";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setDouble(1, newcredit);
			stmt.setInt(2, id);
			affectedrows = stmt.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return affectedrows;
	}

	// ein neuer Student soll hinzugef�gt werden und die neue id zur�ckgeben
	public int addNewStudent(Student a)
	{
		int result = 0;
		try
		{
			String update = "Insert into Student (vorname, nachname, credits, inskribiert, lieblingsfarbe) values (?, ?, ?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setObject(1, a.getVorname());
			stmt.setObject(2, a.getNachname());
			stmt.setObject(3, a.getCredits());
			stmt.setObject(4, a.isInskribiert());
			stmt.setObject(5, a.getLieblingsfarbe());
			stmt.executeUpdate();

			// String query = "Select last(studentid) from student"; // Gefahr besteht, dass
			// gleichzeitig jmd hinzuf�gt
			String query = "Select @@identity"; // der letzte Wert im Feld vom PK wird zur�ckgegeben
			PreparedStatement stmt2 = con.prepareStatement(query);
			ResultSet rs = stmt2.executeQuery();

			while (rs.next())
			{
				result = rs.getInt(1);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return result;
	}

	// alle Studenten die im Vornamen ein "a" haben bekommen n credits mehr
	public int updateStudentWhereFirstnameLike(String vornameLetter, double newcredit)
	{
		int result = 0;
		try
		{
			String update = "Update student set credits = credits + ? where vorname like ?";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setDouble(1, newcredit);
			stmt.setString(2, vornameLetter);
			;
			result = stmt.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public void insertUrlaub(String destination, double preis)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public void insertUrlaub(Urlaub urlaub)
	{
		// insert into MeineUrlaube(Ziel, price) .....
	}

	public int deleteStudent(int id)
	{
		int result = 0;
		try
		{
			String update = "delete from student where studentid = ?";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setInt(1, id);
			result = stmt.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return result;
	}

	// man soll die Metadaten in einer HashMap zur�ckbekommen
	public HashMap<String, String> getColumnNamesForTableStudentFromMetadata()
	{
		HashMap<String, String> tabelle = new HashMap<>();

		try
		{
			ResultSet rs = con.prepareStatement("SELECT * FROM student").executeQuery();
			ResultSetMetaData meta = rs.getMetaData();

			for (int i = 1; i <= meta.getColumnCount(); i++)
			{
				tabelle.put(meta.getColumnLabel(i), meta.getColumnTypeName(i));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return tabelle;
	}

	// 2 Abfragen, 1. Details zum Studenten und 2. zu seinen Urlaubsw�nschen
	public Student getStudentWithUrlaubsDetails(int id)
	{
		Student s = null;
		Urlaub u = null;

		try
		{
			String query = "SELECT StudentID, Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe, Ort, Reihung, Preis FROM Student FULL OUTER JOIN Urlaubswunsch ON Student.studentID = urlaubswunsch.studentID where studentid = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				if (s == null)
				{
					s = new Student(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getBoolean(5),
							rs.getString(6));
				}
				u = new Urlaub(rs.getString(7), rs.getInt(8), rs.getDouble(9));
				s.addUrlaub(u);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return s;
	}

	public void printColumnNamesForTableStudentFromMetadata()
	{
		try
		{
			ResultSet rs = con.prepareStatement("SELECT * FROM student").executeQuery();
			ResultSetMetaData meta = rs.getMetaData();

			for (int i = 1; i <= meta.getColumnCount(); i++)
			{
				System.out.printf("%s %n", meta.getColumnLabel(i));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public ArrayList<Urlaub> GetAlleUrlaubPreisGroesserAls(double preis)
	{
		return null;
	}

	public Student getStudent(int id)
	{
		Student s = null;

		try
		{
			String query = "SELECT StudentID, Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe FROM Student WHERE StudentID=?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				s = new Student(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getBoolean(5),
						rs.getString(6));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return s;
	}

	
}