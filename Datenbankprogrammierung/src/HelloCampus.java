import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HelloCampus
{

	public static void main(String[] args)
	{
		System.out.println("Hello Campus");
		
		try
		{
			//1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		}
		
			catch (ClassNotFoundException cnfex)
			{
			cnfex.printStackTrace();
			
		}
		try //Versuche das
		{
			//2. get Connection to database
			Connection con = DriverManager.getConnection("jdbc:ucanaccess://C://Users//michaela.leskoschek//Documents//Nowa//Datenbanken Programmieren/Urlaubsverwaltung.accdb");
			System.out.println("Hat funktioniert");
		}
		
		//Statement stmt = con.createStatement();
		catch (SQLException e) // wenn nicht, macht das
		{
			e.printStackTrace();
		}
		try
		{
			demoEins();
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	public static void demoEins () throws Exception //Zwang - der Aufrufer muss ein Try-Catch verwenden
	{
		System.out.println("Hello World");
		
		int i = 10, j;
		j= 0;
		int erg = 0;
		
		try
		{
			erg = i / j;
		} catch (Exception e)
		{
			//Fehlerbehandlung
			System.out.println("Exception - das hat nicht funktioniert");
		}
		System.out.println(erg);
	}
	
}

