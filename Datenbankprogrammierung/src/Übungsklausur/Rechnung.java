package Übungsklausur;

public class Rechnung {
	
	private int reNr;
	private String datum;
	private double gesamtbetrag;
	private int kdnr;
	
	
	public Rechnung(int reNr, String datum, double gesamtbetrag, int kdnr)
	{
		
		this.reNr = reNr;
		this.datum = datum;
		this.gesamtbetrag = gesamtbetrag;
		this.kdnr = kdnr;
	}

	public Rechnung(String datum, double gesamtbetrag, int kdnr)
	{
		this.datum = datum;
		this.gesamtbetrag = gesamtbetrag;
		this.kdnr = kdnr;
	}

	public int getKdnr()
	{
		return kdnr;
	}

	public void setKdnr(int kdnr)
	{
		this.kdnr = kdnr;
	}

	public int getReNr()
	{
		return reNr;
	}


	public void setReNr(int reNr)
	{
		this.reNr = reNr;
	}


	public String getDatum()
	{
		return datum;
	}


	public void setDatum(String datum)
	{
		this.datum = datum;
	}


	public double getGesamtbetrag()
	{
		return gesamtbetrag;
	}


	public void setGesamtbetrag(double gesamtbetrag)
	{
		this.gesamtbetrag = gesamtbetrag;
	}


	@Override
	public String toString()
	{
		return "ReNr.: " + reNr + ", Datum: " + datum + ", Gesamtbetrag: " + gesamtbetrag + "\n";
	}
	
	
	
	
	

}
