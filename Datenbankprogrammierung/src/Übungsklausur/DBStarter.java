package �bungsklausur;

public class DBStarter {

	public static void main(String[] args) {
		
		DBHelper helper = new DBHelper();
		
		Kunde k = new Kunde("Susanne", "Mustermann", "Mann", 38);
		Kunde k1 = new Kunde(14, "Max8", "Mustermann", "Mann", 40);
		Rechnung r = new Rechnung("06.06.2016", 2000, 2);
		Kunde k4 = new Kunde("Monika", "Mustermann", "Mann", 38);
		
		helper.init();
		
		
//		System.out.println(helper.getKunde(1));
//		System.out.println("-------\n");
//		
//		System.out.println(helper.getAlleKunden());
//		System.out.println("-------\n");
//		
//		System.out.println("Neuer Kunde wurde hinzugef�gt mit der KdNr.: " + helper.insertKunde(k4));
//		System.out.println("-------\n");
//		
//		k1.setVorname("Hubert");
//		k1.setBonuspunkte(50);
//		System.out.println("Kunde mit der Kundennr.: " + k1.getKdnr() + " wurde ge�ndert");
//		helper.updateKunde(k1);
//		System.out.println("-------\n");
//		
//		System.out.println("Rechnung wurde zu Kunde " + k1.getKdnr() + " hinzugef�gt\n" + "mit der ReNr.: " + helper.insertRechnung(r, k1));
//		System.out.println("-------\n");
//		
//		r.setReNr(2);
//		r.setGesamtbetrag(7000);
//		System.out.println("Rechnung mit der ReNr.: " + r.getReNr() + " wurde ge�ndert");
//		helper.updateRechnung(r);
//		System.out.println("---------\n");
//		
//		System.out.println(helper.getRechnungenByKunde(1));
//		System.out.println("----------\n");
//		
//		System.out.println("Liste aller weiblichen Kunden: " + helper.getWeiblicheKunden());
//		System.out.println("----------\n");
//		
//		
//		System.out.println("Kunde mit den meisten Bonuspunkten ist: " + helper.getKundeMitDenMeistenBonusPunkten());
//		System.out.println("---------\n");
//		

//		k4.setKdnr(26);
//		System.out.println("Kunde mit der KdNr.: " + k4.getKdnr() + " wurde gel�scht");
//		helper.loescheAlleReschnungenUndDanachDenKunden(k4);
//		System.out.println("--------------\n");
		
//		helper.demoTransaktion();
//		System.out.println("-------------\n");
////		
		helper.close();
		
		

	}

}
