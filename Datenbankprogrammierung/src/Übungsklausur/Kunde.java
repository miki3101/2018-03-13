package Übungsklausur;

public class Kunde {
	
	private int kdnr;
	private String vorname;
	private String nachname;
	private String geschlecht;
	private int bonuspunkte;
	
	public Kunde(int kdnr, String vorname, String nachname, String geschlecht, int bonuspunkte)
	{
		
		this.kdnr = kdnr;
		this.vorname = vorname;
		this.nachname = nachname;
		this.geschlecht = geschlecht;
		this.bonuspunkte = bonuspunkte;
	}
	
	public Kunde(String vorname, String nachname, String geschlecht, int bonuspunkte)
	{
		this.vorname = vorname;
		this.nachname = nachname;
		this.geschlecht = geschlecht;
		this.bonuspunkte = bonuspunkte;
	}
	

	public int getKdnr()
	{
		return kdnr;
	}

	public void setKdnr(int kdnr)
	{
		this.kdnr = kdnr;
	}

	public String getVorname()
	{
		return vorname;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}

	public String getNachname()
	{
		return nachname;
	}

	public void setNachname(String nachname)
	{
		this.nachname = nachname;
	}

	public String getGeschlecht()
	{
		return geschlecht;
	}

	public void setGeschlecht(String geschlecht)
	{
		this.geschlecht = geschlecht;
	}

	public int getBonuspunkte()
	{
		return bonuspunkte;
	}

	public void setBonuspunkte(int bonuspunkte)
	{
		this.bonuspunkte = bonuspunkte;
	}

	@Override
	public String toString()
	{
		return "KDNR: " + kdnr + ", Vorname: " + vorname + ", Nachname: " + nachname + ", Geschlecht: " + geschlecht
				+ ", Bonuspunkte: " + bonuspunkte + "\n";
	}
	
	
	
	

}
