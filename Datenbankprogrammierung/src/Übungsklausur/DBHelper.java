package �bungsklausur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBHelper
{

	private Connection con = null;

	public void init()
	{

		try
		{
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			con = DriverManager.getConnection("jdbc:ucanaccess://"
					+ "C://Users//michaela.leskoschek//Documents//Nowa//Datenbanken Programmieren/�bungsklausur.accdb");

			System.out.println("Sie sind mit der Datenbank verbunden \n");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
				System.out.println("Verbindung zur Datenbank wurde getrennt");

			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	public Kunde getKunde(int kdnr)
	{
		Kunde k = null;

		try
		{
			String query = "SELECT kdnr, Vorname, Nachname, Geschlecht, Bonuspunkte FROM Kunden WHERE kdnr=?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, kdnr);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				k = new Kunde(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return k;
	}

	public List<Kunde> getAlleKunden()
	{
		List<Kunde> kunden = new ArrayList();
		Kunde k = null;

		try
		{

			String query = "SELECT kdnr, Vorname, Nachname,  Geschlecht, Bonuspunkte FROM Kunden";
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				k = new Kunde(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
				kunden.add(k);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return kunden;
	}

	public int insertKunde(Kunde k)
	{
		int neueKdnr = 0;

		try
		{
			String update = "Insert into kunden (vorname, nachname, geschlecht, bonuspunkte) values (?, ?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setObject(1, k.getVorname());
			stmt.setObject(2, k.getNachname());
			stmt.setObject(3, k.getGeschlecht());
			stmt.setObject(4, k.getBonuspunkte());
			stmt.executeUpdate();

			String query = "Select @@identity";
			PreparedStatement stmt2 = con.prepareStatement(query);
			ResultSet rs = stmt2.executeQuery();

			while (rs.next())
			{
				neueKdnr = rs.getInt(1);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return neueKdnr;
	}

	public void updateKunde(Kunde k)
	{
		try
		{
			String update = "Update kunden set vorname=?, nachname=?, geschlecht=?, bonuspunkte=? where KDNR=?";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setString(1, k.getVorname());
			stmt.setString(2, k.getNachname());
			stmt.setString(3, k.getGeschlecht());
			stmt.setInt(4, k.getBonuspunkte());
			stmt.setInt(5, k.getKdnr());
			stmt.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public int insertRechnung(Rechnung r, Kunde k)
	{
		int neueReNr = 0;

		try
		{
			String update = "Insert into rechnungen (datum, gesamtbetrag, kdnr) values (?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setObject(1, r.getDatum());
			stmt.setObject(2, r.getGesamtbetrag());
			stmt.setObject(3, k.getKdnr());
			stmt.executeUpdate();

			String query = "Select @@identity";
			PreparedStatement stmt2 = con.prepareStatement(query);
			ResultSet rs = stmt2.executeQuery();

			while (rs.next())
			{
				neueReNr = rs.getInt(1);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return neueReNr;
	}

	public void updateRechnung(Rechnung r)
	{
		try
		{
			String update = "Update rechnungen set datum=?, gesamtbetrag=?, kdnr=? where renr=?";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setString(1, r.getDatum());
			stmt.setDouble(2, r.getGesamtbetrag());
			stmt.setInt(3, r.getKdnr());
			stmt.setInt(4, r.getReNr());
			stmt.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public List<Rechnung> getRechnungenByKunde(int kdnr)
	{
		ArrayList<Rechnung> rechnungen = new ArrayList<>();
		Rechnung r = null;

		try
		{
			String query = "SELECT ReNr, datum, gesamtbetrag, kdnr FROM rechnungen where kdnr = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, kdnr);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				r = new Rechnung(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getInt(4));
				rechnungen.add(r);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return rechnungen;
	}

	public ArrayList<Kunde> getWeiblicheKunden()
	{
		ArrayList<Kunde> kunden = new ArrayList<>();
		Kunde k = null;

		try
		{

			String query = "SELECT kdnr, Vorname, Nachname,  Geschlecht, Bonuspunkte FROM Kunden where geschlecht = 'Frau'";
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				k = new Kunde(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
				kunden.add(k);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return kunden;
	}

	public Kunde getKundeMitDenMeistenBonusPunkten()
	{
		// String query = "Select Top 1 Kdnr from kunden order by bonuspunkte desc"
		// String query = "Select * from kunden where bonuspunkte == (Select Max(Bonusbunkte) from Kunden))"
		
		ArrayList<Kunde> kunde = (ArrayList<Kunde>) getAlleKunden();

		Kunde k = kunde.get(0);

		for (Kunde kunde2 : kunde)
		{
			if (kunde2.getBonuspunkte() > k.getBonuspunkte())
				k = kunde2;
		}

		return k;
	}

	public void loescheAlleReschnungenUndDanachDenKunden(Kunde k)
	{
		try
		{
			String delete = "Delete  from rechnungen where kdnr =?";
			PreparedStatement stmt = con.prepareStatement(delete);
			stmt.setInt(1, k.getKdnr());
			stmt.executeUpdate();

			String delete2 = "Delete  from kunden where kdnr =?";
			PreparedStatement stmt2 = con.prepareStatement(delete2);
			stmt2.setInt(1, k.getKdnr());
			stmt2.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void demoTransaktion()
	{
		try
		{
			con.setAutoCommit(false); // verhindert autom. Speichern nach jedem execute update

			String update = "update kunden set bonuspunkte = bonuspunkte + 10 where kdnr = 1";
			String update2 = "update kunden set bonuspunkte = bonuspunkte - 10 where kdnr = 2";
			
			// oder cons.setAutoCommit(false);
			
			PreparedStatement stmt = con.prepareStatement(update);
			int affectedrow1 = stmt.executeUpdate();

			PreparedStatement stmt2 = con.prepareStatement(update2);
			int affectedrow2 = stmt2.executeUpdate();

			
			// oder con.commit(); statt if und else if
			
			if (affectedrow1 == 1 && affectedrow2 == 1)
			{
				con.commit(); // erst jetzt wird gespeichert
				System.out.println("Transaktion hat funktioniert!");
			}

			else if (affectedrow1 == 0 && affectedrow2 == 0)
			{
				con.rollback(); // macht alles r�ckg�ngig
				System.out.println("Transaktion hat nicht funktioniert");
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
