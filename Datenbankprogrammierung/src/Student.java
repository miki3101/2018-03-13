import java.util.ArrayList;

public class Student
{
	private int studentID;
	private String vorname;
	private String nachname;
	private double credits;
	private boolean inskribiert;
	private String lieblingsfarbe;
	private ArrayList<Urlaub> urlaube = new ArrayList<>();

	public Student(int studentID, String vorname, String nachname, double credits, boolean inskribiert,
			String lieblingsfarbe)
	{	
		this.studentID = studentID;
		this.vorname = vorname;
		this.nachname = nachname;
		this.credits = credits;
		this.inskribiert = inskribiert;
		this.lieblingsfarbe = lieblingsfarbe;
	}
	
	public Student(int studentID, String vorname, String nachname, double credits, boolean inskribiert,
			String lieblingsfarbe, ArrayList<Urlaub> urlaube)
	{	
		this.studentID = studentID;
		this.vorname = vorname;
		this.nachname = nachname;
		this.credits = credits;
		this.inskribiert = inskribiert;
		this.lieblingsfarbe = lieblingsfarbe;
		this.urlaube = urlaube;
	}
	
	public void addUrlaub(Urlaub u)
	{
		urlaube.add(u);
	}
	
	
	public ArrayList<Urlaub> getUrlaube() {
		return urlaube;
	}

	public void setUrlaube(ArrayList<Urlaub> urlaube) {
		this.urlaube = urlaube;
	}

	public Student(String vorname, String nachname, double credits, boolean inskribiert,
			String lieblingsfarbe)
	{	
		this.vorname = vorname;
		this.nachname = nachname;
		this.credits = credits;
		this.inskribiert = inskribiert;
		this.lieblingsfarbe = lieblingsfarbe;
	}
	

	public int getStudentID() {
		return studentID;
	}
	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public double getCredits() {
		return credits;
	}
	public void setCredits(double credits) {
		this.credits = credits;
	}
	public boolean isInskribiert() {
		return inskribiert;
	}
	public void setInskribiert(boolean inskribiert) {
		this.inskribiert = inskribiert;
	}
	public String getLieblingsfarbe() {
		return lieblingsfarbe;
	}
	public void setLieblingsfarbe(String lieblingsfarbe) {
		this.lieblingsfarbe = lieblingsfarbe;
	}
	public String toString()
	{
		return "StudentID: " + studentID + ", Vorname: " + vorname + ", Nachname: " + nachname + ", Credits: "
				+ credits + ", Inskribiert: " + inskribiert + ", Lieblingsfarbe: " + lieblingsfarbe + "\nUrlaubswŁnsche: " + urlaube;
	}
	

}
