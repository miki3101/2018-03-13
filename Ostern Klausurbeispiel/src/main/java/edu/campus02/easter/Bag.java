package edu.campus02.easter;

import java.awt.Color;
import java.util.ArrayList;

public class Bag
{

	ArrayList<Egg> bag = new ArrayList<>();

	public void putEgg(Egg egg)
	{

		bag.add(egg);

	}

	public int countEggs()
	{

		int zaehler = 0;

		for (int i = 0; i < bag.size(); i++)
		{
			zaehler++;
		}

		return zaehler;
	}

	public double sumWeight()
	{

		double summe = 0;

		for (Egg egg : bag)
		{
			summe = summe + egg.getWeight();
		}

		return summe;
	}

	public double sumWeight(String color)
	{

		double summe = 0;
		for (Egg egg : bag)
		{
			if (color == egg.getColor())
			{
				summe = summe + egg.getWeight();
			}
		}

		return summe;
	}

	public int countDifferentColors()
	{

		int zaehler = 0;
		
		ArrayList<String> farben = new ArrayList<>();

		for (Egg egg : bag)
		{
			
			if (!farben.contains(egg.getColor()))
			{
				farben.add(egg.getColor());
			}
		}
		zaehler = farben.size();

		return zaehler;
	}

}
