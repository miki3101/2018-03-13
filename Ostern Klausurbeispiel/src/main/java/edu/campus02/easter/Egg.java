package edu.campus02.easter;

public class Egg {

	private double weight;
	private String color;
	
	
	public Egg(double weight) {
		
		this.weight = weight;
		this.color = "weiss";
	}

	public Egg(double weight, String color) {
		
		this.weight = weight;
		this.color = color;
	}

	public double getWeight() {
		return weight;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		
		this.color = color;
	}

	public String toString()
	{
		return   String.format("(%.2f %s)", weight, color);
	}
	
	

}
