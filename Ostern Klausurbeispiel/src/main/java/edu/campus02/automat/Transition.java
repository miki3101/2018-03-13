package edu.campus02.automat;

public class Transition {
	
	private String trigger;
	private State target;

	public Transition(String trigger, State target) {
		
		this.trigger = trigger;
		this.target = target;
	}

	public State getTarget() {
		return target;
	}

	public boolean match(char c) {
		
		char trigger1 [] = trigger.toCharArray();
		boolean match = false;
		
		for (char d : trigger1)
		{
			if (c == d)
			{
				match = true;
			}
		}
	
		return match;
	}
	
	
	
	public String toString()
	{
		return "[" +  trigger + "]" + "->" +  target;
	}

}
