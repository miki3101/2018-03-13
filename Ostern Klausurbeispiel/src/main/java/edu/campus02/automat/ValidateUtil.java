package edu.campus02.automat;

public class ValidateUtil
{

	
	
	public static boolean verify(State start, String word)
	{
		State zustand = start;
		

		for (int i = 0; i < word.length(); i++)
		{
			char buchstabe = word.charAt(i);
			Transition x = zustand.findMatchingTransition(buchstabe);
			
			if (x==null)
			{
				return false;
			}
			
			zustand = x.getTarget();
		}

		return zustand.isFinal();
	}

	public static boolean verifyRekursive(State state, String word)
	{
		boolean ergebnis = false;
		
		if (word.length() == 0)
		{
			return state.isFinal();	
		}
		
		Transition matchingTransition = state.findMatchingTransition(word.charAt(0));
	
		if(matchingTransition==null)
			return false;
		
		ergebnis = verifyRekursive(matchingTransition.getTarget(), word.substring(1));

		return ergebnis;
	}

}
