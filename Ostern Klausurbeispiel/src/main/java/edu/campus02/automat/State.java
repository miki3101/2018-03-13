package edu.campus02.automat;

import java.util.ArrayList;

public class State {
	
	private String name;
	private boolean fina;
	ArrayList<Transition> x = new ArrayList <>();
	
	public State(String name) {
		
		this.name = name;
	}

	public State(String name, boolean fina) {
		
		this.name = name;
		this.fina = fina;
	}

	public boolean isFinal() {
		
		return fina;
	}

	public void addTransition(Transition tran) {
		
		
		x.add(tran);
		
	}

	public Transition findMatchingTransition(char c) {
		
		for (Transition transition : x)
		{
			if(transition.match(c))
			{
				return transition;
			}
		}
		
		
		return null;
	}
	
	public String toString()
	{
		if(x.isEmpty())
		{
			return "(" + name + "," + fina + ")";
		}
		
		return "(" + name + "," + fina  + "," + x + ")";
	}

}
