package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientRunnable implements Runnable
{

	private Socket client;
	private static float ds;
	private static int summe = 0;
	private static int counter = 0;

	public ClientRunnable(Socket client)
	{
		super();
		this.client = client;
	}

	public void run()
	{
		try (
				PrintWriter pw = new PrintWriter(client.getOutputStream());
				BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));)
		{
			while (true)
			{
				pw.println("Bitte geben Sie eine Zahl ein");
				pw.flush();

				String line;

				while ((line = br.readLine()) != null)
				{
					try
					{
						synchronized (ClientRunnable.class) // das teilen sich die Clients, deshalb synchronized
						{
							this.summe += Integer.parseInt(line);
							this.counter++;
							this.ds = (float) summe / counter;
						}

						pw.println("Der aktuelle Durchschnitt betr�gt: " + ds);

					} catch (NumberFormatException e)
					{
						pw.println("Keine g�ltige Zahl �bertragen!");
					}
					pw.flush();

					System.out.println("Counter: " + counter);
					System.out.println("Summe: " + summe);
					System.out.println("Durchschnitt: " + ds);
					System.out.println("-------------");
				}
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
		try	// muss nicht unbedingt sein
		{
			client.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
