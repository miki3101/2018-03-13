package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server
{
	public static void main(String[] args)
	{
		
		try(ServerSocket server = new ServerSocket(4000);)
		{	
			while (true)
			{
				Socket socket = server.accept();
				System.out.println("Verbindung zu Client: " + socket + " hergestellt");

				ClientRunnable cr = new ClientRunnable(socket);

				Thread t = new Thread(cr);
				t.start();
				
			}	
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
}
