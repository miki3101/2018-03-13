package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class UrlAusgeben
{

	public static void main(String[] args) throws IOException
	{
		URL u = new URL("http://orf.at/");
		BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream()));
		
		String test;
		
		try
		{
			while((test = br.readLine()) != null)
			{
				System.out.println(test);
			}
			
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
