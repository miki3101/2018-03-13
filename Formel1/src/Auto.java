
public class Auto
{
	private String farbe; // gilt nur innerhalb dieser Klasse (private - kann von au�en nicht darauf
							// zugreifen) string = Zeichenkette
	private double tankstand;
	private double geschwindigkeit;

	public Auto(String autofarbe) // ruf ich immer auf wenn ich ein neues Objekt (Auto) habe
	{
		farbe = autofarbe;
	}

	public void volltanken() // Methode f�rs Volltanken
	{
		tankstand = 100; // kann ich verwenden, weil ich sie in der ganzen Klasse schon definiert habe

	}

	public void beschleunigen(double gas) // weil ich es von aussen mitgeben will, deshalb einen Parameter hinzuf�gen
											// (gas)
	{ // liefert nichts zur�ck, aber ver�ndert eine Eigenschaft
		geschwindigkeit = geschwindigkeit + gas;

		if (geschwindigkeit > 350) // gas geben bis ich auf max bin
		{
			geschwindigkeit = 350; // max Geschwindigkeit ist 350
		}
		checkStatus();
	}

	public void langsamer(double bremsen) // bremsen gebe ich von au�en mit und liefert nichts zur�ck
	{
		geschwindigkeit = geschwindigkeit - bremsen;

		if (geschwindigkeit <= 0) // bremsen, bis ich stehe
		{
			geschwindigkeit = 0; // wenn ich bremse, dann bis ich stehe
		}
		checkStatus();
	}

	public void fahren(double zeit) // zeit gebe ich von au�en mit in sekunden
	{
		tankstand = tankstand - (geschwindigkeit * zeit) / 70000;
		
		checkStatus();
	}

	public void status()
	{
		System.out.println(farbe + " " + geschwindigkeit + " " + tankstand);

	}
	
	private void checkStatus() //kann nur von Objekten benutzt werden die in der Klasse sind (Auto)
	{
		if (tankstand <= 0)
		{
			tankstand = 0;
			geschwindigkeit = 0;
		}
	}
}
