package org.campus02.social;

import java.util.ArrayList;

public class User {
	
	private Integer id;
	private String name;
	private Integer age;
	private ArrayList<User> friends = new ArrayList<>();
	
	public User(Integer id, String name, Integer age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((friends == null) ? 0 : friends.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (age == null)
		{
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (friends == null)
		{
			if (other.friends != null)
				return false;
		} else if (!friends.equals(other.friends))
			return false;
		if (id == null)
		{
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "User ID: " + id + ", Name: " + name + ", Age: " + age + " Anzahl der Freunde: " + friends.size() + "\n";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public ArrayList<User> getFriends() {
		return new ArrayList<>(friends);
	}

	public void setFriends(ArrayList<User> friends) {
		this.friends = friends;
	}
	
	//TODO 1: fügen Sie der Klasse eine toString Methode
	//wie in der Angabe beschrieben hinzu 

	//TODO 2: erstellen Sie geeignete equals & hashCode Methoden
	//welche die lt. Angabe definierte Gleichheitsregel erfüllen
	
}
