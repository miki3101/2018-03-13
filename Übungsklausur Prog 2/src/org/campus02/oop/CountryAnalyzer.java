package org.campus02.oop;

import java.util.HashMap;

public class CountryAnalyzer extends PersonAnalyzer
{

	
	private HashMap<String, Integer> result;
	
	public CountryAnalyzer()
	{
		result = new HashMap<String, Integer>();
	}
	
	
	public HashMap<String, Integer> getResult()
	{
		return result;
	}


	public void analyse()
	{
		for (Person person : persons)
		{
			
			if(result.containsKey(person.getCountry()))
			{
					int zahl = result.get(person.getCountry());
					zahl++;
					result.put(person.getCountry(), zahl);
			}
			else {
				result.put(person.getCountry(), 1);
			}
		}
		
	}

	
}
