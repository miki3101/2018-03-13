package org.campus02.oop;

import java.util.ArrayList;

public class PersonManager
{
	private ArrayList<Person> persons;
	
	
	public PersonManager()
	{
		persons = new ArrayList<Person>();
	}

	public void add(Person p)
	{
		persons.add(p);
	}
	
	public void doAnalysis(PersonAnalyzer a)
	{
		a.setPersons(persons);
		a.analyse();
	}

	@Override
	public String toString()
	{
		return "PersonManager [persons=" + persons + "]";
	}
	
	
}
