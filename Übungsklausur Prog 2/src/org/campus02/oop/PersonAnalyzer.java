package org.campus02.oop;

import java.util.ArrayList;

public abstract class PersonAnalyzer
{
	protected ArrayList<Person> persons;
	
	
	public PersonAnalyzer()
	{
	
		persons = new ArrayList<Person>();
		
	}

	public void setPersons(ArrayList<Person> persons)
	{
		this.persons = persons;
	}

	public abstract void analyse();

	@Override
	public String toString()
	{
		return "PersonAnalyzer [persons=" + persons + "]";
	}
	
	
	
}
