package at.campus02.nowa.swe;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = null;

		try {
			scanner = new Scanner(System.in);

			while (true) {
				int shapeToPrint = 0;
				while (shapeToPrint < 1 || shapeToPrint > 6) {
					System.out.println("Bitte w�hlen Sie eine Form aus [1-6]: ");
					shapeToPrint = scanner.nextInt();
				}

				Shape shape = null;
				
				if (shapeToPrint == 1)
					shape = new ShapeA();
				
				else if (shapeToPrint == 2)
					shape = new ShapeB();
				
				else if (shapeToPrint == 3)
					shape = new ShapeC();
				
				else if (shapeToPrint == 4)
					shape = new ShapeD();
				
				else if (shapeToPrint == 5)
					shape = new ShapeE();
				
				else if (shapeToPrint == 6)
					shape = new ShapeF();

				System.out.println(shape.toString());
			}
		}finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
}
