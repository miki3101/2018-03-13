package at.campus02.nowa.swe;

public class ShapeB extends Shape{

	protected ShapeB() {
		chars[0][0] = 'x';
		chars[0][1] = 'x';
		chars[0][2] = 'x';
		
		chars[1][0] = ' ';
		chars[1][1] = ' ';
		chars[1][2] = 'x';
		
		chars[2][0] = ' ';
		chars[2][1] = ' ';
		chars[2][2] = ' ';
	}
}
