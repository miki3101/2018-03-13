package at.campus02.nowa.swe;

public abstract class Shape {
	protected char[][] chars = new char[3][3];
	
	public String toString() {
		String result = "";
		for (int y = 0; y < chars.length; y++) {
			for (int x = 0; x < chars[0].length; x++) {
				result += chars[x][y];
			}
			result += "\n";
		}
		return result;
	}
}
