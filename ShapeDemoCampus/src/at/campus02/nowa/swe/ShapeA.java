package at.campus02.nowa.swe;

public class ShapeA extends Shape{

	protected ShapeA() {
		chars[0][0] = 'x';
		chars[0][1] = 'x';
		chars[0][2] = 'x';
		
		chars[1][0] = ' ';
		chars[1][1] = 'x';
		chars[1][2] = ' ';
		
		chars[2][0] = 'x';
		chars[2][1] = 'x';
		chars[2][2] = 'x';
	}
}
