package org.campus02.oop;

public class MaxSalaryAnalyzer extends PersonAnalyzer
{

	public MaxSalaryAnalyzer()
	{
		
	}
	
	public void analyse()
	{
		int maxsalary = 0;
		
		for (Person person : persons)
		{
		
			if(person.getSalary()> maxsalary)
				maxsalary = person.getSalary();
				
		}
		for (Person person : persons)
		{
			if(person.getSalary() == maxsalary)
				System.out.println(person);
		}
	}

}
