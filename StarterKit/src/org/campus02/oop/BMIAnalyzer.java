package org.campus02.oop;

import java.util.ArrayList;

public class BMIAnalyzer extends PersonAnalyzer
{
	private ArrayList<Pair<Person, Double>> result;
	
	public BMIAnalyzer()
	{
		result = new ArrayList<Pair<Person, Double>>();
	}
	
	private double calcBMI(Person p)
	{
		double bmi = p.getWeight() /(p.getSize()/ 100.0) * (p.getSize()/100.0);
		
		return bmi;
	}
	
	public void analyse()
	{
			
		for (Person person : persons)
		{
			System.out.println(calcBMI(person));
		
			result.add(new Pair<Person, Double> (person, calcBMI(person)));
		}
		
	}

	public ArrayList<Pair<Person, Double>> getResult()
	{
		return result;
	}

	public void setResult(ArrayList<Pair<Person, Double>> result)
	{
		this.result = result;
	}

	@Override
	public String toString()
	{
		return "BMIAnalyzer [result=" + result + "]";
	}
	
	
}
