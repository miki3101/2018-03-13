package org.campus02.oop;


public class GenderAnalyzer extends PersonAnalyzer
{

	public GenderAnalyzer()
	{
		
	}

	public void analyse()
	{
		int male = 0;
		int maleSize = 0;
		int female = 0;
		int femaleSize = 0;
		
		for (Person person : persons)
		{
			if(person.getGender() == 'M')
			{
				male++;
				maleSize += person.getSize();
			}
			
			else if (person.getGender() == 'F')
			{
				female++;
				femaleSize += person.getSize();
			}
		}
		
		System.out.println(male + " M�nner mit einer durchschnittlicher Gr��e von: " +  maleSize / male + " cm");
		System.out.println(female + " Frauen mit einer durchschnittlichen Gr��e von: " + femaleSize / female + " cm");
		
	}

	
	
}
