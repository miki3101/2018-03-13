package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class PingPongServer
{

	public static void main(String[] args)
	{
		try
		{
			ServerSocket server = new ServerSocket(1111); // jede IP-Adresse nimmt auf dem Port an

			while (true)
			{
				Socket socket = server.accept(); // warte bis Verbindung zustande kommt
				System.out.println("Accepted: " + socket);
				
				
				try(// damit kann man die Daten die vom Client kommen als Stream zu empfangen (zeilenweise lesen)
					BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
					// damit kann ich eine Antwort an den Client schicken
					)
				{
					String line; // Zeilenbuffer
					
					while((line = br.readLine()) != null) // schreibt solange bis nichts mehr da ist
					{
						System.out.println("Received: " + line); // sollte man nicht machen, k�nnte gef�hrlich sein
						// in der Netzwerkkommunikation ist es immer besser switch/case statt if zu nehmen (aber eigentlich egal)
						switch (line.toLowerCase()) // um alles was reinkommt klein geschrieben wird und keine Fehlermeldung kommt 
						{
						case "pong":			// wenn ein pong kommt
							pw.println("ping");	// schicken wir ein ping zur�ck
							break;

						case "ping":			// wenn ein ping kommt
							pw.println("pong");	// schicken wir ein pong zur�ck
							break;
						default:
							pw.println("what?"); // wenn etwas anderes kommt schicken wir what zur�ck
							break;
						}
						pw.flush(); // damit zwinge ich den Server ein Paket abzuschicken, auch wenn es noch nicht voll ist (1400 Byte)
					}
				}
			}

		}catch(SocketException e) // damit kann ich verhindern, da� der Server abst�rzt sobald der Client die Verbindung trennt
		{
			System.out.println("Client disconnnected");
		}
		
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
