package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class PingPongClient
{

	public static void main(String[] args)
	{
		try (	// Client Reader ist mit dem ServerWriter verbunden und
				// Client Writer ist mit dem ServerReader verbunden
				Socket socket = new Socket("172.32.0.213", 1111);
				BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
				BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));)
				// um von der Console einlesen zu k�nnen
		{	
			String line;
			
			while((line = brc.readLine()) != null) // warte bis jemand was auf der Console eingegeben hat
			{
				if(line.equals("Exit")) // wenn Exit eingegeben wird, beendet er die while Schleife und trennt die Verbindung zum Server und beendet das Programm
				{
					break;
				}
				//socket.close(); // wenn die Schleife beendet ist, wird der Socket geschlossen, muss aber nicht sein, da er oben im Autoclose ist
				
				pw.println(line); // wird an den Server geschickt, zeilenumbruch muss geschickt werden, ansonsten wird alles im Buffer gespeichert,
								// bis er einen Zeilenumbruch hat, erst dann wird abgeschickt
				pw.flush(); // gleich flushen, nach dem wir reingeschrieben haben, damit es abgeschickt wird
				// w�rde man das nicht machen, w�rde das Programm stecken bleiben
				
				// weil er in der n�chsten Zeile stehen bleibt (Codeblocking)
				System.out.println(br.readLine()); // dann warte ich solange bis der Server etwas zur�ck geschickt hat				
			}
			

		} catch (UnknownHostException e)
		{
			e.printStackTrace();
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
