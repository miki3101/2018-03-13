package Multithreading;

public class RunnableMultithreadSleep
{

	public static void main(String[] args) throws InterruptedException
	{
		// ThreadPools (Sammlung von Threads) um zu vermeiden dass ich zu viele Threads starte
		// Anzahl kann man am Anfang definieren
		// ThreadPools haben eine Queue in die ich Runnables rein geben
		// somit kann ich immer in die Queue reinschauen ob ein freier Thread vorhanden ist,
		// der wird verwendet und dann wieder freigegeben
		// mit Runnable kann man mehr in Java machen, daher vorteilhafter
		
		TimerRunnable r1 = new TimerRunnable();
		TimerRunnable r2 = new TimerRunnable();
		TimerRunnable r3 = new TimerRunnable();
		TimerRunnable r4 = new TimerRunnable();
		
		
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		Thread t3 = new Thread(r3);
		Thread t4 = new Thread(r4);
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		
		Thread.sleep(10000);
		
		r1.requestStop(); // sch�nerer Stop
		r2.requestStop();
		r3.requestStop();
		r4.requestStop();
		
		
		
		
		
		
		
	}

}
