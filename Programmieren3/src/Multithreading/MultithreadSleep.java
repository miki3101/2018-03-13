package Multithreading;

public class MultithreadSleep
{

	public static void main(String[] args) throws InterruptedException
	{
		Thread t1 = new TimerThread();
		Thread t2 = new TimerThread();
		Thread t3 = new TimerThread();
		Thread t4 = new TimerThread();

		t1.start(); // man kann einen Thread immer nur einmal starten, wenn gestoppt, muss man einen neuen erzeugen
		t2.start();
		t3.start();
		t4.start();

		Thread.sleep(1000); // Hauptthread schlafen legen f�r (10sek)

		t1.stop(); // beendet einen Thread sehr brutal, daher normalerweise nicht verwenden
		Thread.sleep(1000); // warte 10sek und dann stoppe den n�chsten
		t2.stop();
		Thread.sleep(1000);
		t3.stop();
		Thread.sleep(1000);
		t4.stop();
		
		System.out.println("Fertig");
		

	}

}
