package Multithreading;

public class TimerRunnable implements Runnable
{
	
	private boolean isRunning = true;

	
	public void requestStop()
	{
		this.isRunning = false;
	}
	
	public void run()
	{
		while(isRunning)
		{
			System.out.println("Runnable: " + Thread.currentThread().getId());
			
			try
			{
				Thread.sleep(200);
				
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
}
