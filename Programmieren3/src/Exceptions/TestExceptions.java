package Exceptions;

import java.io.File;
import java.io.IOException;

public class TestExceptions extends Exception // implementiert Interface Throwable
{
	public static void main(String[] args) throws TestExceptions // f�llt ein anderer Fehler muss ich ihn mit throws behandeln
	{
		try 										// f�ngt die Exception
		{
			File f = new File("E:\\test.txt");
			System.out.println(f.getAbsolutePath());
			if (!f.exists())
			{
				f.createNewFile();
				throw new TestExceptions(); 		// einen Fehlerzustand erzeugen
			}
			
		} catch (IOException e) 					// wenn ein Fehler eintritt mach...
		{
			System.out.println("Wir haben einen I/O Fehler festgestellt");
			TestExceptions te = new TestExceptions();
			te.addSuppressed(e);					// wo ist der Fehler tats�chlich gefallen
			throw te;
			
		} catch (TestExceptions e) 					// immer Meldung ausgeben und eventuell gleich beheben oder
		{ 											// wiederholen lassen
			
			System.out.println("Wir haben einen Test Fehler festgestellt");
			
			e.getCause();							// gibt es innerhalb mehrere Exceptions
			e.getMessage();							// vorgegebener Text (in englisch)
			e.getLocalizedMessage();				// vorgegebener Text (localized - in deutsch)
			e.getStackTrace();						// StackTrace wird ausgegeben wo ist der Fehler geflogen
			e.printStackTrace();					// StackTrace wird auf der Konsole ausgegeben
		}

	}

}
