package Quizduell_Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import Quizduell_Messages.Answer;
import Quizduell_Messages.Player;
import Quizduell_Messages.Question;

public class Client
{

	private static int frageZahl = 1;

	public static void main(String[] args)
	{
		try (
				Socket socket = new Socket("localhost", 1111);
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream()); // f�r Server outputStream
																							// zuerst erzeugt werden
																							// (gegengleich zum
																							// ClientRunnable)
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream()); // f�r Server dann erst den
																						// inputStream
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // f�r die Console
		)

		{
			System.out.println("Bitte Spielername eingeben: ");

			String name = br.readLine();
			Player p = new Player(name); // aus dem Namen wird ein Object
			oos.writeObject(p); // dieses Object k�nnen wir an den Server schicken
			oos.flush(); // absenden des Paketes mit dem Object

			while (true)
			{
				Question q = (Question) ois.readObject(); // Frage wird ausgelesen

				System.out.println(("Frage " + frageZahl + ": " + q.getText() + " (Schwierigkeitsstufe: " + q.getLevel()
						+ ")" + "\n")); // Frage wird auf die Console geschrieben
				frageZahl++;

				int prefix = 1;
				Map<Integer, UUID> prefixMap = new HashMap<>(); // es wird in einer Map gespeichert, zahl mit uuid

				for (java.util.Map.Entry<UUID, String> entry : q.getAnswers().entrySet())
				{
					System.out.println((" " + prefix + ": " + entry.getValue())); // Antwortm�glichkeiten und
																					// dazugeh�rige Zahl wird
					prefixMap.put(prefix, entry.getKey()); // auf die Console geschrieben
					prefix++;
				}

				String answer;
				int id;

				while (true)
				{
					answer = br.readLine();
					if (answer == null) // Client schlie�t die Console, diese kann nicht wieder ge�ffnet werden
					{
						System.out.println("Auf Wiedersehen, selber schuld!");
						return;
					}
					try
					{
						id = Integer.parseInt(answer); // Kontrolle ob die Eingabe eine Zahl ist
						if (prefixMap.containsKey(id)) // Kontrolle ob eingegebene Zahl in der Map vorhanden ist
						{
							break;
						}

					} catch (NumberFormatException e) // f�llt wenn er keine Zahl oder eine falsche Zahl eingegeben hat
					{
						System.out.println(("Ung�ltige Eingabe!"));
					}
				}

				Answer a = new Answer(prefixMap.get(id)); // liefert die zur eingegebenen Zahl die passende UUID zur�ck
				oos.writeObject(a);
				oos.flush();

				Answer as = (Answer) ois.readObject(); // liest die Serverantwort ein der UUID

				if (as.getGuess().equals(prefixMap.get(id))) // Kontrolliert die gesendete mit der vorhandenen UUID
				{
					System.out.println("Korrekt, gut gemacht! \n");
				} else
				{ // man holt sich aus der Map die richtige Antwort heraus
					System.out.println("Falsche Antwort!" + "\n" + "Die richtige Antwort w�re gewesen: "
							+ q.getAnswers().get(as.getGuess()) + "\n");
				}
				if (q.isLast())
				{
					break;
				}
			}
			
			int maxValue = 0;
			List<String> winners = new ArrayList<>();
			
			@SuppressWarnings("unchecked") // weil wir wissen, dass das Object eine Map mit String und Integer ist, ist es ok
			Map<String, Integer>scores = (Map<String, Integer>) ois.readObject();
			
			
			for(Entry<String, Integer> score : scores.entrySet())
			{
				if(score.getValue() >= maxValue)
				{
					maxValue = score.getValue();
				}
				System.out.println("Spieler " + score.getKey() + " hat " + score.getValue() + " Punkte");
			}
			
			for(Entry<String, Integer> score : scores.entrySet())
			{
				if(score.getValue() == maxValue)
				{
					winners.add(score.getKey());
				}
			}
					
			if(winners.size() == 1)
			{
				System.out.println("Gewinner ist " + winners.get(0) + " mit " + maxValue + " Punkten \n");				
			}
			else
			{
				System.out.print("Gewinner sind ");
				
				for (String string : winners)
				{
					System.out.print(string + ", ");
				}
				
				System.out.println(" mit " + maxValue + " Punkten \n");
			}
			
			System.out.println("Danke f�rs Mitspielen!");

		} catch (UnknownHostException e)
		{
			e.printStackTrace();

		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

}
