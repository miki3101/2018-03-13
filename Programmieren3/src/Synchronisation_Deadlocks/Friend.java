package Synchronisation_Deadlocks;

public class Friend implements Runnable
{

	private Friend friend; // Beziehung: der Freund vor dem verbeugt wird 
	private String name;
	
	
	public Friend(String name)
	{
		super();
		this.name = name;
	}

	public void run()
	{
		bow();
	}
	
	public synchronized void bow() // damit sperrt sich ein Freund selbst und mit bowBack den anderen
	{								// immer nur ein Freund, darf sich innerhalb dieser Methode befinden
		System.out.println(name + " verbeugt sich vor " + this.getFriend().getName());
		this.getFriend().bowBack(); // Methode des anderen Thread aufrufen, wenn schon der andere Thread gestartet ist der auch in der Methode und somit f�r den anderen gestoppt
	}
	
	public synchronized void bowBack()
	{
		System.out.println(name + " erwiedert die Verbeugung von " + this.getFriend().getName());
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Friend getFriend()
	{
		return friend;
	}



	public void setFriend(Friend friend)
	{
		this.friend = friend;
	}

	
	
}
