package Synchronisation_Deadlocks;

public class FriendDemo
{

	public static void main(String[] args)
	{
		Friend f1 = new Friend("Max");
		Friend f2 = new Friend("Maria");
		
		f1.setFriend(f2);
		f2.setFriend(f1);
		
		Thread t1 = new Thread(f1);
		Thread t2 = new Thread(f2);
		
		
		// l�st einen Deadlock aus (beide sperren sich gegenseitig)
		// Programm bleibt stecken
		
		t1.start(); // mit der start() Methode aktiviere ich die run() Methode aus
		t2.start();
		
	}

}
