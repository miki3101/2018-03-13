package Quizduell_Messages;

import java.io.Serializable;

public class Player implements Serializable
{
	private static final long serialVersionUID = 2L;
	
	private String name;
	private int points = 0;

	
	public Player(String name)
	{
		super();
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getPoints()
	{
		return points;
	}
	
	public void setPoints(int points)
	{
		points += points;
	}
	
	

}
