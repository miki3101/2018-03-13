package Quizduell_Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;

import Quizduell_Messages.Answer;
import Quizduell_Messages.Player;
import Quizduell_Messages.Question;

public class ClientRunnable implements Runnable
{

	private Socket client;
	private GameManager gm;
	private String playerName;

	public ClientRunnable(Socket client, GameManager gm)
	{
		super();
		this.client = client;
		this.gm = gm;
	}

	public void run()
	{
		try (ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());)
		{

			Player p = (Player) ois.readObject(); // wartet bis der Client ihm ein Object vom Typ Player schickt
			playerName = p.getName();
			System.out.println(p.getName());

			int maxRounds = 3;
			int count = 1;

			if (!gm.registerClient(this)) // wenn noch nicht genug Spieler in der Liste sind
			{
				synchronized (this)
				{
					wait(); // warte bis die Liste voll ist, ansonsten überspringen
				}
			}

			while (count <= maxRounds)
			{
				QuestionRecord qr = gm.getCurrentQuestion();
				Question q = new Question(qr.getLevel(), qr.getText());

				q.getAnswers().put(qr.getCorrect().getUuid(), qr.getCorrect().getText()); // in die Map werden
																							// die richtigen
																							// Antwort getan
				for (AnswerRecord ar : qr.getWrong())
				{
					q.getAnswers().put(ar.getUuid(), ar.getText()); // die falschen Antworten werden in die Map getan
				}

				if (count == maxRounds)
				{
					q.setLast();
				}

				oos.writeObject(q); // schreibe die Frage
				oos.flush(); // schicke sie ab

				Answer a = (Answer) ois.readObject();

				boolean correct = a.getGuess().equals(qr.getCorrect().getUuid()); // Kontrolle ob die gesendete Antwort
																					// richtig ist

				// warten bis alle Spieler eine Antwort geschickt haben
				if (!gm.setAnswer(this, correct))
				{
					synchronized (this)
					{
						wait();
					}
				}
			
				Answer as = new Answer(qr.getCorrect().getUuid()); // raussuchen der UUID der richtigen Antwort
				oos.writeObject(as); // zurückschicken der richtigen UUID
				oos.flush();
				count++;
			}
			
			oos.writeObject(gm.getScores());
			oos.flush();

		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();

		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public Socket getClient()
	{
		return client;
	}

	public void setClient(Socket client)
	{
		this.client = client;
	}

	public GameManager getGm()
	{
		return gm;
	}

	public void setGm(GameManager gm)
	{
		this.gm = gm;
	}

	public String getPlayerName()
	{
		return playerName;
	}

	
}
