package Quizduell_Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server
{

	public static void main(String[] args)
	{

		try(ServerSocket server = new ServerSocket(1111);)
		{
			
			GameManager gm = new GameManager();

			while (true)
			{
				Socket socket = server.accept();
				System.out.println("Accepted: " + socket);

				ClientRunnable cr = new ClientRunnable(socket, gm);

				Thread t = new Thread(cr); // legen eine Thread an mit dem Client
				t.start(); // Thread wird gestartet
				
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
