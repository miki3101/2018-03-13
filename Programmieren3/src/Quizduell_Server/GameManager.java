package Quizduell_Server;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

public class GameManager
{
	private int playerNum; // wenn final - immer 4 Spieler
	private Random random = new Random(); // Zufallsgenerator
	private List<QuestionRecord> questions = new ArrayList<>(); // darin wird die gesamte Frage mit Antwort und Level
																// gespeichert
	private List<ClientRunnable> clients = new ArrayList<>(); // darin werden alle Runnables gespeichert (Spieler)
	private Map<ClientRunnable, Boolean> answers = new HashMap<>();
	private Map<String, Integer> scores = new HashMap<>();
	private QuestionRecord currentQuestion;

	public GameManager()
	{
		try (FileInputStream fin = new FileInputStream("server.properties"))
		{
			Properties config = new Properties();
			config.load(fin); // ladet aus der FileDatei
			playerNum = Integer.parseInt(config.getProperty("players.number")); // damit er wei� welche Zahl er einlesen
																				// muss

		} catch (NumberFormatException e1)
		{
			playerNum = 4;

		} catch (FileNotFoundException e1)
		{
			playerNum = 4;

		} catch (IOException e1)
		{
			System.out.println("Konfiguration konnte nicht gelesen werden: " + e1.getMessage());
			System.exit(-1); // Programm hatte Fehler
								// 0 alles hat funktioniert
		}

		// aus FileReader muss man FileInputStream machen um in UTF 8 umzuwandeln
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream("quizfragen.csv"), StandardCharsets.UTF_8));)
		{
			String line;

			while ((line = br.readLine()) != null)
			{
				String[] fields = line.replace("\"", "").split(";"); // alle " werden als Leerzeichen ersetzt und dann
																		// wird die Zeile gesplittet (man bekommt ein
																		// Array zur�ck)

				if (fields.length != 7) // wenn die Zeile nicht 7 Felder hat
				{
					continue; // Schleife wird wieder reaktiviert
				}
				try
				{
					Integer.parseInt(fields[6]); // kann man das letzte Feld in eine ganze Zahl umwandeln
													// (ist es eine Zahl)

				} catch (NumberFormatException e)
				{
					continue;
				}

				// Daten werden zugeordnet (Feld 0 ist Frage, Feld 6 ist der Level, Feld 1 ist
				// die richtige Antwort
				// Felder 2 - 4 sind die falschen Antworten)
				QuestionRecord qr = new QuestionRecord(fields[0], Integer.parseInt(fields[6]),
						new AnswerRecord(fields[1]));

				qr.getWrong().add(new AnswerRecord(fields[2]));
				qr.getWrong().add(new AnswerRecord(fields[3]));
				qr.getWrong().add(new AnswerRecord(fields[4]));

				questions.add(qr);

			}

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public synchronized QuestionRecord getRandomQuestion() // synchronized, damit stelle ich sicher, dass immer nur 1
	{ // Spieler gleichzeitig auf die Methode zugreifen kann
		QuestionRecord qr = null;

		int r = random.nextInt(questions.size()); // w�hle eine beliebige Frage aus allen in der Liste vorhanden Fragen
													// aus
		qr = questions.get(r);
		questions.remove(r);

		return qr;
	}

	public synchronized boolean registerClient(ClientRunnable r)
	{
		clients.add(r); // Spieler werden hinzugef�gt

		if (clients.size() == playerNum)
		{
			currentQuestion = getRandomQuestion();

			for (ClientRunnable clientRunnable : clients)
			{
				synchronized (clientRunnable)
				{
					clientRunnable.notify();
				}
			}
			return true;
		}
		return false;
	}

	public QuestionRecord getCurrentQuestion()
	{
		return currentQuestion;
	}

	public synchronized boolean setAnswer(ClientRunnable r, boolean correct)
	{
		answers.put(r, correct);

		if (!scores.containsKey(r.getPlayerName()))
		{
			scores.put(r.getPlayerName(), 0);
		}
		
		if (correct)
		{
			int score = scores.get(r.getPlayerName());
			score += currentQuestion.getLevel();
			scores.put(r.getPlayerName(), score);
		}
		
		if (answers.size() == playerNum)
		{
			currentQuestion = getRandomQuestion(); // hol neue Frage aus der Datei

			for (ClientRunnable clientRunnable : clients)
			{
				synchronized (clientRunnable)
				{
					clientRunnable.notify(); // weiter geht's
				}
			}
			answers.clear(); // wenn beide Antworten gegeben worden sind, die Map wieder l�schen
			return true;
		}
		return false;
	}

	public Map<String, Integer> getScores()
	{
		return scores;
	}

}
