package Quizduell_Server;

import java.util.UUID;

public class AnswerRecord
{
	private UUID uuid = UUID.randomUUID();	// eine fixe ID die vergeben wird
	private String text;
	
	
	public AnswerRecord(String text)
	{
		super();
		this.text = text;
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getText()
	{
		return text;
	}
	
	
}
