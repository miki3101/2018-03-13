package Quizduell_Server;

import java.util.ArrayList;
import java.util.List;

public class QuestionRecord
{
	private String text;
	private int level;
	private AnswerRecord correct;
	private List<AnswerRecord> wrong = new ArrayList<>();
	
	public QuestionRecord(String text, int level, AnswerRecord correct)
	{
		super();
		this.text = text;
		this.level = level;
		this.correct = correct;
	}

	public int getLevel()
	{
		return level;
	}

	public String getText()
	{
		return text;
	}

	public AnswerRecord getCorrect()
	{
		return correct;
	}

	public List<AnswerRecord> getWrong()
	{
		return wrong;
	}
	
	
	
	
	
}
