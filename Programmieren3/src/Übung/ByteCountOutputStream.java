package �bung;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

// ByteCountOutputStream soll vom FilterOutputStream erben
// die Methoden write(int b), write(byte[] b) und write (byte[] b, int off, int len) �berschreiben
// es soll mitgez�hlt werden, wie viele Bytes geschrieben werden

public class ByteCountOutputStream extends FilterOutputStream
{

	private int counter = 0;

	public ByteCountOutputStream(OutputStream out)
	{
		super(out);
	}

	public void write(int b) throws IOException
	{
		counter++;
		super.write(b);
	}

	public void write(byte[] b) throws IOException
	{
		counter += b.length;
		super.write(b);
	}

	public void write(byte[] b, int off, int len) throws IOException
	{
		counter += len;
		super.write(b, off, len);
	}

	public int getCounter()
	{
		return counter;
	}
}
