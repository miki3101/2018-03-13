package �bung;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo
{

	// ein beliebiger Text soll mit dem ByteCountOutputStream in eine Datei
	// geschrieben werden
	// auf der Konsole soll ausgegeben werden, wie viele Bytes geschrieben wurden

	public static void main(String[] args)
	{
		String text = "We are counting the bytes of this text being written into a file.";

		File file = new File("output2.txt");

		if (!file.exists())
		{
			try
			{
				file.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}	
		}
			try(ByteCountOutputStream bcos = new ByteCountOutputStream(new FileOutputStream(file));)
			{
				bcos.write(text.getBytes());
				bcos.flush();
				
				System.out.println("Bytes in text: " + text.getBytes().length);
				System.out.println("Bytes written: " + bcos.getCounter());
				
				
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

