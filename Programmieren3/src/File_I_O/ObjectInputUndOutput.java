package File_I_O;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectInputUndOutput
{

	public static void main(String[] args)
	{
		String text = "Dies ist ein Java-Object";
		
		try
		{
			FileOutputStream fos = new FileOutputStream("object.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(text);
			fos.flush();
			fos.close();
			
			FileInputStream fis = new FileInputStream("object.dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			String text_read = (String)ois.readObject();
			
			System.out.println(text.equals(text_read));
			
			fis.close();
					
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
			
		} catch (IOException e)
		{
			e.printStackTrace();
			
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

}
