package File_I_O;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class InputStreamDemo
{

	public static void main(String[] args)
	{
		File f = new File("test.txt");
		if (!f.exists())
		{
			try
			{
				f.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		try
		{
			FileInputStream inp = new FileInputStream("test.txt");

			int b; // Zwischenspeicher

			while ((b = inp.read()) != -1) // solange kein -1 zur�ckkommt also kein zeichen mehr vorhanden ist
			{
				System.out.println(b);
				System.out.print(Character.toChars(b)); // gibt ein Char[] zur�ck
			}

			inp.close();
			
		} catch (FileNotFoundException e)
		{
			System.out.println("Datei wurde nicht gefunden: " + e.getMessage());
			return;
		} catch (IOException e)
		{
			System.out.println("Datei konnte nicht gelesen werden: " + e.getMessage());
			return;
		}

	}

}
