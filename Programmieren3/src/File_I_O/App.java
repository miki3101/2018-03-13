package File_I_O;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

public class App
{
	public static void main(String[] args)
	{
		File f = new File("bla" + File.separator + "foo" + File.separator + "bar");
		// um die Trennzeichen von Java selbst zu machen
		File f1 = new File("test.txt");
		System.out.println(f.getAbsolutePath());

		for (String path : f.getAbsolutePath().split("\\" + File.separator)) // so
		// niemals machen!!!
		{
			System.out.println(path);
		}

		f.canExecute(); // ist die Datei ausf�hrbar
		f.canWrite(); // darf man in der Datei schreiben
		f.canRead(); // darf man die Datei lesen
		f.delete(); // Datei wird gel�scht (boolean)
		f.getAbsoluteFile(); // neues File Objekt, Pfad ist hinterlegt
		System.out.println(f.getAbsolutePath()); // Pfad als String

		if (!f.exists())
		{
			System.out.println(f.mkdir()); // kann keine verschachtelten Verzeichnisse
			// anlegen
			System.out.println(f.mkdirs()); // kann verschachtelte Verzeichnisse anlegen
			try
			{
				f.mkdir(); // lege ein Verzeichnis an
				f.createNewFile(); // Datei wird erstellt (boolean)

			} catch (IOException e)
			{
				System.out.println("Fehler beim Erstellen der Datei: " + e.getMessage());
				e.printStackTrace();
				return; // Programm wird beendet
			}
		}

		f.length(); // Dateigr��e
		f.renameTo(f); // verschieben einer Datei

		if (f.isDirectory())
		{
			for (String l : f.list(new FilenameFilter() // im Verzeichnis enthaltenen
			// Dateien und Unterverzeichnisse
			{

				@Override
				public boolean accept(File dir, String name)
				{
					return name.endsWith(".txt");
				}
			}))
			{
				System.out.println(l);
			}

			System.out.println(f.toURI()); // zeigt die URL an
			System.out.println(f.pathSeparator); // Verzeichnisse trennen ";"
			System.out.println(f.separator); // Verzeichnisnamen trennen "\"
		}
		try
		{
			System.in.read(); // Input Stream liest 1 Byte ein (int) Cursor geht weiter
			System.in.skip(0); // Cursor an bestimmte Stelle setzen
			System.in.reset(); // Cursor wird an den Anfang gestellt
			System.in.close(); // Datei schlie�en

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
