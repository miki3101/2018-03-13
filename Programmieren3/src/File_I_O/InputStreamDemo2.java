package File_I_O;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamDemo2
{

	public static void main(String[] args)
	{
		try
		{
			File f = new File ("output.txt");		// nur notwendig um die L�nge zu ermitteln
			
			long length = f.length();			// l�nge (Bytes) der Datei wird ermittelt
			System.out.println(length);
			
			InputStream inp = new FileInputStream("output.txt"); // Datei zum lesen ge�ffnet
			FileOutputStream out = new FileOutputStream("output.txt", true); // Datei zum schreiben ge�ffnet

			int b;
			
			while((b = inp.read()) != -1) // lies solange bis kein Byte mehr vorhanden (-1)
			{
				System.out.println(b);
				out.write(b); // erzeugt eine Endlosschleife 
			}
				
			inp.close();
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
