package File_I_O;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class UTFWriterDemo
{
	public static void main(String[] args)
	{
				// FileOutputStream kann nicht umwandeln in UTF_8
		try (OutputStream os = new FileOutputStream("umlaute.txt");
				// nur der OutputStreamWriter kann umwandeln auf UTF_8
			 OutputStreamWriter wr = new OutputStreamWriter(os, StandardCharsets.UTF_8))
		{
			wr.write("K�che machen M�sli mit �pfeln");
			

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
