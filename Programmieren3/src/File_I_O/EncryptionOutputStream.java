package File_I_O;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class EncryptionOutputStream extends FilterOutputStream
{

	private int key;
	
	public EncryptionOutputStream(OutputStream out, int key)
	{
		super(out);
		this.key = key;
	}

	@Override
	public void write(int b) throws IOException
	{
		super.write(b + key);
	}

}
