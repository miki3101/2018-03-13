package File_I_O;

import java.io.IOException;

public class �bung1
{

	// Schreiben Sie ein Programm, welches von System.in (Vergleichbar mit FileInputStream) solange Zeichen einliest, bis der Benutzer ein "x" eingibt
	
	public static void main(String[] args)
	{ 
		 int test;
		 
		 try
		{
			while((test = System.in.read()) != -1)
			{
				char c = Character.toChars(test)[0];
				
				if(c == 'x' || c == 'X')
					return;
				
			  System.out.print(Character.toChars(c));
			}
			 
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
