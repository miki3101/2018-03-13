package File_I_O;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class SystemInputReader
{

	// Schreiben Sie ein Programm, das zeilenweise Tastatureingaben auf die Konsole
	// schreibt,
	// bis das Wort "STOP" eingegeben wird
	// Verwenden Sie dazu den InputStream der �ber System.in bereitgestellt wird,
	// sowie die Klassen InputStreamReader und BufferedReader

	public static void main(String[] args)
	{
		try
		{
			InputStreamReader inp = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(inp);

			String line;

			while ((line = br.readLine()) != null)
			{
				switch (line)
				{
				case "HELP":
					System.out.println("STOP: Stoppt die Anwendung");
					break;
				case "STOP":
					return;
				}
			}

			br.close();

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();

		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
