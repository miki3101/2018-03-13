package File_I_O;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStreamDemo
{
	public static void main(String[] args)
	{
		String text = "Dies ist mein Text, er ist nicht sehr interessant";

		try
		{
			OutputStream out = new FileOutputStream("output.txt");

			for (char x : text.toCharArray())
			{
				out.write(x); // java castet automatisch char zu int
			}

			out.flush();// Programm wird solange festgehalten, bis die gesamte Datei
						// vom Betriebssystem auf die Festplatte geschrieben wird
						// und der Cache vom Betriebssystem wird gel�scht
			out.close();

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
