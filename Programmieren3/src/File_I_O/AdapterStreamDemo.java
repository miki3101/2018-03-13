package File_I_O;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class AdapterStreamDemo
{
	

	public static void main(String[] args)
	{
		try
		{
			InputStream ins = new FileInputStream("noten.csv"); // Datei wird als Byte-Stream ge�ffnet
			InputStreamReader isr = new InputStreamReader(ins); // InputStream wird zu Reader
			BufferedReader br = new BufferedReader(isr);		// Reader kann jetzt zeilenweise lesen
		
			br.readLine();
			
			OutputStream out = new FileOutputStream("noten.csv"); // Datei als Byte-Stream ge�ffnet
			OutputStreamWriter wr = new OutputStreamWriter(out); // jetzt kann man Strings in die Datei schreiben
			
			wr.write("Zeile 1 \r\n");
				
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
