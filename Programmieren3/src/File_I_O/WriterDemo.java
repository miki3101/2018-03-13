package File_I_O;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class WriterDemo
{

	public static void main(String[] args)
	{
		Writer out = new OutputStreamWriter(System.out);
		
		BufferedWriter bw = new BufferedWriter(out);
		
		String c = "Hallo \n";
		
		try
		{
			bw.append(c);	// h�ngt hinten etwas an
			bw.write("Dies ist ein Test \n");
			bw.flush();		// unbedingt machen
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}	
		
		PrintWriter pw = new PrintWriter(out);
		pw.println("Zweiter Test");
		pw.flush();
		
	}

}
