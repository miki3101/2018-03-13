package File_I_O;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class TryWithRessource
{
	public static void main(String[] args)
	{
		// Java verhindert automatisch Fehler die mit der Datei passieren k�nnen
		// Streams, Reader, Writer

		try (Reader re = new FileReader("noten.csv"); BufferedReader br = new BufferedReader(re);)
		{
			String line;

			while ((line = br.readLine()) != null)
			{
				System.out.println(line);
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
