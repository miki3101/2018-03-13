package at.campus02.nowa.swe;

import java.awt.Point;

public class Game {
	private char[][] _board;
	private Point snake = new Point(1, 1);
	
	public Game(int size) {
		_board = new char[size][size];
		initBoard();
		_board[snake.x][snake.y] = 's';
	}
	
	private void initBoard() {
		for (int y = 0; y < _board.length; y++) {
			for (int x = 0; x < _board[0].length; x++) {
				if (y == 0 || y == _board.length -1) {
					_board[x][y] = 'x';
				}else if (x == 0 || x == _board.length -1) {
					_board[x][y] = 'x';
				}
			}
		}
	}
	
	public void moveRight() {
		if (snake.x < _board.length-2) {
			_board[snake.x][snake.y] = ' ';
			snake.x += 1;
			_board[snake.x][snake.y] = 's';
		}
	}
	
	public void moveLeft() {
		if (snake.x > 1) {
			_board[snake.x][snake.y] = ' ';
			snake.x += -1;
			_board[snake.x][snake.y] = 's';
		}
	}
	
	public void moveUp() {
		if (snake.y > 1) {
			_board[snake.x][snake.y] = ' ';
			snake.y += -1;
			_board[snake.x][snake.y] = 's';
		}
	}
	
	public void moveDown() {
		if (snake.y < _board.length-2) {
			_board[snake.x][snake.y] = ' ';
			snake.y += 1;
			_board[snake.x][snake.y] = 's';
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();		// wenn man Strings oft manipuliert
		for (int y = 0; y < _board.length; y++) {
			for (int x = 0; x < _board[0].length; x++) {
				sb.append(_board[x][y]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
