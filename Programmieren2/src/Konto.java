// Schreiben Sie eine Methode setInhaber(), welche den Inhaber setzt und den Kontostand auf 0 setzt
// Schreiben Sie eine Methode aufbuchen (), welche einen �bergebenen Betrag aufbucht
// Schreiben Sie eine Methode abbuchen (), welche einen �bergebenen Betrag abbucht.
// Stellen Sie jedoch sicher, dass der Kontostand 0 nicht unterschreiten darf 

public class Konto
{
	private String inhaber;
	private double kontostand;
	private final String Standort = "Campus02";				// final = dieser Standort kann nicht ver�ndert werden
															// muss immer gleich initialisiert werden
	public void inhaber(String inhaber, double kontostand)
	{
		this.inhaber = inhaber;
		this.kontostand = 0.0;
	}

	public String getInhaber()
	{
		return inhaber;
	}

	public void setInhaber(String inhaber)
	{
		this.inhaber = inhaber;
	}
	
	public void getKontostand()
	{
		System.out.println("Sie haben derzeit: � " + kontostand + " am Konto");
	}

	public void setKontostand(double kontostand)
	{
		this.kontostand = kontostand;
	}
	
	public void aufbuchen(double einlage)
	{
		kontostand = kontostand + einlage;
		System.out.println("Sie haben gerade: � " + einlage + " auf Ihr Konto eingezahlt");
		
	}
	
	public double abbuchen (double betrag)
	{
		if (betrag > kontostand)
		{
			System.out.println("Sie k�nnen leider keine � " + betrag + " abheben");
			System.out.println("Sie haben nicht genug Geld am Konto");
			System.out.println("Sie k�nnen max � " + kontostand + " abheben");
			return kontostand;
		}
		 kontostand = kontostand - betrag;
		 System.out.println("Sie haben gerade: � " + betrag + " von Ihrem Konto abgehoben");
		 
		 return kontostand;
	}
	
	public static int summe (int a, int b)
	{
		return a*b;
	}
	
	public String toString()
	{
		return "Kontoinhaber: " + inhaber + "\n" + "neuer Kontostand: � " + kontostand + "\n";
	}

}
