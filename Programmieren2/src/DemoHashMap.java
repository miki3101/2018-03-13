import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class DemoHashMap
{
	public static void main(String[] args)
	{
		HashMap<String, Integer> staedteMap = new HashMap<>();	// Key = String, Value = Integer
		
		staedteMap.put("Graz", 273838);
		staedteMap.put("Wien", 5000000);
		staedteMap.put("Deutschlandberg", 60410);
		
		System.out.println("Schl�ssel: " + staedteMap.keySet());				// gibt nur die Schl�ssel aus
		System.out.println("Werte: " + staedteMap.values());					// gibt nur die Werte aus
		System.out.println("Schl�ssel und Werte: " + staedteMap.entrySet());	// gibt die Schl�ssel und die Werte aus
		
		Set<String> keySet = staedteMap.keySet();		// wandelt die Schl�ssel in ein Set um
		
		System.out.println();
		System.out.println("Schl�ssel im Set: " + keySet);
		
		for (String string : keySet)
		{
			System.out.println(string);
		}
		System.out.println();
		
		Collection<Integer> valueSet = staedteMap.values();		// wandelt die Werte in eine Collection um
		
		System.out.println("Werte in der Collection: " + valueSet);
		
		for (Integer integer : valueSet)
		{
			System.out.println(integer);
		}
		System.out.println();
		
		Set<Entry<String,Integer>> keyValues = staedteMap.entrySet();	// wandelt Schl�ssel und Werte in ein Entry-Set
		
		System.out.println("Schl�ssel und Werte in EntrySet: " + keyValues);
		
		for (Entry<String, Integer> entry : keyValues)
		{
			System.out.println("Stadt: " + entry.getKey() + "," + " Einwohnerzahl: " + entry.getValue());
		}
		System.out.println();
		System.out.println("Entry Set:");
		
		for (Entry<String, Integer> entry : keyValues)
		{
			System.out.println(entry);
		}
		System.out.println();
		
		staedteMap.clear();						// l�scht die gesamte Map
		System.out.println("gesamter Inhalt der Map wird gel�scht: " + staedteMap);
	}
}
