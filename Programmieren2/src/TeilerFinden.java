
public class TeilerFinden
{

	public static void main(String[] args)
	{
		int zahl = 128;
		int teiler = 2;
		
		findeTeiler(zahl, teiler);
	
	}
	
	public static void findeTeiler(int zahl, int teiler)
	{
		System.out.println("Teiler von " + zahl + " sind: ");
		
		while (teiler <= zahl / teiler)
		{
			if (zahl % teiler == 0)
			{
			zahl = zahl/teiler;
			System.out.print(zahl + ", ");
			}
		
		}

	}

}
