package org.campus02.emp;

public class Osterhase extends Hase
{
	public Osterhase(String name)
	{
		super(name);
	}
	
	public void ostereierVerstecken()
	{
		System.out.println(name + " " + "versteckt Ostereier");
	}

	public void fressen()
	{
		System.out.println(name + " " + "frisst Ostereier");
	}

	public void hoppeln()
	{
		System.out.println(name + " " + "hoppelt schnell");
	}

	public void schlafen()
	{
		System.out.println(name + " " + "schl�ft im Winter");
	}
	
	
	
	
}
