package org.campus02.emp;

public class Weihnachtshase extends Hase
{
	public Weihnachtshase(String name)
	{
		super(name);
	}
	
	public void geschenkeVerteilen()
	{
		System.out.println(name + " " + "verteilt Geschenke");
	}

	public void fressen()
	{
		System.out.println(name + " " + "frisst Weihnachtskugeln");
	}


	public void hoppeln()
	{
		System.out.println(name + " " + "hoppelt langsam");
	}

	public void schlafen()
	{
		System.out.println(name + " " + "schl�ft im Sommer");
	}
	
	
	
}
