package org.campus02.emp;

public class Animal
{
	public String name;
	
	public void makeNoise()
	{
		System.out.println(name + " unbekannt!!");
	}
	
	public void move()
	{
		System.out.println("Bewegung nicht bekannt!!");
	}	
}
