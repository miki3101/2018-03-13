package org.campus02.emp;

public class MayBe2 <T>
{
	private T data;
	private int status;
	
	
	public MayBe2 (T data)
	{
		this.data = data;
		this.status = 1;
	}
	
	public void setStatus(int status)
	{
		if(status >= 0 && status <= 1)
		{
			this.status = status;
		}
	}
	
	public void print ()
	{
		switch (status)
		{
		case 0:
			System.out.println("Daten werden nicht angezeigt");
			break;
		case 1:
			System.out.println("Daten: " + data);
			break;
		case 3:
			System.out.println("Daten wurden nicht gesetzt");
			break;
		default:
			System.out.println("Daten sind unbekannt");
			break;
		}
	}
}
