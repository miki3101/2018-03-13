package org.campus02.emp;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Blackjack
{
	private HashMap<Player,Integer> players = new HashMap<>();
	

	public boolean add(Player player)
	{
		if(!players.containsKey(player))
		{
			players.put(player, 0);
		
			return true;
		}
		
		return false;
	}
	
	public boolean addCard (Player player, int cardValue)
	{
		if(players.containsKey(player))
		{
			players.put(player, this.getValue(player) + cardValue);
			
			return true;
		}
		
		return false;
	}

	public int getValue (Player player)
	{
		return players.get(player);
	}
	
	public String toString()
	{
		String text = "";
		Set<Entry<Player,Integer>> set = players.entrySet();
		
		for (Entry<Player, Integer> entry : set)
		{
			text += entry.getKey().getName() + ", " + "Kartenwert: " + entry.getValue() + "\n";
		}
		return text;
	}
	
}
