package org.campus02.emp;

public class MayBe
{
	private int data;
	private int status;

	public MayBe(int data, int status)
	{
		this.data = data;
		this.status = status;
	}

	public int getData()
	{
		return data;
	}

	public void setData(int data)
	{
		this.data = data;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String print()
	{
		String text = "";

		switch (status)
		{
		case 1:
			text = "Zugriff gestattet: " + data;
			break;
		case 2:
			text = "Zugriff nicht gestattet!";
			break;
		case 3:
			text = "Nicht erfasst!";
			break;

		default:
			text = "Ungültiger Status!!";
			break;

		}
		return text;
	}
}
