package org.campus02.emp;

public class Person2
{
	private String vorname;
	private String nachname;
	private String alter;
	
	public Person2(String vorname, String nachname, String alter)
	{
		this.vorname = vorname;
		this.nachname = nachname;
		this.alter = alter;
	}
	
	public void doSomething()
	{
		System.out.println(vorname + " " + nachname + ": " +"macht etwas");
	}
	
	
}
