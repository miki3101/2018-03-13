package org.campus02.emp;

import java.util.ArrayList;

public class EmployeeManager
{
	private ArrayList <Employee> employees = new ArrayList<Employee>();
	
	
	public void addEmployee (Employee e)
	{
		employees.add(e);
	}
	
	public void findByMaxSalary ()
	{
		
		for (Employee name : employees)
		{
			double max = employees.get(0).getSalary();
			
			
			if (name.getSalary() > max)
			{
				Employee meister = name;
				System.out.println("Mitarbeiter mit dem h�chsten Gehalt ist: " + "\n"+ meister+ "\n");
			}
		}
		
	}
	
	public void findByEmpNumber (int number)
	{
		for (Employee name : employees)
		{
			if (name.getEmpNumber() == number)
			{
				System.out.println("Gesuchte Mitarbeiternummer geh�rt zu: " + "\n"+ name + "\n");
				break;
			}
			else {
				System.out.println("Es wurde kein Mitarbeiter mit dieser Nummer gefunden");
				break;
			}
			 
		}
		
	}
	
	public void findByDepartment (String department)
	{
		System.out.println("In der Abteilung " + department + " arbeitet: " + "\n");
		
		for (Employee name : employees)
		{
			if (name.getDepartment() == department)
			{
				System.out.println(name);
			}
		}
	}

}
