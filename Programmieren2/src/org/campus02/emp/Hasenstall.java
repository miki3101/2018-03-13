package org.campus02.emp;

import java.util.List;

public class Hasenstall
{
	protected List<Hase> hasenstall;
	
	public void haseInStall(Hase hase)
	{
		hasenstall.add(hase);
	}
	
	public List<Hase> getHasenstall()
	{
		return hasenstall;
	}
	
	public void setHasenstall(List<Hase> hasenstall)
	{
		this.hasenstall = hasenstall;
	}

	public void imStall()
	{
		System.out.println("Hasen im Hasenstall: ");
			
		for (Hase hase : hasenstall)
		{
			System.out.println(hase.name);
		}
	}
	
	public void fressendeHasen()
	{
		System.out.println("So fressen die Hasen im Hasenstall: ");
		
		for (Hase hase : hasenstall)
		{
			hase.fressen();
		}
	}
	
	public void schlafendeHasen()
	{
		System.out.println("So schlafen die Hasen im Hasenstall: ");
		
		for (Hase hase : hasenstall)
		{
			hase.schlafen();
		}
	}
	
	public void zeigeHase()
	{
		System.out.println("Welche Eigenschaften haben die Hasen im Stall: ");
		
		for (Hase hase : hasenstall)
		{
			if(hase instanceof Osterhase)
			{
				((Osterhase) hase).ostereierVerstecken();
			}
			if(hase instanceof Weihnachtshase)
			{
				((Weihnachtshase) hase).geschenkeVerteilen();
			}
		}
	}
	
	
	
	
	
}
