package org.campus02.emp;

public class Student
{
	
	private String name;
	private Integer alter;
	private String stadt;
	
	
	public Student (String name,Integer alter)
	{
		this.name = name;
		this.alter = alter;
	}
	
	public String getStadt()
	{
		return stadt;
	}
	
	public void setStadt(String stadt)
	{
		this.stadt = stadt;
	}
	
	public int getAlter()
	{
		return alter;
	}
	
	public void setAlter(Integer alter)
	{
		this.alter = alter;
	}

	public String getname()
	{
		return name;
	}
	
	public void setname(String name)
	{
		this.name = name;
	}
	
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alter == null) ? 0 : alter.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((stadt == null) ? 0 : stadt.hashCode());
		return result;
	}

	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (alter == null)
		{
			if (other.alter != null)
				return false;
		} else if (!alter.equals(other.alter))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (stadt == null)
		{
			if (other.stadt != null)
				return false;
		} else if (!stadt.equals(other.stadt))
			return false;
		return true;
	}

	public String toString()
	{
		return String.format("%s, %d", name, alter);
	}
}
