package org.campus02.emp;

public class Adress
{
	protected String strasse;
	protected String zip;
	protected String stadt;
	protected String nr;
	
	public Adress (String strasse, String zip, String stadt, String nr)
	{
		this.strasse = strasse;
		this.zip = zip;
		this.stadt = stadt;
		this.nr = nr;
	}
	
}
