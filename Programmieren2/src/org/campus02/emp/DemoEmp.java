package org.campus02.emp;

public class DemoEmp
{

	public static void main(String[] args)
	{
		EmployeeManager c1 = new EmployeeManager();
		
		Employee e1 = new Employee(1, "Hugo", 12000, "Buchhaltung");
		Employee e2 = new Employee(2, "Susi", 700, "Buchhaltung");
		Employee e3 = new Employee(3, "Sarah", 22000, "IT");
		Employee e4 = new Employee(4, "Heinz", 3000, "IT");
		Employee e5 = new Employee(5, "Max", 2000, "IT");
		

		c1.addEmployee(e1);
		c1.addEmployee(e2);
		c1.addEmployee(e3);
		c1.addEmployee(e4);
		c1.addEmployee(e5);
	
		c1.findByMaxSalary();
		c1.findByEmpNumber(1);
		c1.findByEmpNumber(6);
		
		c1.findByDepartment("Buchhaltung");
		
		
	
		
		
		
		


	}

}
