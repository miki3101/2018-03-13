package org.campus02.emp;

import java.util.GregorianCalendar;

public class Profil
{
	private String name;
	private String adress;
	private int age;
	private MayBe2<String> email;
	private char gender;
	private MayBe2<GregorianCalendar> dateOfBirth;
	private MayBe2<Double> salary;
	
	
	public Profil (String name, String adress, int age, String email, char gender, GregorianCalendar dateOfBirth, Double salary)
	{
		this.name = name;
		this.adress = adress;
		this.age = age;
		this.email = new MayBe2<String>(email);
		this.gender = gender;
		this.dateOfBirth = new MayBe2<GregorianCalendar>(dateOfBirth);
		this.salary = new MayBe2<Double>(salary);
	}

	public void setSalary(int status)
	{
		this.salary.setStatus(status);
	}
	
	public void print()
	{
		String text = "Profil von: " + name + "\n" + "Adresse: " + adress + "\n" + "Alter: " + age + "\n" + "Geschlecht: " + gender;
		
		System.out.println(text);
		
		this.email.print();
		this.dateOfBirth.print();
		this.salary.print();
	}
	
	
}
