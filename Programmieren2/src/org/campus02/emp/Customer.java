package org.campus02.emp;

public class Customer extends Person2
{
	private int id;
	private Adress adress;
	
	public Customer(int id, String vorname, String nachname, String alter, Adress adress)
	{
		super(vorname, nachname, alter);
		this.id = id;
		this.adress = adress;
	}
	
	public Customer(int id, Adress adress)
	{
		super("", "", "");
		this.id = id;
		this.adress = adress;
	}
	
	public void changeAdress(Adress adress)
	{
		this.adress = adress;
	}
	
}
