package org.campus02.emp;

import java.util.LinkedList;

public class DemoHase
{
	public static void main(String[] args)
	{
		Hase hase1 = new Hase("Hoppel");
		Weihnachtshase hase2 = new Weihnachtshase("Weihnachtshasi");
		Osterhase hase3 = new Osterhase("Osterhasi");
				
		hase1.fressen();
		hase2.fressen();
		hase3.fressen();
				
		System.out.println("-------------");
		
		hase2.geschenkeVerteilen();
		hase3.ostereierVerstecken();
		
		System.out.println("-------------");
		
		hase1.hoppeln();
		hase2.hoppeln();
		hase3.hoppeln();
		
		System.out.println("-------------");
		
		hase1.schlafen();
		hase2.schlafen();
		hase3.schlafen();
		
		System.out.println("-------------");
		
		hase2.geschenkeVerteilen();
		hase3.ostereierVerstecken();
		
		System.out.println("-------------");
		
		Hasenstall stall = new Hasenstall();
		LinkedList<Hase> hasen = new LinkedList<>();
		stall.setHasenstall(hasen);
		stall.haseInStall(hase1);
		stall.haseInStall(hase2);
		stall.haseInStall(hase3);
		
		stall.imStall();
		System.out.println("-------------");
		
		stall.fressendeHasen();
		System.out.println("-------------");
		
		stall.schlafendeHasen();
		System.out.println("-------------");
		
		Hase hase4 = new Weihnachtshase("Weihnachts-Elfenhasi");
		Hase hase5 = new Osterhase("Oster-Helferlein");
		
		System.out.println("up-casting: ");
		
		hase4.fressen();
		hase5.fressen();
		stall.haseInStall(hase4);
		stall.haseInStall(hase5);
		
		System.out.println("-------------");
		System.out.println("down-casting: ");
		
		Osterhase hase6 = (Osterhase) hase5;
		hase6.ostereierVerstecken();
		
		Weihnachtshase hase7 = (Weihnachtshase) hase4;
		hase7.geschenkeVerteilen();
		
		System.out.println("-------------");
		stall.zeigeHase();
		
	}

}
