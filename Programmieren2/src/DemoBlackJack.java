import org.campus02.emp.Blackjack;
import org.campus02.emp.Player;

public class DemoBlackJack
{

	public static void main(String[] args)
	{
		Player p1 = new Player("Max Mustermann", 20);
		Player p2 = new Player("Susi Sorglos", 30);
		
		Blackjack test = new Blackjack();
		
		test.add(p1);
		test.add(p2);
		
		System.out.println("Spieler setzt sich an den Tisch:");
		System.out.println(test);
		
		test.addCard(p1, 10);
		test.addCard(p2, 3);
		
		System.out.println("Wert nach der 1. Karte: ");
		System.out.println(test);
		
		test.addCard(p1, 10);
		test.addCard(p2, 14);
		
		System.out.println("Wert nach der 2. Karte:");
		System.out.println(test);

	}

}
