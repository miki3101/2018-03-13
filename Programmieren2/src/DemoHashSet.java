import java.util.HashSet;
import java.util.Iterator;

public class DemoHashSet
{
	public static void main(String[] args)
	{
		HashSet<String> hs = new HashSet<>();
		
		hs.add("String 1");
		hs.add("String 2");
		hs.add("String 3");
		
		for (String string : hs)			
		{
			System.out.println("Ausgabe mit for Schleife: " + string);
		}
		
		System.out.println();
		
		Iterator<String> iterator = hs.iterator();
		
		String newString = iterator.next();
		
		System.out.println("next() ohne Schleife: " + newString);
		
		String newString2 = iterator.next(); 
		System.out.println("next() ohne Schleife: " + newString2);
		
		System.out.println();
		
		iterator = hs.iterator();
		
		while(iterator.hasNext())
		{
			String str = iterator.next();
			System.out.println("next() mit while Schleife: " + str);
		}
		
		System.out.println();
		
		hs.remove("String 1");
		
		iterator = hs.iterator();
		
		while(iterator.hasNext())
		{
			String str = iterator.next();
			System.out.println("nach einem remove: " + str);
		}
		
		System.out.println();
		
		if(hs.contains("String 3"))
		{
			hs.remove("String 3");
		}
		
		iterator = hs.iterator();
		
		while(iterator.hasNext())
		{
			String str = iterator.next();
			System.out.println("jetzt mit contains: " + str);
		}
	}
}
