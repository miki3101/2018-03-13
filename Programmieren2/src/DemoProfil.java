import java.util.GregorianCalendar;

import org.campus02.emp.Profil;

public class DemoProfil
{

	public static void main(String[] args)
	{
		Profil p1 = new Profil("Susi", "Graz", 27, "susi@gmail.com", 'F', new GregorianCalendar(1990, 01, 15), 2000.00);
		
		System.out.println("Ausgabe mit Status 1: ");
		p1.print();
		System.out.println();
		
		System.out.println("Ausgabe mit Status 0: ");
		p1.setSalary(0);
		p1.print();

	}

}
