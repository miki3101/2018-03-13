
import java.util.LinkedList;
import java.util.ListIterator;
import org.campus02.emp.Student;

public class DemoStudent
{
	public static void main(String[] args)
	{
		LinkedList<Student> students = new LinkedList<>();
		
		students.addFirst(new Student ("Max", 34));
		students.addFirst(new Student ("Peter", 38));
		students.addFirst(new Student ("John", 38));
		students.addFirst(new Student ("Phil", 25));
				
		ListIterator<Student> it = students.listIterator();
		
		System.out.println("Erster in der Liste ist der Iterator: " + it.next()); // darauf zeigt der aktuelle Iterator
		System.out.println("Zweiter in der Liste: " + it.next());				// Iterator merkt sich intern und wandert weiter
		System.out.println("--------------");
		
		while(it.hasNext())				
		{
			Student p = it.next();		// iterator wandert weiter auf die 3. Position
			System.out.println("Student: " + p.getname());
		}
		
		System.out.println("------------");
		
		Student a = new Student("Max", 25);
		Student b = new Student("Max", 26);
		
		boolean istGleich = a.equals(b); // sind diese Objekte gleich
		System.out.println("Sind a und b gleich: " + istGleich);
		
		boolean istGleich3 = a.equals(b);
		System.out.println("Methode equals wurde verwendet: " + istGleich3);
		
		boolean istGleich2 = a.getname() == b.getname(); // sind diese Namen der Objekte gleich
		System.out.println("Ist der Name von a und b gleich: " + istGleich2);
		
		
		System.out.println("--------------");
		
				
		it = students.listIterator();			// muss, wenn etwas neu hinzugefügt wurde, nocheinmal initialisiert werden
		
		System.out.println("---------------");
		System.out.println(it.next());			// zeigt den Studenten auf den der Iterator zeigt
		System.out.println("--------------");
	
		for (Student student : students)		// gibt alle Studenten in der Liste aus
		{
			System.out.println("Alle Studenten: " + student.getname());
		}
		it.remove();						// entfernt den Studenten auf den der Iterator derzeit zeigt
		
		System.out.println("------------");
			
		while(it.hasNext())
		{
			Student p = it.next();
			System.out.println("Alle Studenten: " + p.getname());
		}
		it = students.listIterator();			// muss wieder neu initialisiert werden
		
		System.out.println("------------");
		System.out.println("Darauf zeigt der Iterator: " + it.next());		// auf diesen Studenten zeigt der Iterator (immer auf den ersten)
		System.out.println("-------------");
		System.out.println("Student an erster Stelle: " + students.getFirst()); // der erste in der Liste
	}
}
