
public class DemoWrapper
{
	public static void main(String[] args)
	{
		String salary = "20000";
		
		double salaryDouble = Double.parseDouble(salary);
		
		System.out.println("Als String: " + salary);
		System.out.println("Als double Wert: " + salaryDouble);
		
		int zahl = 6;
		
		String zahlInt = String.valueOf(zahl);
		String zahlDouble = String.valueOf(zahl);
		
		System.out.println(zahlDouble);

	}

}
