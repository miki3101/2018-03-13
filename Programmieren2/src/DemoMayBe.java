import org.campus02.emp.MayBe;

public class DemoMayBe
{

	public static void main(String[] args)
	{
		MayBe b1 = new MayBe(10, 1);
		System.out.println(b1.print());
		
		MayBe b2 = new MayBe(100, 2);
		System.out.println(b2.print());
		
		MayBe b3 = new MayBe(50, 3);
		System.out.println(b3.print());
		
		MayBe b4 = new MayBe(50, 0);
		System.out.println(b4.print());
		

	}

}
