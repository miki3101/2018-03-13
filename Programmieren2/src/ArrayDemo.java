
public class ArrayDemo
{

	public static void main(String[] args)
	{
		int[] array1 = { 3, 4, 5 };
		int[] array2 = { 7, 1, 3 };

		summe(array1, array2);
		multi(array1, array2);

	}

	public static void summe(int[] array1, int[] array2)
	{
		int summe = 0;

		System.out.print("Summen der Zahlen sind: ");

		for (int i = 0; i < array1.length; i++)
		{
			summe = array1[i] + array2[i];
			System.out.print(summe + " ");
		}
		System.out.println();
	}

	public static void multi(int[] array1, int[] array2)
	{
		int multi = 0;

		System.out.print("Produkte der Zahlen sind: ");

		for (int j = 0; j < array1.length; j++)
		{
			multi = array1[j] * array2[j];
			System.out.print(multi + " ");
		}

	}

}
