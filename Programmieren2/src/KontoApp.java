

public class KontoApp
{
	public static void main(String[] args)
	{
		Konto konto1 = new Konto();
		Konto konto3 = new Konto();
	
		int summe = Konto.summe(2, 3);			// Aufruf einer statischen Methode ohne Objekt
		System.out.println(summe);	
		
		konto3.summe(4, 5);						// Aufruf einer statischen Methode mit Objekt
		System.out.println(konto3);
		System.out.println(konto3.summe(4, 5));
		System.out.println("-------------");	// Aufruf einer statischen Methode mit Objekt
					
		konto1.setInhaber("Hugo");
		konto1.setKontostand(90.00);
		konto1.getKontostand();

		konto1.aufbuchen(56);
		System.out.println(konto1);
		
		konto1.aufbuchen(200);
		System.out.println(konto1);
		
		konto1.abbuchen(342);
		System.out.println(konto1);
				
		konto1.abbuchen(80);
		System.out.println(konto1);
		
		konto1.getKontostand();
	}

}
