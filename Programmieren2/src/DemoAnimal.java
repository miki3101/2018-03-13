import org.campus02.emp.Beagle;
import org.campus02.emp.Cat;
import org.campus02.emp.Dog;

public class DemoAnimal
{

	public static void main(String[] args)
	{
		Dog dog1 = new Dog();
		Cat cat1 = new Cat();
		
		dog1.name = "Niko";
		cat1.name = "Fauchi";
		
		dog1.makeNoise();
		dog1.move();
		cat1.makeNoise();
		cat1.move();
		
		Beagle dog2 = new Beagle();
		
		dog2.name = "Wuschel";
		dog2.makeNoise();
		dog2.loveFood = "Knochen";

	}

}

