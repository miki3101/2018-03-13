package org.campus02.oop;

public class CashPayment extends Payment
{
	private String name;

	public CashPayment(double amount, String currency)
	{
		super(amount, currency);
		
	}
	
	public CashPayment(double amount, String currency, String name)
	{
		super(amount, currency);
		this.name = name;
	}
	
	public double calcTransactionCosts()
	{
		double x = 0;
		
		if(name == null)
			x = 2;
		else
			x= 0;
		
		return x;
	}

	public String toString()
	{
		return "[CashPayment,  Name: " + name  + ", Amount: " + getAmount() + ", Currency: " + getCurrency() + ", TransactionCosts: " + calcTransactionCosts() + "]";
	}

	

}
