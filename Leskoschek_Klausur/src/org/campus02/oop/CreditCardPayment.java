package org.campus02.oop;

public class CreditCardPayment extends Payment
{

	private String cardNumber;
	private String securityCode;

	public CreditCardPayment(double amount, String currency, String cardNumber, String securityCode)
	{
		super(amount, currency);
		this.cardNumber = cardNumber;
		this.securityCode = securityCode;
		
		
	}

	@Override
	public double calcTransactionCosts()
	{
		double x = 0;
		
		if(getCurrency()!="EUR")
			x = (getAmount() + 1.5) * (getAmount() / 100 * 2.75);
		
		else
			x = (getAmount() + 0.5) * (getAmount() / 100 * 1.1);
		
		return x;
	}

	public String toString()
	{
		return "[CreditCardPayment,  CardNumber: " + cardNumber + ", SecurityCode: " + securityCode + ", Amount: " + getAmount() + ", Currency: " + getCurrency() + ", TransactionsCosts" + calcTransactionCosts() +  "]";
	}
	
	

}
