package org.campus02.oop;

import java.util.Comparator;

public class PaymentAmountDescComparator implements Comparator<Payment>
{

	public int compare(Payment o1, Payment o2)
	{	
		return Double.compare(o1.getAmount(), o2.getAmount()) * -1;
	}

}
