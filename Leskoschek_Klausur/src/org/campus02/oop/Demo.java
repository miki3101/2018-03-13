package org.campus02.oop;

public class Demo
{

	public static void main(String[] args)
	{
		PaymentJournal pm = new PaymentJournal();
		
		CreditCardPayment ccp = new CreditCardPayment(1000, "EUR", "Michi", "1234");
		CashPayment cp = new CashPayment(2000, "USD");
		MaestroPayment mp = new MaestroPayment(700, "Schilling", "12345");
		
		pm.add(ccp);
		pm.add(cp);
		pm.add(mp);
		
		System.out.println(pm.sortByAmount());
		System.out.println(pm.getPaymentsPerCurrency());
		System.out.println(pm.totalTransabtionCosts());
	}

}
