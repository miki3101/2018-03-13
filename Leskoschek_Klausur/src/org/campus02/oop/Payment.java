package org.campus02.oop;

public abstract class Payment
{
	
	private double amount;
	private String currency;

	public Payment(double amount, String currency)
	{
		this.amount = amount;
		this.currency = currency;
	}
	
	public abstract double calcTransactionCosts();
	
	public double exchangeToEUR()
	{
		double x = 0;
		
		switch (currency)
		{
		case "USD":
			x = amount / 1.1;
			break;
		case "GBP":
			x = amount / 0.85;
			break;
		case "SEK":
			x = amount / 9.5;
			break;
		case "HUF":
			x = amount / 310;
			break;


		default:
			x = amount / 2;
			break;
		}
		return x;
	}

	public double getAmount()
	{
		return amount;
	}

	public String getCurrency()
	{
		return currency;
	}

	@Override
	public String toString()
	{
		return "Amount: " + amount + ", Currency: " + currency;
	}
	
	

}
