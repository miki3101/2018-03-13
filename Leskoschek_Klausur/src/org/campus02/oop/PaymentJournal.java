package org.campus02.oop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class PaymentJournal
{
	private ArrayList<Payment> journal;

	public PaymentJournal()
	{
		journal = new ArrayList<Payment>();
	}

	public void add(Payment p)
	{
		journal.add(p);
	}

	public ArrayList<Payment> sortByAmount()
	{
		Collections.sort(journal, new PaymentAmountDescComparator());
		return journal;
		
	}

	public double totalTransabtionCosts()
	{
		double x = 0;

		for (Payment payment : journal)
		{
			x += payment.calcTransactionCosts();
		}

		return x;
	}

	public HashMap<String, Integer> getPaymentsPerCurrency()
	{
		HashMap<String, Integer> map = new HashMap<>();

		for (Payment payment : journal)
		{

			if (map.containsKey(payment.getCurrency()))
			{
				int zahl = map.get(payment.getCurrency());
				zahl++;
				map.put(payment.getCurrency(), zahl);
			}

			{
				map.put(payment.getCurrency(), 1);
			}
		}
		return map;
	}

	@Override
	public String toString()
	{
		return "PaymentJournal [Journal: " + journal + "]";
	}
	
	

}
