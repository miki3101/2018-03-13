package org.campus02.oop;

public class MaestroPayment extends Payment
{
	
	private String cardNumber; 

	public MaestroPayment(double amount, String currency, String cardNumber)
	{
		super(amount, currency);
		this.cardNumber = cardNumber;
	}

	@Override
	public double calcTransactionCosts()
	{
		double x = 0;
		
		x = getAmount() /100 * 0.75;
		
		if(x < 0.95)
			return 0.95;
		
		return x;
	}

	public String toString()
	{
		return "Maestro, CardNumber: " + cardNumber + ", Amount: " + getAmount() + ", Currency: " + getCurrency() + ", TransactionCosts: " + calcTransactionCosts() + "]";
	}
	
	

	
}
