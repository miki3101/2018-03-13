package org.campus02.rec;

import java.util.ArrayList;

public class Folder extends FSEntry
{

	private ArrayList<FSEntry> files = new ArrayList<>();
	private ArrayList<FSEntry> folders = new ArrayList<>();

	public Folder(String name)
	{
		super(name);
	}

	public void putEntry(File entry)
	{
		files.add(entry);
	}

	public void putEntry(Folder entry)
	{
		folders.add(entry);
	}

	@Override
	public void print()
	{
		System.out.println(name + ": Folder");
	}

	public void printAll()
	{
		for (FSEntry fsEntry : folders)
		{
			fsEntry.print();

			for (FSEntry fsEntry2 : files)
			{
				fsEntry2.print();
			}
		}
	}

	public void listAllContent(String prefixPath)
	{

		for (FSEntry fsEntry : files)
		{
			File file = (File) fsEntry;
			file.print();
			
			for (FSEntry fsEntry2 : folders)
			{
				Folder folder = (Folder) fsEntry2;
				folder.listAllContent(prefixPath + "/" + folder.getName());
			}
			
		}

	}

	public void printTree(String indent)
	{

	}

	@Override
	public String toString()
	{
		return "Folder [files=" + files + ", folders=" + folders + "]";
	}

}
