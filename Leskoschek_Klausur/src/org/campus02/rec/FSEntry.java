package org.campus02.rec;

public abstract class FSEntry {

	protected String name;

	public FSEntry(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
	public abstract void print();

	@Override
	public String toString()
	{
		return "FSEntry [name: " + name + "]";
	}
	
	
}
