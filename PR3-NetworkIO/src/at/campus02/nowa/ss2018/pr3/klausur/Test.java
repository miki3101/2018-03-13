package at.campus02.nowa.ss2018.pr3.klausur;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

class Test {

	@org.junit.jupiter.api.Test
	void testTime() {
		try (
			BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
			Socket socket = new Socket(InetAddress.getLocalHost(), 1111);
			BufferedReader brs = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter pws = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		) {
			pws.println("time");
			pws.flush();
			assertTrue(brs.readLine().endsWith(" 2018"));
		} catch (UnknownHostException e) {
			fail("Could not resolve remote host: " + e.getMessage());
		} catch (IOException e) {
			fail("Could not connect to remote host: " + e.getMessage());
		}
	}
	
	
	@org.junit.jupiter.api.Test
	void testClients() throws InterruptedException {
		int parallel = 4;
		List<Socket> sockets = new ArrayList<>();
		try (
			BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
			Socket socket = new Socket(InetAddress.getLocalHost(), 1111);
			BufferedReader brs = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter pws = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		) {
			pws.println("clients");
			pws.flush();
			assertTrue(brs.readLine().endsWith(" 1"));
			for (int i = 0; i < parallel; i++) {
				sockets.add(new Socket(InetAddress.getLocalHost(), 1111));
			}
			Thread.sleep(1000);
			pws.println("clients");
			pws.flush();
			assertTrue(brs.readLine().endsWith(" 5"));
			for (Socket s : sockets) {
				s.close();
			}
			Thread.sleep(1000);
			pws.println("clients");
			pws.flush();
			assertTrue(brs.readLine().endsWith(" 1"));
		} catch (UnknownHostException e) {
			fail("Could not resolve remote host: " + e.getMessage());
		} catch (IOException e) {
			fail("Could not connect to remote host: " + e.getMessage());
		}
	}
	
	@org.junit.jupiter.api.Test
	void testUnknown() {
		try (
			BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
			Socket socket = new Socket(InetAddress.getLocalHost(), 1111);
			BufferedReader brs = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter pws = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		) {
			pws.println("hello");
			pws.flush();
			assertTrue(brs.readLine().toLowerCase().contains("unknown"));
		} catch (UnknownHostException e) {
			fail("Could not resolve remote host: " + e.getMessage());
		} catch (IOException e) {
			fail("Could not connect to remote host: " + e.getMessage());
		}
	}

}
