package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server
{

	private static int counter = 1;

	public static void main(String[] args)
	{

		try (ServerSocket server = new ServerSocket(1111);)
		{
			while (true)
			{
				Socket socket = server.accept();
				System.out.println("Verbindung zu Client: " + socket + " hergestellt");

				ClientRunnable cr = new ClientRunnable(socket, counter);
				counter++;

				Thread t = new Thread(cr);
				t.start();
			}

		} catch (IOException e)
		{
			e.printStackTrace();	
		} 
	}

	public int getCounter()
	{
		return counter;
	}

}
