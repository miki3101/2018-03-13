package at.campus02.nowa.ss2018.pr3.klausur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;

public class ClientRunnable implements Runnable
{

	private Socket socket;
	private static int counter;
	private Date now = new Date();

	public ClientRunnable(Socket socket, int counter)
	{
		this.socket = socket;
		ClientRunnable.counter = counter;
	}

	@Override
	public void run()
	{

		try (BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));)
		{
			String line;

			while ((line = br.readLine()) != null)
			{

				synchronized (ClientRunnable.class)
				{

					switch (line)
					{
					case "time":
						pw.println(now.toString());
						pw.flush();
						break;

					case "clients":
						pw.println("Connected clients: " + ClientRunnable.counter);
						pw.flush();
						break;

					default:
						pw.println("Unknown command");
						pw.flush();
						break;
					}
				}
			}

		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			synchronized (ClientRunnable.class)
			{
				ClientRunnable.counter--;
			}
		}
	}

}
