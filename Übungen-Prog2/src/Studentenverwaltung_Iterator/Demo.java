package Studentenverwaltung_Iterator;

import java.util.LinkedList;
import java.util.ListIterator;

public class Demo
{

	public static void main(String[] args)
	{
		LinkedList<Student> studenten = new LinkedList<>();
		
		Student s1 = new Student("Max");
		Student s2 = new Student("Peter");
		Student s3 = new Student("John");
		Student s4 = new Student("Phil");
		
		studenten.addFirst(s1);
		studenten.addFirst(s2);
		studenten.addFirst(s3);
		studenten.addFirst(s4);
		
		ListIterator<Student> it = studenten.listIterator();
		
		System.out.println("Iterator zeigt auf: " + it.next());
		System.out.println("Iterator zeigt auf: " + it.next());
		System.out.println("-----------\n");
		
		it = studenten.listIterator();
		
		while(it.hasNext())
		{
			Student p = it.next();
			System.out.println(p.getName());
		}
		
		System.out.println("------------\n");
		studenten.add(new Student("Marie"));
		studenten.add(new Student ("Nena"));
		
		it = studenten.listIterator();
		
		while(it.hasNext())
		{
			Student p = it.next();
			System.out.println(p.getName());
		}
		
		it = studenten.listIterator();
		
		System.out.println("Iterator zeigt auf: " + it.next());
		System.out.println("----------\n");
		
		
		it.remove();
		
		it = studenten.listIterator();
		while(it.hasNext())
		{
			Student p = it.next();
			System.out.println(p.getName());
		}

	}

}
