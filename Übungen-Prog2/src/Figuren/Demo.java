package Figuren;

public class Demo
{

	public static void main(String[] args)
	{
		FigureManager fm = new FigureManager();
		
		fm.add(new Rectangle(10, 20));
		fm.add(new Circle(8.4));
		fm.add(new Circle(9));
		fm.add(new Rectangle(21, 70));
		fm.add(new Rectangle(199, 200.1));
		
		
		System.out.println(fm.getAverageAreaSize());
		System.out.println(fm.getMaxPerimeter());
		System.out.println(fm.getAreaBySizeCategories());

	}

}
