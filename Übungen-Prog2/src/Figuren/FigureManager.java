package Figuren;

import java.util.ArrayList;
import java.util.HashMap;

public class FigureManager
{
	private ArrayList<Figure> figuren = new ArrayList<>();

	public void add(Figure f)
	{
		figuren.add(f);
	}

	public double getMaxPerimeter()
	{
		double max = 0;

		for (Figure figure : figuren)
		{
			if (figure.getPerimeter() > max)
				max = figure.getPerimeter();
		}
		return max;
	}

	public double getAverageAreaSize()
	{
		double avg = 0;

		for (Figure figure : figuren)
		{
			avg += figure.getArea();
		}
		return avg / figuren.size();
	}

	public HashMap<String, Double> getAreaBySizeCategories()
	{
		HashMap<String, Double> size = new HashMap<>();
		
		size.put("klein", 0.0);
		size.put("mittel", 0.0);
		size.put("gro�", 0.0);
		
		for (Figure figure : figuren)
		{
			if(figure.getArea()< 1000)
			{
				double value = size.get("klein");
				value += figure.getArea();
				size.put("klein", value);
			}
			else if(figure.getArea()< 5000)
			{
				double value = size.get("mittel");
				value += figure.getArea();
				size.put("mittel", value);
			}
			else
			{
				double value = size.get("gro�");
				value += figure.getArea();
				size.put("gro�", value);
			}
		}
		
		return size;
	}
}
