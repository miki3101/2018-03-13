package Soziales_MayBe;

public class Profil
{
	private String name;
	private String adress;
	private int age;
	private MayBe<String> email;
	private char gender;
	private MayBe<Double> salary;
	
	public Profil(String name, String adress, int age, String email, char gender, double salary)
	{
		this.name = name;
		this.adress = adress;
		this.age = age;
		this.email = new MayBe<String>(email, 1);
		this.gender = gender;
		this.salary = new MayBe<Double>(salary, 0);
	}
	
	public void setSalaryStatus(int status)
	{
		this.salary.setStatus(status);
	}
	
	public void print()
	{
		String profil = "Profil:\n" + "Name: " + name + ", Adresse: " + adress + ", Alter: " + age + " Geschlecht: " + gender;
		
		System.out.println(profil);
		System.out.printf("Email: ");
		this.email.print();
		System.out.printf("Gehalt: ");
		this.salary.print();
	}
	
	
	
}
