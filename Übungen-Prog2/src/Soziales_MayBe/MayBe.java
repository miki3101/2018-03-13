package Soziales_MayBe;

public class MayBe <T>
{
	private T value;
	private int status;
	
	public MayBe(T value, int status)
	{
		this.value = value;
		this.status = 1;
	}
	
	public void setStatus(int status)
	{
		if(status >= 0 || status <= 1)
			this.status = status;
	}
	
	public void print()
	{
		switch (status)
		{
		case 0:
			System.out.println("Daten werden nicht angezeigt");
			break;
		case 1:
			System.out.println(value);
			break;
		case 3:
			System.out.println("Daten wurden nicht gesetzt");
			break;
		default:
			break;
		}
	}

	
}
