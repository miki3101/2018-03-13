package PegelVerwaltung;

import java.util.ArrayList;

public class WasserstandManager
{
	private ArrayList<Wasserstand> wasserstaende = new ArrayList<>();
	
	public void addWasserstand(Wasserstand x)
	{
		wasserstaende.add(x);
	}
	
	public Wasserstand findByID(int id)
	{
		Wasserstand x = null;
		
		for (Wasserstand wasserstand : wasserstaende)
		{
			if(wasserstand.getId() == id)
				x = wasserstand;
		}
		return x;
	}
	
	public ArrayList<Wasserstand> findAllByGewaesser(String name)
	{
		ArrayList<Wasserstand> gewaesser = new ArrayList<>();
		
		for (Wasserstand wasserstand : wasserstaende)
		{
			if(wasserstand.getName() == name)
				gewaesser.add(wasserstand);
		}
		
		return gewaesser;
	}
	
	public Wasserstand findLastWasserstand(String name)
	{
		Wasserstand x = wasserstaende.get(0);
		
		for (Wasserstand wasserstand : wasserstaende)
		{
			if(wasserstand.getName() == name && wasserstand.getZeitpunkt() > x.getZeitpunkt())
			{
				x = wasserstand;
			}
		}
		
		return x;
	}
	
	public ArrayList<Wasserstand> findForAlamierung()
	{
		ArrayList<Wasserstand> x = new ArrayList<>();
		
		for (Wasserstand wasserstand : wasserstaende)
		{
			if(wasserstand.getMessWert() >= wasserstand.getAlamieren())
				x.add(wasserstand);
		}
		
		return x;
	}
	
	public double calcMittlererWasserstand(String name, String ort)
	{
		double calc = 0;
		int i = 0;
		
		for (Wasserstand wasserstand : wasserstaende)
		{
			if(wasserstand.getName() == name && wasserstand.getOrt() == ort)
				i++;
				calc+= wasserstand.getMessWert();
		}
		return calc / i;
	}
	
	public ArrayList<Wasserstand> findByZeitpunkt(int von, int bis, String name, String ort)
	{
		ArrayList<Wasserstand> zeit = new ArrayList<>();
		
		for (Wasserstand wasserstand : wasserstaende)
		{
			if(wasserstand.getName() == name && wasserstand.getOrt() == ort && wasserstand.getZeitpunkt()> von && wasserstand.getZeitpunkt()< bis)
				zeit.add(wasserstand);
		}
		return zeit;
	}

	@Override
	public String toString()
	{
		return "Wasserstände: " + wasserstaende;
	}
	
}
