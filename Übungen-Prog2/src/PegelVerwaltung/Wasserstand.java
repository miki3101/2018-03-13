package PegelVerwaltung;

public class Wasserstand
{
	private int id;
	private String name;
	private double messWert;
	private double alamieren;
	private long zeitpunkt;
	private String ort;
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public double getMessWert()
	{
		return messWert;
	}
	public void setMessWert(double messWert)
	{
		this.messWert = messWert;
	}
	public double getAlamieren()
	{
		return alamieren;
	}
	public void setAlamieren(double alamieren)
	{
		this.alamieren = alamieren;
	}
	public long getZeitpunkt()
	{
		return zeitpunkt;
	}
	public void setZeitpunkt(long zeitpunkt)
	{
		this.zeitpunkt = zeitpunkt;
	}
	public String getOrt()
	{
		return ort;
	}
	public void setOrt(String ort)
	{
		this.ort = ort;
	}
	@Override
	public String toString()
	{
		return "\nID: " + id + ", Name: " + name + ", Messwert: " + messWert + ", Alamieren ab: " + alamieren
				+ ", Zeitpunkt seit: " + zeitpunkt + " Sekunden"+ ", Ort: " + ort;
	}
	
	
}
