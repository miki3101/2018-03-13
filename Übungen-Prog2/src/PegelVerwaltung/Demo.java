package PegelVerwaltung;

public class Demo
{

	public static void main(String[] args)
	{
		Wasserstand w1 = new Wasserstand();
		w1.setName("Schwarzl");
		w1.setId(1);
		w1.setMessWert(5.2);
		w1.setAlamieren(6.0);
		w1.setZeitpunkt(3500);
		w1.setOrt("Graz");
		
		Wasserstand w2 = new Wasserstand();
		w2.setName("Hilmteich");
		w2.setId(2);
		w2.setMessWert(4.3);
		w2.setAlamieren(6.0);
		w2.setZeitpunkt(4700);
		w2.setOrt("Graz");
		
		Wasserstand w3 = new Wasserstand();
		w3.setName("Grüner See");
		w3.setId(3);
		w3.setMessWert(8.8);
		w3.setAlamieren(4.2);
		w3.setZeitpunkt(2600);
		w3.setOrt("Steiermark");
		
		WasserstandManager mg = new WasserstandManager();
		
		mg.addWasserstand(w1);
		mg.addWasserstand(w2);
		mg.addWasserstand(w3);
		
		System.out.println("Wasserstand mit der ID 3: " + mg.findByID(3));
		System.out.println("-----------");
		System.out.println("Alle Wasserstände: " + mg);
		System.out.println("----------");
		System.out.println("Neuester Wasserstand: " + mg.findLastWasserstand("Schwarzl"));
		System.out.println("-------------");
		System.out.println("Wasserstände die alamierend sind: " + mg.findForAlamierung());
		System.out.println("-----------");
		System.out.println("Durchschnittlicher Wasserstand in Graz: " + mg.calcMittlererWasserstand("Schwarzl", "Graz"));
		System.out.println("--------------");
		System.out.println("Wasserstände die gemessen wurden: " + mg.findByZeitpunkt(3000, 5000, "Schwarzl", "Graz"));

	}

}
