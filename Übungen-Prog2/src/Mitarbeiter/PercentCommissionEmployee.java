package Mitarbeiter;

public class PercentCommissionEmployee extends Employee
{
	private double perscentCommission;

	public PercentCommissionEmployee(String lastname, String firstname, String departement, double baseSalary, double perscentCommission)
	{
		super(lastname, firstname, departement, baseSalary);
		this.perscentCommission = perscentCommission;
	}
	
	public double getFullSalary()
	{
		return baseSalary + (baseSalary / 100 * perscentCommission);
	}

}
