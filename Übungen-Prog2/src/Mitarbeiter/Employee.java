package Mitarbeiter;

public class Employee
{
	protected String lastname;
	protected String firstname;
	protected String departement;
	protected double baseSalary;
	
	
	public Employee(String lastname, String firstname, String departement, double baseSalary)
	{
		super();
		this.lastname = lastname;
		this.firstname = firstname;
		this.departement = departement;
		this.baseSalary = baseSalary;
	}
	
	public double getFullSalary()
	{
		return baseSalary;
	}

	public String getDepartement()
	{
		return departement;
	}

	public double getBaseSalary()
	{
		return baseSalary;
	}
	
	
}
