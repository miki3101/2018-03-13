package Mitarbeiter;

public class FixCommissionEmployee extends Employee
{

	private double additionalCommission;
	
	public FixCommissionEmployee(String lastname, String firstname, String departement, double baseSalary, double additionalCommission)
	{
		super(lastname, firstname, departement, baseSalary);
		this.additionalCommission = additionalCommission;
	}
	
	public double getFullSalary()
	{
		return baseSalary + additionalCommission;
	}

}
