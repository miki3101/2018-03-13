package Mitarbeiter;

import java.util.ArrayList;
import java.util.HashMap;

public class EmployeeManager
{
	private ArrayList<Employee> mitarbeiter = new ArrayList<>();
	
	public void addEmployee(Employee e)
	{
		mitarbeiter.add(e);
	}
	
	public double calcTotalSalary()
	{
		double salary = 0;
		
		for (Employee employee : mitarbeiter)
		{
			salary += employee.getFullSalary();
		}
		
		return salary;
	}
	
	public HashMap<String, Double> getSalaryByDepartment()
	{
		HashMap<String, Double> result = new HashMap<>();
		

		for (Employee employee : mitarbeiter)
		{
			if(result.containsKey(employee.getDepartement()))
					{
						double salary = result.get(employee.departement) + employee.getFullSalary(); 
						result.put(employee.getDepartement(), salary);
					}
			else
			{
				result.put(employee.getDepartement(), employee.getFullSalary());
			}
		}
		
		return result;
	}

	public String toString()
	{
		return "EmployeeManager [mitarbeiter=" + mitarbeiter + "]";
	}
	
	
	
}
