package Mitarbeiter;

public class Demo
{

	public static void main(String[] args)
	{
		Employee e1 = new Employee("Huber", "Hans", "Verwaltung", 1200.00);
		Employee e2 = new Employee("Sorglos", "Susi", "Marketing", 1300.00);
		Employee e3 = new Employee("Mustermann", "Max", "Buchhaltung", 1400.00);
		Employee e4 = new Employee("Anna", "Friedrich", "Programmierer", 1500.00);
		FixCommissionEmployee f1 = new FixCommissionEmployee("HuberF", "Hans", "Verwaltung", 1800.00, 300);
		FixCommissionEmployee f2 = new FixCommissionEmployee("SorglosF", "Susi", "Marketing", 1500.00,200);
		FixCommissionEmployee f3 = new FixCommissionEmployee("MustermannF", "Max", "Buchhaltung", 1200.00,400);
		FixCommissionEmployee f4 = new FixCommissionEmployee("AnnaF", "Friedrich", "Programmierer", 1300.00, 500);
		PercentCommissionEmployee p1 = new PercentCommissionEmployee("HuberP", "Hans", "Verwaltung", 1600.00,10);
		PercentCommissionEmployee p2 = new PercentCommissionEmployee("SorglosP", "Susi", "Marketing", 1400.00,15);
		PercentCommissionEmployee p3 = new PercentCommissionEmployee("MustermannP", "Max", "Buchhaltung", 1200.00,20);
		PercentCommissionEmployee p4 = new PercentCommissionEmployee("AnnaP", "Friedrich", "Programmierer", 1200.00,10);
		
		EmployeeManager em = new EmployeeManager();
		
		em.addEmployee(e4); em.addEmployee(e3); em.addEmployee(e2); em.addEmployee(e1);
		em.addEmployee(f1); em.addEmployee(f2); em.addEmployee(f3); em.addEmployee(f4);
		em.addEmployee(p4); em.addEmployee(p3); em.addEmployee(p2); em.addEmployee(p1);
		
		System.out.println("Geh�lter aller Angestellten: " + em.calcTotalSalary());
		System.out.println("Geh�lter nach Abteilung: " + em.getSalaryByDepartment());
	
	}

}
