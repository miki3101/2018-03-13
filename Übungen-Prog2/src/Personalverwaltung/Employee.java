package Personalverwaltung;

public class Employee
{
	private int empNumber;
	private String name;
	private double salary;
	private String department;
	
	
	public Employee(int empNumber, String name, double salary, String department)
	{
		this.empNumber = empNumber;
		this.name = name;
		this.salary = salary;
		this.department = department;
	}

	public double getSalary()
	{
		return salary;
	}

	public void setSalary(double salary)
	{
		this.salary = salary;
	}

	public int getEmpNumber()
	{
		return empNumber;
	}

	public String getName()
	{
		return name;
	}

	public String getDepartment()
	{
		return department;
	}

	public String toString()
	{
		return "Employee: \n" + "EmpNumber: " + empNumber + ", Name: " + name + ", Salary: " + salary + ", Department: "
				+ department;
	}
	
	
	
	
}
