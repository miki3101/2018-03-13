package Personalverwaltung;

import java.util.ArrayList;
import java.util.List;

public class EmployeeManager
{
	private List<Employee> employees = new ArrayList<>();
	
	public void addEmployee(Employee e)
	{
		employees.add(e);
	}
	
	public Employee findByMaxSalary()
	{
		Employee e = employees.get(0);
		
		for (Employee employee : employees)
		{
			if(employee.getSalary()> e.getSalary())
				e= employee;
		}
		
		return e;
	}
	
	public Employee findByEmpNumber(int number)
	{
		Employee e = employees.get(0);
		
		for (Employee employee : employees)
		{
			if(employee.getEmpNumber() == number)
				e= employee;
		}
		return e;
	}
	
	public ArrayList<Employee> findByDepartment(String department)
	{
		ArrayList<Employee> e = new ArrayList<>();
		
		for (Employee employee : employees)
		{
			if(employee.getDepartment() == department)
				e.add(employee);
		}
		return e;
	}

	@Override
	public String toString()
	{
		return "EmployeeManager\n" + employees;
	}
	
}
