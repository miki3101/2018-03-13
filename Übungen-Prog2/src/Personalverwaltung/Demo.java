package Personalverwaltung;

public class Demo
{

	public static void main(String[] args)
	{
		Employee e1 = new Employee(3, "Susi", 1200, "Marketing");
		Employee e2 = new Employee(5, "Max", 1270, "Marketing");
		Employee e3 = new Employee(6, "Hans", 1320, "Buchhaltung");
		Employee e4 = new Employee(7, "Maria", 1340, "Buchhaltung");
		Employee e5 = new Employee(9, "Alex", 1200, "Verwaltung");
		Employee e6 = new Employee(2, "Markus", 1710, "Verwaltung");
		Employee e7 = new Employee(1, "Sandra", 1820, "Marketing");
		Employee e8 = new Employee(4, "Thomas", 1630, "Buchhaltung");
		Employee e9 = new Employee(8, "Michi", 1920, "Verwaltung");
		
		EmployeeManager em = new EmployeeManager();
		
		em.addEmployee(e9);
		em.addEmployee(e8);
		em.addEmployee(e7);
		em.addEmployee(e6);
		em.addEmployee(e5);
		em.addEmployee(e4);
		em.addEmployee(e3);
		em.addEmployee(e2);
		em.addEmployee(e1);
		
		System.out.println("Angestellter mit meistem Gehalt: " + em.findByMaxSalary());
		System.out.println("------------\n");
		
		System.out.println("Angestellter mit der Nr. 3 " + em.findByEmpNumber(3));
		System.out.println("---------------\n");
		
		System.out.println("Liste der Angestellten in der Abteilung Verwaltung: " + em.findByDepartment("Verwaltung"));
		
		
		

	}

}
