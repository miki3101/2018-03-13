package BlackJack;

import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

public class BlackJack
{
	private HashMap<Player, Integer> players = new HashMap<>();
	
	public boolean addPlayer(Player p)
	{
		if(!players.containsKey(p))
		{
			players.put(p, 0);
			return true;
		}
		return false;
	}
	
	public boolean addCard(Player p, Integer cv)
	{
		if(players.containsKey(p))
		{
			players.put(p, this.getValue(p) + cv);
			return true;
		}
		return false;
	}
	
	public Integer getValue(Player p)
	{
		return players.get(p);
	}

	public String toString()
	{
		String spieler = "";
		
		Set<Entry<Player,Integer>> set = players.entrySet();
		
		for (Entry<Player, Integer> entry : set)
		{
			spieler += entry.getKey().getName() + ", " + "Kartenwert: " + entry.getValue() + "\n";
		}
	
		return spieler;
		
	}
	
	
}
