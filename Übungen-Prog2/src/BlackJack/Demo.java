package BlackJack;

public class Demo
{

	public static void main(String[] args)
	{
		Player p1 = new Player("Max Mustermann", 20);
		Player p2 = new Player("Susi Sorglos", 22);
		
		BlackJack bj = new BlackJack();
		
		bj.addPlayer(p1);
		bj.addPlayer(p2);
		
		System.out.println("Spieler setzen sich an den Tisch:\n" + bj);
		
		bj.addCard(p1, 17);
		bj.addCard(p2, 20);
		
		System.out.println("Spieler haben Karten erhalten:\n" + bj);
		
		bj.addCard(p1, 12);
		bj.addCard(p2, 1);
		System.out.println("Spieler haben Karten erhalten:\n" + bj);
		
		
		
		
		
	}

}
