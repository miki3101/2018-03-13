package Land;

import java.util.ArrayList;

public class Bundesstaat extends Land
{

	private ArrayList<Land> laender = new ArrayList<>();
	
	public void addLand(Land a)
	{
		laender.add(a);
	}
	
	public double getBruttoSozialProdukt()
	{
		double bsp = 0;
		
		for (Land land : laender)
		{
			bsp += land.getBruttoSozialProdukt();
		}
	
		return bsp;
	}

}
