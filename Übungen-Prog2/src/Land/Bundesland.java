package Land;

public class Bundesland extends Land
{
	private double bsp;

	public Bundesland(double bsp)
	{
		this.bsp = bsp;
		
	}

	public double getBruttoSozialProdukt()
	{
		return bsp;
	}

}
