package Land;

public class Demo
{

	public static void main(String[] args)
	{
		Bundesland a = new Bundesland(200);
		Bundesland b = new Bundesland(400);
		
		Bundesstaat laender = new Bundesstaat();
		
		laender.addLand(a);
		laender.addLand(b);
		
		System.out.println(laender.getBruttoSozialProdukt());

	}

}
