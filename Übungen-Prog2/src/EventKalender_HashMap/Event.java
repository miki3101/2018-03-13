package EventKalender_HashMap;

public class Event
{
	private String titel;
	private String ort;
	private double preis;
	
	
	public String getTitel()
	{
		return titel;
	}

	public void setTitel(String titel)
	{
		this.titel = titel;
	}

	public String getOrt()
	{
		return ort;
	}

	public void setOrt(String ort)
	{
		this.ort = ort;
	}

	public double getPreis()
	{
		return preis;
	}

	public void setPreis(double preis)
	{
		this.preis = preis;
	}

	public String toString()
	{
		return "\nTitel: " + titel + ", Ort: " + ort + ", Preis: " + preis;
	}
	
	
}
