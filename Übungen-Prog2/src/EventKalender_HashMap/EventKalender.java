package EventKalender_HashMap;

import java.util.ArrayList;
import java.util.HashMap;

public class EventKalender
{
	private ArrayList<Event> events = new ArrayList<>();

	public void addEvent(Event e)
	{
		events.add(e);
	}

	public Event getByTitle(String titel)
	{
		Event e = null;

		for (Event event : events)
		{
			if (event.getTitel() == titel)
				e = event;
		}
		return e;
	}

	public ArrayList<Event> getByOrt(String ort)
	{
		ArrayList<Event> orte = new ArrayList<>();

		for (Event event : events)
		{
			if (event.getOrt() == ort)
				orte.add(event);
		}
		return orte;
	}

	public ArrayList<Event> getByEintrittsPreis(double min, double max)
	{
		ArrayList<Event> preis = new ArrayList<>();

		for (Event event : events)
		{
			if (event.getPreis() > min && event.getPreis() < max)
				preis.add(event);
		}
		return preis;
	}

	public Event getMostExpensiveByOrt(String ort)
	{
		Event teuer = events.get(0);

		for (Event event : events)
		{
			if (event.getOrt() == ort && event.getPreis() > teuer.getPreis())
				teuer = event;
		}

		return teuer;
	}

	public double getAvgPreisByOrt(String ort)
	{
		double preis = 0;
		int i = 0;

		for (Event event : events)
		{
			if (event.getOrt() == ort)
				i++;
			preis += event.getPreis();
		}
		return preis / i;
	}

	public HashMap<String, Integer> getCountEventsByOrt()
	{

		HashMap<String, Integer> x = new HashMap<>();
		
		for (Event event : events)
		{
			if(x.containsKey(event.getOrt()))
			{
			int zahl = x.get(event.getOrt());
			zahl++;
			x.put(event.getOrt(), zahl);
			}else {
				x.put(event.getOrt(), 1);
			}
		}
		
		return x;
	}
	
	public HashMap<String, Double> getSumPriceEventsByOrt()
	{HashMap<String, Double> x = new HashMap<>();
	
	for (Event event : events)
	{
		if(x.containsKey(event.getOrt()))
		{
			double zahl = x.get(event.getOrt());
			zahl+= event.getPreis();
		x.put(event.getOrt(), zahl);
		}
		else {
			x.put(event.getOrt(), event.getPreis());
		}
		
		}
			
	return x;
	}

	@Override
	public String toString()
	{
		return "EventKalender: " + events;
	}
	
	

}
