package EventKalender_HashMap;

public class Demo
{

	public static void main(String[] args)
	{
		Event e1 = new Event();
		Event e2 = new Event();
		Event e3 = new Event();
		Event e4 = new Event();
		Event e5 = new Event();
		Event e6 = new Event();
		Event e7 = new Event();
		
		e1.setOrt("Graz");
		e1.setPreis(150.00);
		e1.setTitel("EAV");
		e2.setOrt("Graz");
		e2.setPreis(250.00);
		e2.setTitel("Sting");
		e3.setOrt("Wien");
		e3.setPreis(350.00);
		e3.setTitel("Phil Collins");
		e4.setOrt("Wien");
		e4.setPreis(270.00);
		e4.setTitel("Fanta4");
		e5.setOrt("Salzburg");
		e5.setPreis(220.00);
		e5.setTitel("EAV");
		e6.setOrt("Salzburg");
		e6.setPreis(130.00);
		e6.setTitel("Helene Fischer");
		e7.setOrt("Graz");
		e7.setPreis(280.00);
		e7.setTitel("Helene Fischer");
		
		EventKalender ek = new EventKalender();
		
		ek.addEvent(e1);
		ek.addEvent(e2);
		ek.addEvent(e3);
		ek.addEvent(e4);
		ek.addEvent(e5);
		ek.addEvent(e6);
		ek.addEvent(e7);
		
		System.out.println("Alle Events: " + ek);
		System.out.println("-----------");
		System.out.println("Events die zwischen 130 und 240 kosten:" + ek.getByEintrittsPreis(130, 240));
		System.out.println("-----------");
		System.out.println("Events die in Wien stattfinden:" + ek.getByOrt("Wien"));
		System.out.println("-----------");
		System.out.println("Event das den Titel Sting hat:" + ek.getByTitle("Sting"));
		System.out.println("-----------");
		System.out.println("Events  gez�hlt nach Orten:" + ek.getCountEventsByOrt());
		System.out.println("-----------");
		System.out.println("Events Preis zusammengerechnet nach Orten:" + ek.getSumPriceEventsByOrt());
		System.out.println("-----------");
		

	}

}
