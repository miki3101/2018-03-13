package Übungsklausur;


public class MaxSalaryAnalyzer extends PersonAnalyzer
{

	public MaxSalaryAnalyzer()
	{

	}

	public void analyze()
	{

		Person p = persons.get(0);

		for (Person person : persons)
		{
			if (person.getSalary() > p.getSalary())
				p = person;
		}
		
		for (Person person : persons)
		{
			if(person.getSalary() == p.getSalary())
				System.out.println("Personen mit dem höchsten Gehalt:\n" + person);
		}
	}

}
