package Übungsklausur;

public class GenderAnalyzer extends PersonAnalyzer
{
	
	public GenderAnalyzer()
	{
		
	}

	public void analyze()
	{
		int male = 0;
		int maleSize = 0;
		int female = 0;
		int femaleSize = 0;
		
		for (Person person : persons)
		{
			if(person.getGender() == 'M')
			{
				male++;
				maleSize += person.getSize();
			}
			
			else if (person.getGender() == 'F')
			{
				female++;
				femaleSize += person.getSize();
			}
		}
		
		System.out.println(male + " Männer, avg. Size: " +  maleSize / male + " cm");
		System.out.println(female + " Frauen, avg. Size: " + femaleSize / female + " cm");
	}

	
}
