package Übungsklausur;

import java.util.ArrayList;

public class BMIAnalyzer extends PersonAnalyzer
{
	private ArrayList<Pair<Person, Double>> result;
	
	public BMIAnalyzer()
	{
		result = new ArrayList<Pair<Person, Double>>();
	}
	
	private double calcBMI(Person p)
	{
		double bmi = p.getWeight() /(p.getSize()/ 100.0) * (p.getSize()/100.0);
		
		return bmi;
	}
	
	public void analyze()
	{
			
		for (Person person : persons)
		{
			System.out.printf("%s %s %s %.2f; %n ", person.getFirstname(), person.getLastname()," hat einen BMI von: ",  calcBMI(person));
		
			result.add(new Pair<Person, Double> (person, calcBMI(person)));
		}
		System.out.println();
	}

	public ArrayList<Pair<Person, Double>> getResult()
	{
		return result;
	}

	public void setResult(ArrayList<Pair<Person, Double>> result)
	{
		this.result = result;
	}

	public String toString()
	{
		return "Result: " + result;
	}

	
	
}
