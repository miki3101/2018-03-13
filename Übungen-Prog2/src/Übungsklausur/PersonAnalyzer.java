package Übungsklausur;

import java.util.ArrayList;

public abstract class PersonAnalyzer extends PersonManager
{
	protected ArrayList<Person> persons = new ArrayList<>();

	
	public PersonAnalyzer()
	{
		persons = new ArrayList<Person>();
	}
	
	
	public void setPersons(ArrayList<Person> persons)
	{
		this.persons = persons;
	}
	
	public abstract void analyze();
	
}
