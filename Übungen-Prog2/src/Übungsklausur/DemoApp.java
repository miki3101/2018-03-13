package �bungsklausur;

public class DemoApp
{
public static void main(String[] args) {
		
		PersonManager test = new PersonManager();
		
		for(int i = 0; i<100; i++)
		{
			test.add(createTestPerson());
		}
		
		GenderAnalyzer ga = new GenderAnalyzer();
		CountryAnalyzer ca = new CountryAnalyzer();
		MaxSalaryAnalyzer ma = new MaxSalaryAnalyzer();
		BMIAnalyzer bm = new BMIAnalyzer();
		
		System.out.println("Anzahl der M�nner und Frauen und deren durchschnittliche Gr��e:");
		test.doAnalysis(ga);
		System.out.println("-----------\nAnzahl der Personen nach L�ndern: ");
		test.doAnalysis(ca);
		System.out.println(ca.getResult());
		System.out.println("-------------");
		test.doAnalysis(ma);
		System.out.println("----------\nBMI aller Personen: ");
		test.doAnalysis(bm);
		System.out.println("-----------\nPersonen und deren BMI: ");
		
		System.out.println(bm.getResult());
		

	}
	
	
	public static Person createTestPerson()
	{
		String[] firstnames = new String[] {"John", "Max", "Susi", "Georg", "Gerald", "Michael", "Steve", "J�rg", "Sebastian", "Louis", "Thomas", "Tom", "Sandra", "Beate", "Birgit"};
		String[] lastnames = new String[] {"Muster", "Doe", "Schultz", "Cuevas", "Rhodes", "Mckenzie", "Taylor", "Glenn", "Reilly", "Morris", "Herman", "Beltran", "Swanson", "Roth"};
		String[] eyeColors = new String[] {"blau", "braun", "gr�n", "gelb"};
		String[] countries = new String[] {"AT", "DE", "CH", "SI"};
		char[] genders = new char[] {'M', 'F'};
		
		Person p = new Person(
				getRandom(firstnames), getRandom(lastnames), 
				getRandom(genders), 
				(int) (Math.random()* 100.0), 
				getRandom(countries), (int) (Math.random() * 5900) + 1000, 
				getRandom(eyeColors), (int) (Math.random() * 50) + 50,
				(int)(Math.random() * 50) + 160);
		
		
	
		return p;
	}
	
	public static String getRandom(String[] arr)
	{
		return arr[(int) (Math.random() * 100.0) % arr.length];
	}
	
	public static char getRandom(char[] arr)
	{
		return arr[(int) (Math.random() * 100) % arr.length];		
	}
	
}
