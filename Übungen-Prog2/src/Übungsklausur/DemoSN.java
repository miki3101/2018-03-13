package Übungsklausur;

public class DemoSN
{

	public static void main(String[] args)
	{
		SocialNetwork sn = SocialNetwork.generateDemoNetwork();

		System.out.println("Gesamtes Netzwerk: \n" + sn + "\n--------------");
		

		User franz = null;

		for (User user : sn.getUsers())
		{
			if (user.getName().equals("Franz"))
				franz = user;
		}

		System.out.println("Freunde von Franz: \n" + sn.findAllFriends(franz, 1, 2));

	}

}
