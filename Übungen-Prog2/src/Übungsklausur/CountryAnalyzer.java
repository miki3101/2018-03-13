package Übungsklausur;

import java.util.HashMap;

public class CountryAnalyzer extends PersonAnalyzer
{
	private HashMap<String, Integer>result;

	public CountryAnalyzer()
	{
		result = new HashMap<String, Integer>();
	}


	public void analyze()
	{
		for (Person person : persons)
		{
			
			if(result.containsKey(person.getCountry()))
			{
					int zahl = result.get(person.getCountry());
					zahl++;
					result.put(person.getCountry(), zahl);
			}
			else {
				result.put(person.getCountry(), 1);
			}
		}
		
		
	}

	public HashMap<String, Integer> getResult()
	{
		return result;
	}

	public void setResult(HashMap<String, Integer> result)
	{
		this.result = result;
	}

	
}
