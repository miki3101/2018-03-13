package Warenkorb_anonymer_Comparator;

import java.util.Comparator;

public class Username implements Comparator<Cart>
{

	public int compare(Cart c1, Cart c2)
	{
		if(c1.getUsername().compareTo(c2.getUsername()) < 0)
			return -1;
		if(c1.getUsername().compareTo(c2.getUsername()) > 0)
			return 1;
		
		return 0;
	}

}
