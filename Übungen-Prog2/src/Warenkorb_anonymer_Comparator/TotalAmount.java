package Warenkorb_anonymer_Comparator;

import java.util.Comparator;

public class TotalAmount implements Comparator<Cart>
{

	public int compare(Cart c1, Cart c2)
	{
		return Double.compare(c1.getTotalAmount(), c2.getTotalAmount());
	}

}
