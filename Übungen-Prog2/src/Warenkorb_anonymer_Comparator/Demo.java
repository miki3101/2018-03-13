package Warenkorb_anonymer_Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Demo
{

	public static void main(String[] args)
	{
		ArrayList<Cart> cart = new ArrayList<>();

		Cart c1 = new Cart("Franz", 33, 270, 1400.00);
		Cart c2 = new Cart("Alex", 21, 100, 1200.00);
		Cart c3 = new Cart("Max", 10, 820, 8400.00);
		Cart c4 = new Cart("Susi", 56, 600, 5400.00);

		cart.add(c1);
		cart.add(c2);
		cart.add(c3);
		cart.add(c4);

		System.out.println("Liste vor dem Sortieren: ");
		printArray(cart);
		System.out.println("----------");
		Collections.sort(cart, new TotalAmount());
		System.out.println("Liste nach dem Sortieren nach Total Amount:");
		printArray(cart);
		Collections.sort(cart, new Username());
		System.out.println("-----------\nListe nach dem Sortieren nach Username:");
		printArray(cart);
		Collections.sort(cart, new TotalItems());
		System.out.println("-----------\nListe nach dem Sortieren nach Total Items:");
		printArray(cart);

		Collections.sort(cart, new Comparator<Cart>()
		{
			public int compare(Cart c1, Cart c2)
			{
				return Double.compare(c1.getTotalAmount() / c1.getTotalItems(),
						c2.getTotalAmount() / c2.getTotalItems());
			}
		});
		
		
		System.out.println("----------\nListe nach dem Sortieren Amount per Item");
		printArray(cart);

	}

	public static void printArray(ArrayList<Cart> cart)
	{
		for (Cart i : cart)
		{
			System.out.print(i);
		}
		System.out.println();

	}
}
