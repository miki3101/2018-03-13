package Warenkorb_anonymer_Comparator;

import java.util.Comparator;

public class TotalItems implements Comparator<Cart>
{

	@Override
	public int compare(Cart c1, Cart c2)
	{
		return Double.compare(c1.getTotalItems(), c2.getTotalItems())*-1;
	}

}
