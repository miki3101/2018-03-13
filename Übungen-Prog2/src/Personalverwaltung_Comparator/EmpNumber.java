package Personalverwaltung_Comparator;

import java.util.Comparator;

public class EmpNumber implements Comparator<Employee>
{

	public int compare(Employee e1, Employee e2)
	{
		return Integer.compare(e1.getEmpNumber(), e2.getEmpNumber());
	
	}

}
