package Tiere_Vererbung_Polymorphismus;

public class Dog extends Animal
{

	public Dog(String name, String color, int countEyes)
	{
		super(name, color, countEyes);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void walk()
	{
		System.out.println("Der Hund schlendert");
		
	}

	@Override
	public void makeNoise()
	{
		System.out.println("Der Hund macht wau");
		
	}

}
