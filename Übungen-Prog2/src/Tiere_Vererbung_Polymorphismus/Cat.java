package Tiere_Vererbung_Polymorphismus;

public class Cat extends Animal
{

	public Cat(String name, String color, int countEyes)
	{
		super(name, color, countEyes);
		
	}

	@Override
	public void walk()
	{
		System.out.println("Die Katze schleicht");
		
	}

	@Override
	public void makeNoise()
	{
		System.out.println(name + " macht miau");
		
	}
	public String toString()
	{
		return name;
	}

}
