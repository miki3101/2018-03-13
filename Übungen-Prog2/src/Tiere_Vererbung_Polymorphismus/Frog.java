package Tiere_Vererbung_Polymorphismus;

public class Frog extends Animal
{

	public Frog(String name, String color, int countEyes)
	{
		super(name, color, countEyes);
	}

	@Override
	public void walk()
	{
		System.out.println(name + " h�pft");
	}

	public void makeNoise()
	{
		System.out.println(name + " macht quak");
		
	}
	public String toString()
	{
		return name;
	}

}
