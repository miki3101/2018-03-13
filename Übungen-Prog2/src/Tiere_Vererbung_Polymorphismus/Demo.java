package Tiere_Vererbung_Polymorphismus;

public class Demo
{

	public static void main(String[] args)
	{
		
		Animal a = new Animal("Susi", "grau", 2);
		a.makeNoise();
		System.out.println("-----------\n");
		
		System.out.println("Upcast:\nTier a wird jetzt ein Frosch");
		Animal b = new Frog("Quaxi", "gr�n", 2);
		b.makeNoise();
		System.out.println("----------\n");
		
		System.out.println("Downcast:\nTier a wird jetzt ein Tier");
		b = new Animal("Quaxi", "gr�n", 2);
		b.makeNoise();
		System.out.println("----------");
		
		
		
		Nature tiere = new Nature();
		
		tiere.addAnimal(b);
		tiere.addAnimal(a);
		System.out.println("Tiere mit gr�nen Augen: " +tiere.countColor("gr�n"));
		
		
		
		
		
		
				
		
	
		
		
		

	}

}
