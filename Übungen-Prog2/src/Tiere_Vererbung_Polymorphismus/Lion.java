package Tiere_Vererbung_Polymorphismus;

public class Lion extends Animal
{

	public Lion(String name, String color, int countEyes)
	{
		super(name, color, countEyes);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void walk()
	{
		System.out.println("Der L�we rennt");
		
	}

	@Override
	public void makeNoise()
	{
		System.out.println("Der L�we macht rrrrrrr");
		
	}

}
