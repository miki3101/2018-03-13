package Tiere_Vererbung_Polymorphismus;

public class Animal
{
	protected String name;
	private String color;
	private int countEyes;
	
	public Animal(String name, String color, int countEyes)
	{
		this.name = name;
		this.color = color;
		this.countEyes = countEyes;
	}
	
	
	
	public String getName()
	{
		return name;
	}



	public String getColor()
	{
		return color;
	}



	public int getCountEyes()
	{
		return countEyes;
	}

	public void walk()
	{
		System.out.println("Das Tier geht");
	}
	public void makeNoise()
	{
		System.out.println( name + " macht ein Ger�usch");
	}
	public String toString()
	{
		return name;
	}
	
}
