package Tiere_Vererbung_Polymorphismus;

import java.util.ArrayList;

public class Nature
{
	private ArrayList<Animal> tiere = new ArrayList<>();
	
	
	public void addAnimal(Animal a)
	{
		tiere.add(a);
	}
	
	public int countColor(String color)
	{
		int farbe = 0;
		
		for (Animal animal : tiere)
		{
			if(animal.getColor() == color)
				farbe++;
		}
		
		return farbe;
	}
}
