package Konto_Casting;

public class GiroKonto extends Konto
{
	protected double limit;

	public GiroKonto(String inhaber, double limit)
	{
		super(inhaber);
		this.limit = limit;
	}
	
	public void einzahlen(double wert)
	{
		this.kontostand = kontostand + wert;
		System.out.println("Sie haben € " + wert + " eingezahlt");
		System.out.println("Kontostand von " + inhaber + " beträgt: € " + kontostand);
	
	}
	
	public void auszahlen(double wert)
	{
		if(wert > kontostand + limit)
		{
			System.out.println("Auszahlungsbetrag von € " + wert + " überschreitet Ihr Limit von € " + limit);
			System.out.println("Kontostand: € " + kontostand);
		}
		else
		{
			this.kontostand = kontostand - wert;
			System.out.println("Von ihrem Girokonto wurde " + wert + " abgebucht.");
			System.out.println("Kontostand: € " + kontostand);
		}
	}
}
