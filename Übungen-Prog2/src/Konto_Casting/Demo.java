package Konto_Casting;

public class Demo
{

	public static void main(String[] args)
	{
		Konto k = new GiroKonto("Konto", 500);
		GiroKonto gk = new GiroKonto("Girokonto", 500);
		SparKonto sk = new SparKonto("Sparkonto");
		JugendGiroKonto jgk = new JugendGiroKonto("Jugendgirokonto", 500, 200);
		
		System.out.println(k);
		System.out.println(gk);
		System.out.println(sk);
		System.out.println(jgk);
		System.out.println("---------");
		
		k.einzahlen2(500);
		gk.einzahlen(500);
		sk.einzahlen(500);
		jgk.einzahlen(500);
		System.out.println("-----------");
		
		k.auszahlen2(2000);
		System.out.println("----------");
		gk.auszahlen(1000);
		
		gk.auszahlen(100);
		System.out.println("----------");
		sk.auszahlen(500);
		sk.auszahlen(100);
		System.out.println("-----------");
		jgk.auszahlen(100);
		jgk.auszahlen(500);
		System.out.println("-----------");
		jgk.auszahlen(100);
		jgk.auszahlen(200);
		jgk.auszahlen(200);
		jgk.auszahlen(200);
		jgk.auszahlen(200);
		jgk.auszahlen(200);
		System.out.println("-------------\n");
		
		
		System.out.println("Casting \n");
		System.out.println("----------");
		
		Konto k2 = new JugendGiroKonto("JGK", 500, 100);
		System.out.println("Konto ist ein JGK benutzt aber die Methode vom Konto: \n");
		
		System.out.println(k2);
		k2.einzahlen2(400);
		k2.auszahlen2(200);
		k2.auszahlen2(100);
		k2.auszahlen2(20000);
		k2.einzahlen2(20000);
		System.out.println("-----------\n");
		
		
		System.out.println("Konto ist ein JGK");
		JugendGiroKonto k4 = (JugendGiroKonto)k2;	
		System.out.println(k4);
		k4.einzahlen(800);
		k4.auszahlen3(400);
		
		System.out.println("----------");
		System.out.println("Konto bleibt ein JGK benutzt aber die Methode von GK: \n");
		((GiroKonto) k4).auszahlen(400);
		((GiroKonto) k4).auszahlen(2000);
		GiroKonto k5 = (GiroKonto) k4;
		System.out.println(k5);
		
		
		

	
		
			}

}
