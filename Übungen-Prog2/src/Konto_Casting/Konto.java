package Konto_Casting;

public abstract class Konto
{
	protected String inhaber;
	protected double kontostand;
	
	public Konto(String inhaber)
	{
		this.inhaber = inhaber;
		this.kontostand = 0;
	}
	
	public void einzahlen2(double wert)
	{
		this.kontostand = kontostand + wert;
		System.out.println("Sie haben � " + wert + " eingezahlt");
		System.out.println("Kontostand von " + inhaber + " betr�gt: � " + kontostand);
	}
	
	public void auszahlen2(double wert)
	{
		this.kontostand = kontostand - wert;
		System.out.println("Von ihrem Konto wurde " + wert + " abgebucht.");
		System.out.println("Kontostand von " + inhaber + " betr�gt: � " + kontostand);
	}
	
	public double getKontostand()
	{
		return kontostand;
	}

	public String toString()
	{
		return "Kontoinhaber: " + inhaber + "\t \tKontostand: � " + kontostand;
	}
	
}
