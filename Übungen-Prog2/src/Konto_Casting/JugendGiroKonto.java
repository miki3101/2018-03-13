package Konto_Casting;

public class JugendGiroKonto extends GiroKonto
{
	protected double buchungslimit;

	public JugendGiroKonto(String inhaber, double limit, double buchungslimit)
	{
		super(inhaber, limit);
		this.buchungslimit = buchungslimit;
	}
	
	public void einzahlen3(double wert)
	{
		this.kontostand = kontostand + wert;
		System.out.println("Sie haben € " + wert + " eingezahlt");
		System.out.println("Kontostand von " + inhaber + " beträgt: € " + kontostand);
	}
	
	public void auszahlen3(double wert)
	{
		if(wert > buchungslimit)
		{
			System.out.println(wert + " überschreitet Ihren Auszahlungsbetrag");
			System.out.println("Kontostand: € " + kontostand);
		}
		else if(wert > kontostand + limit)
		{
			System.out.println(wert + " überschreitet Ihr Limit");
		}
		else
		{
			this.kontostand = kontostand - wert;
			System.out.println("Von ihrem Jugendsparkonto wurde " + wert + " abgebucht.");
			System.out.println("Kontostand: € " + kontostand);
		}
	}
}
