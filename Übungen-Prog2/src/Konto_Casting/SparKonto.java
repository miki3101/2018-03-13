package Konto_Casting;

public class SparKonto extends Konto
{

	public SparKonto(String inhaber)
	{
		super(inhaber);
		
	}
	
	public void einzahlen(double wert)
	{
		this.kontostand = kontostand + wert;
		System.out.println("Sie haben � " + wert + " eingezahlt");
		System.out.println("Kontostand von " + inhaber + " betr�gt: � " + kontostand);
	}
	
	public void auszahlen(double wert)
	{
		if(kontostand - wert < 0)
		{
			System.out.println("Auszahlungsbetrag von � " + wert +" ist h�her als Ihr Kontostand, sie k�nnen nicht ins Minus gehen");
		}
		else
		{
			this.kontostand = kontostand - wert;
			System.out.println("Von ihrem Sparkonto wurde " + wert + " abgebucht.");
			System.out.println("Kontostand: � " + kontostand);
		}
	}
}
