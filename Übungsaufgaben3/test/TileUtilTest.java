

import static org.junit.Assert.*;

import java.util.ArrayList;

import javax.swing.plaf.SeparatorUI;

import org.campus02.domino.Tile;
import org.campus02.domino.util.TileUtil;
import org.junit.Before;
import org.junit.Test;

public class TileUtilTest {
	private ArrayList<Tile> tiles = new ArrayList<Tile>();

	@Before
	public void setup() {
		tiles.add(new Tile(3, 4));
		tiles.add(new Tile(1, 1));
		tiles.add(new Tile(4, 4));
		tiles.add(new Tile(5, 1));
		tiles.add(new Tile(6, 4));
		tiles.add(new Tile(3, 3));
		tiles.add(new Tile(1, 2));
	}

	@Test
	public void test01() throws Exception {
		int result = TileUtil.sum(tiles);
		assertEquals(42, result);
	}

	@Test
	public void test02() throws Exception {

		Tile result = TileUtil.greatesTile(tiles);
		assertEquals("(6,4)", result.toString());

	}

}
