package org.campus02.set;

import java.util.ArrayList;

public class Set {
	
	ArrayList<Integer> menge = new ArrayList<>();
	ArrayList<Set> set = new ArrayList<>();
	
	
	public void add(int value) {
		
		menge.add(value);
		
	}

	public void add(Set set) {
		
		this.set.add(set);

	}

	public int sum() {
		
		int summe = 0;
		int summe1 = 0;
		
		for (Integer zahl : menge)
		{
			summe = summe + zahl;
		}
		
		for (Set set : set)
		{
			summe1 = summe1 + set.sum();
		}
		
		return summe;
	}
	
	public String toString()
	{
		if(set.isEmpty())
		{
		return  menge.toString();
		}
		return menge.toString() + set.toString();
	}

}
