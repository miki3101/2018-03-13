package org.campus02.domino;

public class Tile
{

	private int num1;
	private int num2;

	public Tile(int h, int s)
	{
		if (h > s)
		{
			this.num1 = h;
			this.num2 = s;
		} else
		{
			this.num1 = s;
			this.num2 = h;
		}
	}

	public int getNumber1()
	{

		return num1;
	}

	public int getNumber2()
	{

		return num2;
	}

	public boolean compare(Tile otherTile)
	{
		if (otherTile.getNumber1() > num1)
		{
			return true;
		} else if (otherTile.getNumber1() == num1)
			if (otherTile.getNumber2() > num2)
			{
				return true;
			}
			else if (otherTile.getNumber2() > num2)
			{
				return true;
			}

		return false;
	}

	@Override
	public String toString()
	{
		return "(" + num1 + "," + num2 + ")";
	}
}
