package org.campus02.domino.util;

import java.util.ArrayList;

import org.campus02.domino.Tile;

public class TileUtil {

	public static int sum(ArrayList<Tile> tiles) {
		
		int summe = 0;
		
		for (Tile tile : tiles)
		{
			summe = summe + tile.getNumber1() + tile.getNumber2(); 
		}
		
		return summe;
	}

	public static Tile greatesTile(ArrayList<Tile> tiles) {
		
		Tile gross = tiles.get(0);
			
		for (Tile tile : tiles)
		{
			if (tile.getNumber1() > gross.getNumber1())
			{
				gross = tile;
			}
		}
			return gross;
	}
}
