package org.campus02.ue.emp;

public class OrganigramHandler
{

	public static String processHierarchy(Employee emp)
	{

		String result = emp.getName() + "\n";

		for (Employee e : emp.getSubordinates())
		{
			result += processHierarchy(e);
		}

		return result;

	}

	public static String processHierarchy(Employee emp, String indent)
	{
		
		String result = indent + ">" + emp.getName() + "\n";

		for (Employee e : emp.getSubordinates())
		{
			result += processHierarchy(e, indent + "  ");
		}
		
		return result;
	}

	public static double getTotalSalary(Employee emp)
	{
		double result = emp.getSalary();
		
		for (Employee e : emp.getSubordinates())
		{
			result += getTotalSalary(e);
		}
			
		return result;
	}

}
