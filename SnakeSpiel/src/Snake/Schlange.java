package Snake;

public class Schlange extends Spielfeld
{
	protected char snake;
	protected int x;
	protected int y;
	
	
	public Schlange(int x, int y)
	{
		this.snake = 'S';
		brett[x][y] = snake;
		this.x = x;
		this.y = y;
	}
	
	public void left()
	{
		if(brett[x][y-1] == 'x')
		{
			brett [x][y] = snake;
		}
		else {
		brett [x][y] = ' ';
		y -= 1;
		brett [x][y] = snake;
		}
		
	}
	
	public void down()
	{	
		if(brett[x][x+1] == 'x')
		{
			brett [x][y] = snake;
		}
		else {
		brett [x][y] = ' ';
		x += 1;
		brett [x][y] = snake;
		}

	}
	
	public void right()
	{
		if(brett[x][y+1] == 'x')
		{
			brett [x][y] = snake;
		}
		else {
		brett [x][y] = ' ';
		y += 1;
		brett [x][y] = snake;
		}
	}
	
	public void up()
	{
		if(brett[x][x-1] == 'x')
		{
			brett [x][y] = snake;
		}
		else {
		brett [x][y] = ' ';
		x -= 1;
		brett [x][y] = snake;
		}
	}
}
