package Snake;



public class Spielfeld
{
	protected char brett [][] = new char [10][10];
	
	public Spielfeld()
	{
		for(int i = 0; i<brett.length; i++)
		{
			brett[i][9] = 'x';
			brett[9][i] = 'x';
			brett[0][i] = 'x';
			brett[i][0] = 'x';
		}
	}
	
	
	public String toString()
	{
		String out = "";

		for(int i = 0; i < brett.length; i++)
		{
			for (int j = 0; j < brett.length; j++)
			{
				out += brett[i][j];	
			}
			out += "\n";
		}
		return out;
	}
	
}
