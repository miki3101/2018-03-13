package Snake;

import java.util.Scanner;

public class Demo
{

	public static void main(String[] args)
	{
		Spielfeld demo = new Spielfeld();
		Schlange s = new Schlange(5, 5);
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Bitte w�hlen Sie Ihre Richtung: ");
		System.out.println("M�gliche Richtungen: a, s, d, f");
			
		try
		{
		while(true)
		{
		
		String eingabe = scanner.nextLine();
			
			switch (eingabe)
			{
			case "a":
				s.left();
				System.out.println(s);
				break;
			case "d":
				s.right();
				System.out.println(s);
				break;
			case "w":
				s.up();
				System.out.println(s);
				break;
			case "s":
				s.down();
				System.out.println(s);
				break;
			default:
				System.out.println("Falsche Eingabe!!!");
				break;
			}
		}
		}
		
		finally
		{
			scanner.close();
		}
		
	}		
}