package at.campus02.ase.books;

import java.util.ArrayList;

public class Bibliothek {
	
	
	 ArrayList<Book> bibliothek = new ArrayList<Book>();
	
	public void addBook(Book b)
	{
		bibliothek.add(b);
	}

	public int countPages()
	{
		int summe = 0;
		
		for (Book book : bibliothek)
		{
			summe = summe + book.getSeiten();
		} 
		
		return summe;
	}

	public double avaragePages()
	{
		double summe = 0;
		double ergebnis = 0;
		
		for (Book zahl : bibliothek)
		{
			summe = summe + zahl.getSeiten();
			ergebnis = summe /bibliothek.size();
		}
		
		return ergebnis;
	}

	public ArrayList<Book> booksByAuthor(String autor)
	{
		ArrayList<Book> Autor = new ArrayList<>(); 
			
		for (Book book : bibliothek)//gehe jedes Buch in der Bibliothek durch 
		{
			
			if (book.getAutor() == autor) //wenn an der Stelle book der Autor gleich ist wie autor
			{
				Autor.add(book); // dann f�ge der Liste Autor dieses Buch hinzu
			}
		}
			
		return Autor;
	
}

	public ArrayList<Book> findBook(String search)
	{
		ArrayList<Book>  Buch = new ArrayList<>();
		
		for (Book book : bibliothek)
		{
			if (book.match(search))
			{
				Buch.add(book);
			}
		}

		return Buch;
	}

}
