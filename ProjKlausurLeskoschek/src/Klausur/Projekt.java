package Klausur;

public class Projekt
{
	
	private int projektID;
	private String projektbezeichnung;
	private String projekttyp;
	private double budget;
	private int laufzeit;
	
	
	public Projekt(int projektID, String projektbezeichnung, String projekttyp, double budget, int laufzeit)
	{
		this.projektID = projektID;
		this.projektbezeichnung = projektbezeichnung;
		this.projekttyp = projekttyp;
		this.budget = budget;
		this.laufzeit = laufzeit;
	}
	
	public void printInfo()
	{
		System.out.println("Ich bin ein Projekt");
	}
	
	public Projekt(String projektbezeichnung, String projekttyp, double budget, int laufzeit)
	{
		this.projektbezeichnung = projektbezeichnung;
		this.projekttyp = projekttyp;
		this.budget = budget;
		this.laufzeit = laufzeit;
	}
	
	public int getProjektID()
	{
		return projektID;
	}


	public void setProjektID(int projektID)
	{
		this.projektID = projektID;
	}


	public String getProjektbezeichnung()
	{
		return projektbezeichnung;
	}


	public void setProjektbezeichnung(String projektbezeichnung)
	{
		this.projektbezeichnung = projektbezeichnung;
	}


	public String getProjekttyp()
	{
		return projekttyp;
	}


	public void setProjekttyp(String projekttyp)
	{
		this.projekttyp = projekttyp;
	}


	public double getBudget()
	{
		return budget;
	}


	public void setBudget(double budget)
	{
		this.budget = budget;
	}


	public int getLaufzeit()
	{
		return laufzeit;
	}


	public void setLaufzeit(int laufzeit)
	{
		this.laufzeit = laufzeit;
	}

	public String toString()
	{
		return "ProjektID: " + projektID + ", Projektbezeichnung: " + projektbezeichnung + ", Projekttyp: "
				+ projekttyp + ", Budget: " + budget + ", Laufzeit: " + laufzeit + "\n";
	}
	
	
	

	
}
