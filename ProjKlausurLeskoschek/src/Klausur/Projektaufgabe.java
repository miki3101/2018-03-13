package Klausur;

public class Projektaufgabe
{
	
	private int projektaufgabeID;
	private int projektID;
	private String aufgabenbezeichnung;
	private int aufwandInStunden;
	
	
	public Projektaufgabe(int projektaufgabeID, int projektID, String aufgabenbezeichnung, int aufwandInStunden)
	{
		this.projektaufgabeID = projektaufgabeID;
		this.projektID = projektID;
		this.aufgabenbezeichnung = aufgabenbezeichnung;
		this.aufwandInStunden = aufwandInStunden;
	}
	
	public Projektaufgabe(int projektID, String aufgabenbezeichnung, int aufwandInStunden)
	{
		this.projektID = projektID;
		this.aufgabenbezeichnung = aufgabenbezeichnung;
		this.aufwandInStunden = aufwandInStunden;
	}


	public int getProjektaufgabeID()
	{
		return projektaufgabeID;
	}


	public void setProjektaufgabeID(int projektaufgabeID)
	{
		this.projektaufgabeID = projektaufgabeID;
	}


	public int getProjektID()
	{
		return projektID;
	}


	public void setProjektID(int projektID)
	{
		this.projektID = projektID;
	}


	public String getAufgabenbezeichnung()
	{
		return aufgabenbezeichnung;
	}


	public void setAufgabenbezeichnung(String aufgabenbezeichnung)
	{
		this.aufgabenbezeichnung = aufgabenbezeichnung;
	}


	public int getAufwandInStunden()
	{
		return aufwandInStunden;
	}


	public void setAufwandInStunden(int aufwandInStunden)
	{
		this.aufwandInStunden = aufwandInStunden;
	}

	public String toString()
	{
		return "ProjektaufgabeID: " + projektaufgabeID + ", ProjektID: " + projektID
				+ ", Aufgabenbezeichnung: " + aufgabenbezeichnung + ", Aufwand in Stunden: " + aufwandInStunden + "\n";
	}
	
	
}
