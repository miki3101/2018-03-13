package Klausur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBHelper
{
	private Connection con = null;

	public void init()
	{
		try
		{
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e)
		{

			e.printStackTrace();
		}

		try
		{

			con = DriverManager.getConnection("jdbc:ucanaccess://"
					+ "C://Users//michaela.leskoschek//Documents//Nowa//Datenbanken Programmieren/Projektverwaltung.accdb");

			System.out.println("Sie sind mit der Datenbank verbunden \n");

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void close()
	{
		if (con != null)
			try
			{
				con.close();
				System.out.println("Verbindung zur Datenbank wurde getrennt");

			} catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}

	public Projekt getProjekt(int projektID)
	{
		Projekt x = null;

		try
		{
			String query = "SELECT projektid, projektbezeichnung, projekttyp, budget, laufzeit FROM Klausurprojekte WHERE projektid=?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, projektID);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				x = new Projekt(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getInt(5));
				x.setProjektID(rs.getInt(1));	// ID setzen
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return x;
	}

	public List<Projekt> getAlleProjekteSortedByBudgetDesc()
	{
		List<Projekt> liste = new ArrayList<>();
		Projekt p = null;

		try
		{
			String query = "SELECT projektid, projektbezeichnung, projekttyp, budget, laufzeit FROM Klausurprojekte order by budget desc";
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				p = new Projekt(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getInt(5));
				liste.add(p);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return liste;
	}

	public List<Projektaufgabe> getAlleProjektaufgaben()
	{
		List<Projektaufgabe> liste = new ArrayList<>();
		Projektaufgabe p = null;

		try
		{
			String query = "SELECT projektaufgabeid, projektid, aufgabenbezeichnung, aufwandinstunden FROM Klausurprojektaufgaben";
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				p = new Projektaufgabe(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4));
				liste.add(p);
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return liste;
	}

	public int insertProjekt(Projekt neuesProjekt)
	{
		int neueProjektnr = 0;
		try
		{
			String update = "Insert into klausurprojekte (projektbezeichnung, projekttyp, budget, laufzeit) values (?, ?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setObject(1, neuesProjekt.getProjektbezeichnung());
			stmt.setObject(2, neuesProjekt.getProjekttyp());
			stmt.setObject(3, neuesProjekt.getBudget());
			stmt.setObject(4, neuesProjekt.getLaufzeit());
			stmt.executeUpdate();

			String query = "Select @@identity";
			PreparedStatement stmt2 = con.prepareStatement(query);
			ResultSet rs = stmt2.executeQuery();

			while (rs.next())
			{
				neueProjektnr = rs.getInt(1);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return neueProjektnr;
	}

	public void updateProjekt(Projekt projekt)
	{
		try
		{
			String update = "Update klausurprojekte set projektbezeichnung=?, projekttyp=?, budget=?, laufzeit=? where projektid=?";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setString(1, projekt.getProjektbezeichnung());
			stmt.setString(2, projekt.getProjekttyp());
			stmt.setDouble(3, projekt.getBudget());
			stmt.setInt(4, projekt.getLaufzeit());
			stmt.setInt(5, projekt.getProjektID());
			stmt.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void deleteProjekt(Projekt projekt)
	{
		try
		{
			String delete = "Delete from klausurprojektaufgaben where projektid =?";
			PreparedStatement stmt = con.prepareStatement(delete);
			stmt.setInt(1, projekt.getProjektID());
			stmt.executeUpdate();

			String delete2 = "Delete from klausurprojekte where projektid =?";
			PreparedStatement stmt2 = con.prepareStatement(delete2);
			stmt2.setInt(1, projekt.getProjektID());
			stmt2.executeUpdate();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public Projektaufgabe insertProjektaufgabe(Projektaufgabe aufgabe, int projektID)
	{
		try
		{
			String update = "Insert into klausurprojektaufgaben (projektid, aufgabenbezeichnung, aufwandinstunden) values (?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setObject(1, projektID);
			stmt.setObject(2, aufgabe.getAufgabenbezeichnung());
			stmt.setObject(3, aufgabe.getAufwandInStunden());
			stmt.executeUpdate();

			String query = "Select @@identity";
			PreparedStatement stmt2 = con.prepareStatement(query);
			ResultSet rs = stmt2.executeQuery();

			while (rs.next())
			{
				aufgabe.setProjektaufgabeID(rs.getInt(1));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return aufgabe;
	}

	public List<Projektaufgabe> getProjektaufgabenZuProjekt(int projektID)
	{
		ArrayList<Projektaufgabe> aufgaben = new ArrayList<>();
		Projektaufgabe p = null;

		try
		{
			String query = "SELECT projektaufgabeid, projektid, aufgabenbezeichnung, AufwandInStunden FROM klausurprojektaufgaben where projektid = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, projektID);
			ResultSet rs = stmt.executeQuery();

			while (rs.next())
			{
				p = new Projektaufgabe(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4));
				aufgaben.add(p);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return aufgaben;
	}

	public Projekt getProjektMitLaengsterLaufzeit()
	{
		ArrayList<Projekt> projekte = (ArrayList<Projekt>) getAlleProjekteSortedByBudgetDesc();

		Projekt x = projekte.get(0);

		for (Projekt projekt : projekte)
		{
			if (projekt.getLaufzeit() > x.getLaufzeit())
				x = projekt;
		}

		return x;
	}

	public void loescheAlleProjektaufgabenUndDanachDasProjekt(Projekt projekt)
	{
		try
		{
			con.setAutoCommit(false);

			String delete = "Delete from klausurprojektaufgaben where projektid =?";
			PreparedStatement stmt = con.prepareStatement(delete);
			stmt.setInt(1, projekt.getProjektID());
			stmt.executeUpdate();

			String delete2 = "Delete from klausurprojekte where projektid =?";
			PreparedStatement stmt2 = con.prepareStatement(delete2);
			stmt2.setInt(1, projekt.getProjektID());
			stmt2.executeUpdate();

			con.commit();

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void updateProjektlaufzeiten()
	{
		ArrayList<Projekt> p = (ArrayList<Projekt>) getAlleProjekteSortedByBudgetDesc();
		
		int lz = 5;
		int lz2 = 10;
		
		for (Projekt projekt : p)
		{
			if(projekt.getBudget() < 1000000)
			{
				projekt.setLaufzeit(lz);
			}
			else
			{
				projekt.setLaufzeit(lz2);
			}
		}
		
		try
		{
			con.setAutoCommit(false);

			String update = "update klausurprojekte set laufzeit = ? where budget < 1000000";
			String update2 = "update klausurprojekte set laufzeit = ? where budget > 1000000";
			
			PreparedStatement stmt = con.prepareStatement(update);
			stmt.setInt(1, lz);
			stmt.executeUpdate();

			PreparedStatement stmt2 = con.prepareStatement(update2);
			stmt2.setInt(1, lz2);
			stmt2.executeUpdate();
			
			con.commit();

						
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
