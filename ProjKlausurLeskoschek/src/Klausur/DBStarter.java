package Klausur;

public class DBStarter
{
	public static void main(String[] args)
	{
		
		DBHelper helper = new DBHelper();
		Projekt p = new Projekt("Michaela", "Leskoschek", 1740004007, 3);
		Projektaufgabe a = new Projektaufgabe(1740004007, "Michaela Leskoschek", 40);
		
		System.out.println("Aufgabe 4:\n");
		helper.init();
		System.out.println("------------");
		
		System.out.println("Aufgabe 5a:\n");
		System.out.println("Projetk mit der ID 1:\n" + helper.getProjekt(1));
		System.out.println("-------------");
		
		System.out.println("Aufgabe 5b:\n");
		System.out.println("Alle Projekte sortiert:\n" + helper.getAlleProjekteSortedByBudgetDesc());
		System.out.println("-------------");
		
//		System.out.println("Aufgabe 6a:\n");
//		System.out.println("Neues Projekt mit der Nr:\n" + helper.insertProjekt(p));
//		System.out.println("-------------");
		
		System.out.println("Aufgabe 6b:\n");
		p.setProjektID(4);
		System.out.println("Projekt mit der ID 4 vor �nderung:\n" + helper.getProjekt(4));
		p.setLaufzeit(7);
		System.out.println("�ndern der Laufzeit des Projektes mit der ID 4:\n");
		helper.updateProjekt(p);
		System.out.println("Projekt mit der ID 4:\n" + helper.getProjekt(4));
		System.out.println("-------------");
		
		System.out.println("Aufgabe 6c:\n");
		System.out.println("Projekte vor dem L�schen:\n" + helper.getAlleProjekteSortedByBudgetDesc());
		helper.deleteProjekt(p);
		System.out.println("Projekte nach dem L�schen:\n" + helper.getAlleProjekteSortedByBudgetDesc());
		System.out.println("-------------");
		
//		System.out.println("Aufgabe 7a:\n");
//		System.out.println("Alle Projektaufgaben:\n" + helper.getAlleProjektaufgaben());
//		System.out.println("Neue Projektaufgabe:\n " + helper.insertProjektaufgabe(a, 2));
//		System.out.println("Alle Projektaufgaben:\n" + helper.getAlleProjektaufgaben());
//		System.out.println("-------------");
		
		System.out.println("Aufgabe 8a:\n");
		System.out.println("Projektaufgaben zu Projekten:\n" + helper.getProjektaufgabenZuProjekt(2));
		System.out.println("-------------");
		
		
		System.out.println("Aufgabe 8b:\n");
		System.out.println("Alle Projektaufgaben:\n" + helper.getAlleProjektaufgaben());
		System.out.println("-------------");
		
		System.out.println("Aufgabe 9:\n");
		System.out.println("Alle Projekte:\n" + helper.getAlleProjekteSortedByBudgetDesc());
		System.out.println("Projekt mit der l�ngsten Laufzeit:\n" + helper.getProjektMitLaengsterLaufzeit());
		System.out.println("------------");
		
		System.out.println("Aufgabe 10:\n");
		System.out.println("Alle Projekte vor dem L�schen mit Aufgaben:\n" + helper.getProjektaufgabenZuProjekt(3));
//		helper.loescheAlleProjektaufgabenUndDanachDasProjekt(p);
		System.out.println("Alle Projekte nach dem L�schen mit Aufgaben:\n" + helper.getProjektaufgabenZuProjekt(3));
		System.out.println("-------------");
		
		System.out.println("Aufgabe 11:\n");
		System.out.println("Alle Projekte vor dem �ndern der Laufzeit:\n" + helper.getAlleProjekteSortedByBudgetDesc());
		helper.updateProjektlaufzeiten();
		System.out.println("Alle Projekte nach dem �ndern der Laufzeit:\n" + helper.getAlleProjekteSortedByBudgetDesc());
		System.out.println("-------------");
	
		System.out.println("Aufgabe 12:\n");
		helper.close();

	}

}
