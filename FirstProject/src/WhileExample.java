
public class WhileExample
{

	public static void main(String[] args)
	{
		double einlage = 2000;
		double zinssatz = 1.025;
		double zielbetrag = 3000;
		double kontostand = einlage;
		
		int jahre = 0; //fangen bei 0 Jahren an

		
		while (kontostand < zielbetrag) // Schleife solange, bis Kontostand den Zielbetrag erreicht
		{
			kontostand = kontostand * zinssatz;
			
			jahre++; //z�hlt nach jedem Durchlauf der Schleife ein Jahr dazu bis der Zielbetrag erf�llt ist
			
			System.out.printf("%d. Jahr: %.2f \n", jahre, kontostand); // "%d. Jahr:" gibt mir die Zahl zum Jahr dazu
																// ("%.2f",) nur 2 Kommastellen, (\n) Zeilenabstand 
		}
		
		System.out.println("So viele Jahre dauert es: " +jahre);


	}

}
