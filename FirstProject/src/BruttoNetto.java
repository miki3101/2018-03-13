
public class BruttoNetto
{
	public static void main(String args[])
	{
		double netto =100;
		double steuer = 0.2;
		double brutto = netto *(1+ steuer/100);
		
		System.out.println(brutto);
		System.out.printf("      netto: %.2f \n", netto); // \n Zeilenumbruch
		System.out.printf("     steuer: %.2f \n", steuer); // \n Zeilenumbruch
		System.out.printf("Bruttopreis: %.2f \n", brutto); // \n Zeilenumbruch

		int a = 4; // ist Zuweisung von a

		if (a == 2) // if braucht immer einen boolean, == vergleichen
		{
			System.out.println("a ist gleich 2");
		} else if (a == 3)
		{
			System.out.println("a ist gleich 3");
		} else
		{
			System.out.println("a ist nicht gleich 2 und nicht 3");
		}

	}

}
