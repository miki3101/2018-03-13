
public class TypKonvertierung
{

	public static void main(String[] args)
	{
		int intWert = 10;
		double doubleWert = 20.0;
		
		doubleWert = intWert; //int auf double zuweisen geht
		
		intWert = (int) doubleWert;
		
		String zeichenkette = "10";
		
		intWert = Integer.valueOf(zeichenkette); //Methode "Integer.valueOf" mit Parameter "zeichenkette";
												// liefert den Wert einer Zeichenkette in einen Zahlenwert zur�ck
		
		doubleWert = Double.valueOf(zeichenkette); //liefert den Wert einer Zeichenkette in einen Zahlenwert mit
													// mit Dezimalstellen zur�ck
	}

}
