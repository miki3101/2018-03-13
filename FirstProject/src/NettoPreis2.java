
public class NettoPreis2
{
	public static void main(String[] args)
	{
		double netto = berechneNettoPreis(100, 2);

		berechneNettoPreis(100, 1);

		System.out.println(netto);
		
		double brutto = BruttoNettoKategorie2.brutto(100, 1); // Methode aus einer anderen Klasse
		System.out.println(brutto);
		
	}

	// methode aus einem bruttoPreis einen netto Preis berechnet

	public static double berechneNettoPreis(double bruttoPreis, int steuerkategorie)
	{
		System.out.println(bruttoPreis);
		System.out.println(steuerkategorie);

		double result; // variable deklarieren

		switch (steuerkategorie)
		{
		case 1:
			result = bruttoPreis / 1.2;
			break;
		case 2:
			result = bruttoPreis / 1.12;
			break;
		case 3:
			result = bruttoPreis / 1.1;
			break;
		default:
			result = bruttoPreis;
		}

		System.out.println(bruttoPreis + "" +steuerkategorie + " " + result);

		return result;

	}
}
