import java.util.HashSet;

public class Main
{

	public static void main(String[] args)
	{
		
		HashSet<Buch> buchregal = new HashSet<>();
		
		buchregal.add(new Buch("I61111", "Herzmuschelsommer", 123));
		buchregal.add(new Buch("I11112", "Selection 5", 500));
		buchregal.add(new Buch("I61111", "Herzmuschelsommer", 123));
		buchregal.add(new Buch("I14123", "Shining", 23));
		buchregal.add(new Buch("I13156", "Verblendung", 155));
		buchregal.add(new Buch("I12345", "Die Nadel", 250));
		buchregal.add(new Buch("I11112", "Selection 5", 500));
		
		System.out.println(buchregal);

	}

}
