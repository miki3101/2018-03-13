
public class Buch
{
	
	private String isbn;
	private String name;
	private int seitenZahl;
	
	
	public Buch(String isbn, String name, int seitenZahl)
	{
		this.isbn = isbn;
		this.name = name;
		this.seitenZahl = seitenZahl;
	}


	public String getIsbn()
	{
		return isbn;
	}


	public void setIsbn(String isbn)
	{
		this.isbn = isbn;
	}


	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public int getSeitenZahl()
	{
		return seitenZahl;
	}


	public void setSeitenZahl(int seitenZahl)
	{
		this.seitenZahl = seitenZahl;
	}


	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Buch other = (Buch) obj;
		if (isbn == null)
		{
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	@Override
	public String toString()
	{
		return "ISBN: " + isbn + ", Name: " + name + ", SeitenZahl: " + seitenZahl + "\n";
	}
	
	
}
