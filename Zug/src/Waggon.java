
public class Waggon
{
	private String ladung; //hat eine unveränderbare Ladung
	private Waggon nachbar; // hat einen unveränderbaren Nachbarn

	// seine ladung kann ich nicht verändern, deshalb definiere ich inhalt den ich ändern kann
	public Waggon(String inhalt)
	{
		ladung = inhalt;
	}

	public void einhaengen(Waggon neuerNachbar)
	{
		nachbar = neuerNachbar; // so verbinde ich den vorigen mit dem nächsten Nachbar
	}
	// damit ich ausgeben kann, was da drinnen ist
	public String toString() // beim Debuggen übersichtlicher, Vererbung //
	{
		return ladung; //gibt die ladung bzw. den inhalt (der veränderbar ist)aus
	}
	//ich kann fragen, wer dein Nachbar ist
	
	public Waggon nachbar ()
	{
		return nachbar; // gibt mir zurück wer der Nachbar ist
	}
}
