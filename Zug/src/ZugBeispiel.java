
public class ZugBeispiel
{

	public static void main(String[] args)
	{
		Zug meineEisenbahn = new Zug(); // ladung ist Mimi, spielt BAll
		
		meineEisenbahn.neuerWaggon("Mimi");	// ist erster und letzter
		meineEisenbahn.neuerWaggon("spielt"); // erster ist mimi letzter ist spielt
		meineEisenbahn.neuerWaggon("Ball"); // erster ist mimi, zweiter ist spielt, dritter ball
		meineEisenbahn.neuerWaggon("!");
		
		System.out.println(meineEisenbahn);
		System.out.println(meineEisenbahn.length());
		System.out.println(meineEisenbahn.waggonInhaltAnStelle(1));
		
	}
}
