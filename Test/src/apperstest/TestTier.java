package apperstest;

import com.fasterxml.jackson.annotation.JsonView;


public class TestTier
{
	public TestTier(String name, String art, int beine)
	{
		this.name = name;
		this.art = art;
		this.beine = beine;
	}
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getArt()
	{
		return art;
	}

	public void setArt(String art)
	{
		this.art = art;
	}

	public int getBeine()
	{
		return beine;
	}

	public void setBeine(int beine)
	{
		this.beine = beine;
	}
	
	@JsonView(Views.Normal.class)
	private String name;
	
	@JsonView(Views.Normal.class)
	private String art;
	
	@JsonView(Views.Zoo.class)
	private int beine;
	
	
	@Override
	public String toString()
	{
		return "TestTier [art=" + art + ", beine=" + beine + ", name=" + name + "]";
	}
	
	
	
	
	
}
