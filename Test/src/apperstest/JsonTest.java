package apperstest;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTest
{
	public static void main(String[] args)
	{
		JsonTest obj = new JsonTest();

		obj.run();
		obj.run2();
	}

	private void run()
	{
		TestTier a = new TestTier("Bello", "Hund", 4);

		ObjectMapper mapper = new ObjectMapper();
		
		try
		{
			String json = mapper.writeValueAsString(a);
			System.out.println("Json als String: \n" + json);
			System.out.println();
			
			String json2 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(a);
			System.out.println("Json als Json: \n" + json2);
			System.out.println();

			
			}catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public void run2()
	{
		TestTier b = new TestTier("Mauzi", "Katze", 4);
		
		ObjectMapper mapper2 = new ObjectMapper();
		
		try
		{
			String jsonview = mapper2.writerWithView(Views.Normal.class).writeValueAsString(b);
			String jsonview2 = mapper2.writerWithView(Views.Zoo.class).writeValueAsString(b);
			
			System.out.println("JsonView in Normalansicht: \n" + jsonview);
			System.out.println();
			System.out.println("JsonView in Zooansicht: \n" + jsonview2);
			System.out.println();
			
			
		} catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		
		
	}
}
