package at.campus02.nowa.prg3.klausur.networkio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Server
{
	public static void main(String[] args)
	{
		try (ServerSocket server = new ServerSocket(4000);)
		{
			while (true)
			{

				Socket socket = server.accept();

				System.out.println(socket + " accepted");

				Thread t = new Thread(new ClientRunnable(socket, new Random().nextInt(10)));
				t.start();
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
