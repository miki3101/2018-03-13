package at.campus02.nowa.prg3.klausur.networkio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientRunnable implements Runnable
{

	private Socket socket;
	private int rounds;

	public ClientRunnable(Socket socket, int rounds)
	{
		this.socket = socket;
		this.rounds = rounds;
	}

	public void run()
	{

		try (
				BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				PrintWriter pw = new PrintWriter(socket.getOutputStream());)
		{

			int round = this.getRounds();
			System.out.println("Rounds: " + round);

			for(int i = 0; i<= rounds; i++)
			{
				String line = br.readLine();
				if (i == rounds)
				{						// nicht write !!!!
					pw.println("stop"); // nicht das println vergessen
				} else	
				{
					pw.println("pong");
				}
				pw.flush();
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public int getRounds()
	{
		return rounds;
	}

}
