package KartenTest;


public class Demo
{
	public static void main(String[] args)
	{
		Spielablauf test = new Spielablauf();
		
		Spieler sp1 = new Spieler("Miki");
		Spieler sp2 = new Spieler("Alex");
		
		test.austeilen();
		test.zeigeKartenstapel();
		
		sp2.getKarten();
		
		Zahlenkarte karte2 = new Zahlenkarte(Farbe.blau, "8");
			
		sp2.aufnehmen(karte2);
		
		sp2.getKarten();
		
	

	}

}
