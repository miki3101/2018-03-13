package KartenTest;

import java.util.ArrayList;
import java.util.Collections;

public class Spielablauf
{

	private AblegeStapel as; // verwaltet den Ablagestapel
	private Kartenstapel ks; // verwaltet den Kartenstapel
	private ArrayList<Spieler> mitspieler = new ArrayList<>(); // verwaltet die Spieler

	public Spielablauf()
	{
		this.as = new AblegeStapel();
		this.ks = new Kartenstapel();
		ks.bef�llen();
		ks.mischen();
	}
	
	public void zeigeKartenstapel()
	{
		ks.zeigeStapel();
	}
	
	public Karten abheben() 
	{
		return ks.karteAbheben();
	}

	public void karteAblegen(Karten k)
	{
		as.karteAblegen(k); // Karte an der Stelle 0 einf�gen
	}

	public void mitspielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler); // mit dieser Methode f�gt man einen neuen Spieler, der Liste Mitspieler hinzu
	}

	public void austeilen()
	{
		for (int index = 0; index < 7; index++) // jeder Spieler zieht 7 mal eine Karte
		{
			for (Spieler sp : mitspieler) // jeder Spieler kommt einmal dran
			{	
										// ich sage das der Spieler eine Karte abhebt
				sp.aufnehmen(abheben()); // hab ich in der Klasse Spieler definiert / Spieler nimmt karte in die Hand
			}
		}
		Karten temp = abheben(); // ich hebe noch eine Karte ab
		as.karteAblegen(temp);// ich lege eine Karte auf den Ablagestapel

		while (temp instanceof Aktionskarten)
		{
			temp = abheben();
			as.karteAblegen(temp);
		}
	}
}
