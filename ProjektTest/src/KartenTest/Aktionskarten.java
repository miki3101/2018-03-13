package KartenTest;

public class Aktionskarten extends Karten
{
	

	public Aktionskarten(String aktion, String wert)
	{
		super(aktion, wert);
	}
	
	public Aktionskarten(Farbe farbe, String wert)
	{
		super(farbe, wert);
	}

	public Aktionskarten(Farbe farbe)
	{
		super(farbe);
	}
	
	public Aktionskarten()
	{
		
	}

	String getWert()
	{
		return wert;
	}

	Farbe getFarbe()
	{
		return farbe;
	}
	
	String getAktion()
	{
		return aktion;
	}

}
