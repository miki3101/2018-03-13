package KartenTest;


abstract class Karten
{
	public Farbe farbe;
	public String wert;
	public String aktion;
	
	public Karten(Farbe farbe, String wert)
	{
		this.farbe = farbe;
		this.wert = wert;
	}
	
	public Karten(String aktion, String wert)
	{
		this.aktion = aktion;
		this.wert = wert;
	}
	
	public Karten()
	{
		
	}
	public Karten(Farbe farbe)
	{
		this.farbe = farbe;
	}
	
	
	abstract String getWert();
	abstract Farbe getFarbe();
	
	public void setFarbe(Farbe farbe)
	{
		this.farbe = farbe;
	}
	public void setWert()
	{
		this.wert = wert;
	}
}
