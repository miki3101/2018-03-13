package KartenTest;

import java.util.ArrayList;

public class Handkarten extends Karten
{
	public ArrayList<Karten> hk;

	
	public Handkarten()
	{
		super();
		this.hk = new ArrayList<>();
	}

	public void zeigKarte()
	{
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print(" Karte " + i + "    ");
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("xxxxxxxxxx  ");
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("x        x  ");
		}
		System.out.print("\n");
		
		for(Karten karte : hk)
		{
			if(karte.getFarbe() == farbe.rot)
			{
				System.out.print("x  " + karte.getFarbe() + "   x  ");
			}
			else if(karte.getFarbe() == farbe.hebe)
			{
				System.out.print("x  " + karte.getFarbe() + "  x  ");
			}
			else if(karte.getFarbe() == farbe.Farben)
			{
				System.out.print("x " + karte.getFarbe() + " x  ");
			}
			else
			{
			System.out.print("x  " + karte.getFarbe() + "  x  ");
			}
		}
		System.out.print("\n");
		
		for(Karten karte : hk)
		{
			if(karte instanceof Plus2 || karte instanceof Plus4)
			{
				System.out.print("x   " + karte.getWert() + "   x  ");
			}
			else if(karte instanceof Richtungswechsel)
			{
				System.out.print("x   " + karte.getWert() + "  x  ");
			}
			else if(karte instanceof Aussetzen)
			{
				System.out.print("x  " + karte.getWert() + " x  ");
			}
			else if(karte instanceof Farbwechsel)
			{
				System.out.print("x " + karte.getWert() + "x  ");
			}
			else
			{
			System.out.print("x   " + karte.getWert() + "    x  ");
			}
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("x        x  ");
		}
		System.out.print("\n");
		
		for (int i = 0; i < hk.size(); i++)
		{
			System.out.print("xxxxxxxxxx  ");
		}
		System.out.print("\n");
	}

	public void addKarten(Karten karte)
	{
		hk.add(karte);
	}

	public String getWert()
	{	
		return wert;
	}

	public Farbe getFarbe()
	{
		return farbe;
	}
	
	public void removeKarte(int index)
	{
		hk.remove(index);
	}

	

}