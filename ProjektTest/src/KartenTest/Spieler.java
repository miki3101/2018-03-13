package KartenTest;


public class Spieler
{
	private String name;
	private Handkarten karten;
	

	public Spieler(String name)
	{
		this.setName(name);
		this.karten = new Handkarten();
	}
	
	public void aufnehmen(Karten karte)
	{
		karten.addKarten(karte);
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public void getKarten()
	{
		System.out.println("Karten von Spieler: " + name);
		karten.zeigKarte();
	}
	
	public String toString()
	{
		return name;
	}
	


}
