package KartenTest;

public class Zahlenkarte extends Karten
{
	
	public Zahlenkarte(Farbe farbe, String wert)
	{
		super(farbe, wert);
	}

	String getWert()
	{
		return wert;
	}

	Farbe getFarbe()
	{
		return farbe;
	}

}
