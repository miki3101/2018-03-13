package at.campus02.prg3.streams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class StreamApp {

	public static void main(String[] args) throws IOException {

		String text = "We are counting the bytes of this text being written into a file.";

		File file = new File("output.txt");

		if (!file.exists()) {
			file.createNewFile();
		}

		ByteCountOutputStream bcos = new ByteCountOutputStream(new FileOutputStream(file));

		bcos.write(text.getBytes());
		bcos.flush();

		System.out.println("Bytes in text: " + text.getBytes().length);
		System.out.println("Bytes written: " + bcos.getCounter());

		bcos.close();

	}

}
