package at.campus02.prg3.streams;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ByteCountOutputStream extends FilterOutputStream {

	private int counter = 0;

	public ByteCountOutputStream(OutputStream out) {
		super(out);
	}

	@Override
	public void write(int b) throws IOException {
		counter++;
		super.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		counter += b.length;
		super.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		counter += len;
		super.write(b, off, len);
	}

	public int getCounter() {
		return counter;
	}

}
