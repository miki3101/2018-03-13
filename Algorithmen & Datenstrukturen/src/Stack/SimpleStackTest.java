package Stack;

public class SimpleStackTest
{

	public static void main(String[] args)
	{

		SimpleStack stack = new SimpleStack();
		stack.push("Value 1");
		stack.push("Value 2");
		stack.push("Value 3");
		stack.push("Value 4");

		while (!stack.isEmpty())
		{
			System.out.println(stack.pop());
		}

		// stack is already empty
		String element = stack.pop();
		System.out.println(element);
	}
}
