package Stack;

import java.util.ArrayList;

public class SimpleStack {

	private ArrayList<String> values;

	public SimpleStack()
	{
		values = new ArrayList<String>();
	}

	public void push(String value)
	{

		values.add(value);
	}

	public String pop()
	{

		if (values.isEmpty())
			return "IS EMPTY";

		int lastIndex = values.size() - 1;
		return values.remove(lastIndex);
	}

	public boolean isEmpty()
	{
		return values.isEmpty();
	}
}
