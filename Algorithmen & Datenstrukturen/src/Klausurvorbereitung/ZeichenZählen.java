package Klausurvorbereitung;

import java.util.HashMap;
import java.util.Map.Entry;

public class ZeichenZ�hlen
{

	public static void main(String[] args)
	{
		String text = "the quick brown fox jumps over the lazy dog";
		
		HashMap<Character, Integer> map = new HashMap<>();
		
		char [] x = text.toCharArray();
		
		for (char c : x)
		{
			if (!map.containsKey(c)) // wenn das Wort noch nicht drinnen ist
			{
				map.put(c, 1);		// gibt es als Key mit dem Value 1 rein
			} else
			{
				int i = map.get(c); // wenn es schon drinnen ist hol dir den Key
				i++; 					// erh�he den Counter um eins
				map.put(c, i);
			}
		}
		
		for (Entry<Character, Integer> eintrag : map.entrySet()) // iteriere durch die HashMap
		{
			System.out.print(eintrag.getKey() + ": "); // gibt die Keys aus
			System.out.println(eintrag.getValue() + "x"); // gibt die Values aus
		}
		
	}

}
