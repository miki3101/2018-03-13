package Klausurvorbereitung;

import java.util.HashSet;

public class HashSetÜbung
{

	public static void main(String[] args)
	{
		HashSet<Student> students = new HashSet<Student>();
		
		students.add(new Student(1, "Nina Guteres"));
		students.add(new Student(2, "Dina Tester"));
		students.add(new Student(3, "Heimo Pfeifer"));
		students.add(new Student(1, "Anton Topf"));
		
		
		for (Student student : students)
		{
			System.out.println(student);
		}
		

	}

}
