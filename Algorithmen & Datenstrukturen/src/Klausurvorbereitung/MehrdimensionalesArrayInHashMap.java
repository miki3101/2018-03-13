package Klausurvorbereitung;

import java.util.HashMap;

public class MehrdimensionalesArrayInHashMap
{

	public static void main(String[] args)
	{

		String[][] values = new String[][]
		{
				{ "rot", "#FF0000" },
				{ "gr�n", "#00FF00" },
				{ "blau", "#0000FF" } };

				
		HashMap<String, String> map = new HashMap<>();

		for(int i = 0; i < values.length; i++)
		{
			String [] row = values [i];		// gibt ein eindimensionales String Array zur�ck
			
			for(int j = 0; j < row.length; j++)
			{
				System.out.println(row [j]); // gibt die Zeilen aus
				
				if(row.length == 2)
				{
					map.put(row[0], row [1]); // erste Spalte Key, zweite Spalte value
				}
			}
			
		}
		
		System.out.println("----------");
		
		for(String key : map.keySet())		// Ausgabe der HashMap mit key und value
		{
			String value = map.get(key);
			System.out.println(key + ":\t" + value);
		}
	}

}
