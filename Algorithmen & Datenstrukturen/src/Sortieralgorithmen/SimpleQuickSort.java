package Sortieralgorithmen;

import java.util.ArrayList;
import java.util.Collections;

public class SimpleQuickSort
{

	public static void main(String[] args)
	{

		ArrayList<Integer> values = new ArrayList<>();

		for(int i = 0; i < 10000; i++)
		{
			values.add(i);
		}
		
		Collections.shuffle(values);
		long startTime = System.nanoTime();
	
		
		ArrayList<Integer> sortedValues = quickSort(values);
		
		long endTime = System.nanoTime();
		long diff = endTime - startTime;
		
		System.out.println("sorted in " + diff / 1000000);
		
//		values.add(2);
//		values.add(7);
//		values.add(9);
//		values.add(1);
//		values.add(4);
//		values.add(3);
//		values.add(4);

//		int pivot = values.get(values.size() - 1);
//		System.out.println("Pivot: " + pivot);
//
//		for (int value : values)
//		{
//			System.out.print(value + ", ");
//		}
//
//		System.out.println();
//		System.out.println("-----------");
//
//		ArrayList<Integer> sortedValues = quickSort(values);
//		System.out.println();
//		System.out.println("SortedValues: ");
//
//		for (int value : sortedValues)
//		{
//			System.out.print(value + ", ");
//		}
	}

	private static ArrayList<Integer> quickSort(ArrayList<Integer> values)
	{
		// Abbruch Bedingung
		if (values.size() <= 1) // wenn ich nur mehr 1 Zahl drinnen habe
		{
			return values;
		}

		int pivot = values.get(values.size() - 1);
//		System.out.println("Pivot: " + pivot);

//		System.out.println("--------------");

		ArrayList<Integer> rightValues = new ArrayList<>();
		ArrayList<Integer> leftValues = new ArrayList<>();

		for (int i = 0; i < values.size() - 1; i++)
		{
			if (values.get(i) < pivot)
			{
				leftValues.add(values.get(i));
//				System.out.println("LeftValues: " + leftValues);

			} else
			{
				rightValues.add(values.get(i));
//				System.out.println("RightValues: " + rightValues);
			}
		}

		// rekursiv linken und rechten Teilbaum splitten
		ArrayList<Integer> sortedLeftValues = quickSort(leftValues);
		ArrayList<Integer> sortedRightValues = quickSort(rightValues);

		// Links und rechts zusammenfügen
		ArrayList<Integer> sortedValues = new ArrayList<>();

		sortedValues.addAll(sortedLeftValues);
		sortedValues.add(pivot);
		sortedValues.addAll(sortedRightValues);

		return sortedValues;
	}

}
