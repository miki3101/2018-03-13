package Sortieralgorithmen;

import java.util.ArrayList;

public class SimpleComparable
{

	public static void main(String[] args)
	{
		ArrayList<Customer> values = new ArrayList<>();
		
		values.add(new Customer(2, "Hugo"));
		values.add(new Customer(7, "Emeli"));
		values.add(new Customer(9, "Sandra"));
		values.add(new Customer(1, "Max"));
		values.add(new Customer(4, "Moritz"));
		values.add(new Customer(3, "Heideltraud"));
		
		for (Customer customer : values)
		{
			System.out.println("Customer: " + customer);
		}
		
		ArrayList<Customer> sortedValues = quickSort(values);
		
		System.out.println("------------");
		System.out.println("SortedValues: ");
		
		
		for (Customer customer : sortedValues)
		{
			System.out.println("Customer: " + customer);
		}


	}

	
	private static ArrayList<Customer> quickSort(ArrayList<Customer> values)
	{
		// Abbruch Bedingung
		if (values.size() <= 1) // wenn ich nur mehr 1 Zahl drinnen habe
		{
			return values;
		}

		Customer pivot = values.get(values.size() - 1);
	
		ArrayList<Customer> rightValues = new ArrayList<>();
		ArrayList<Customer> leftValues = new ArrayList<>();

		for (int i = 0; i < values.size() - 1; i++)
		{
			Customer item = values.get(i);
			if (item.compareTo(pivot) < 0)
			{
				leftValues.add(item);
		
			} else
			{
				rightValues.add(item);
			}
		}

		// rekursiv linken und rechten Teilbaum splitten
		ArrayList<Customer> sortedLeftValues = quickSort(leftValues);
		ArrayList<Customer> sortedRightValues = quickSort(rightValues);

		// Links und rechts zusammenfügen
		ArrayList<Customer> sortedValues = new ArrayList<>();

		sortedValues.addAll(sortedLeftValues);
		sortedValues.add(pivot);
		sortedValues.addAll(sortedRightValues);

		return sortedValues;
	}

}
