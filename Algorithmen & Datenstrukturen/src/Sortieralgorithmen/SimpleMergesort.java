package Sortieralgorithmen;

import java.util.Arrays;
import java.util.Random;

public class SimpleMergesort
{

	public static void main(String[] args)
	{
		int[] values = new int[10000];
//		{ 2, 7, 9, 1, 3, 4 };

		Random random = new Random();
		
		for(int i = 0; i < values.length; i++)
		{
			values[i] = random.nextInt();
		}
		
		long startTime = System.nanoTime();
		
		int[] sortedValues = mergeSort(values);
		
		long endTime = System.nanoTime();
		long diff = endTime - startTime;
		
		System.out.println("sorted in " + diff / 1000000);
		
		
		
//		for (int value : values)
//		{
//			System.out.print(value + ", ");
//		}
//
//		System.out.println();

//	int[] sortedValues = mergeSort(values);

//		for (int value : sortedValues)
//		{
//			System.out.print(value + ", ");
//		}

	}

	private static int[] mergeSort(int[] values)
	{
		// Abbruch Bedingung
		if (values.length == 1) // wenn ich nur mehr 1 Zahl drinnen habe
		{
			return values;
		}

		// Values in 2 Array aufteilen
		int rightSide = values.length / 2;
		int leftSide = values.length - rightSide;

		
		// Array - Hälften werden kopiert
		int[] leftValues = Arrays.copyOfRange(values, 0, leftSide);
		int[] rightValues = Arrays.copyOfRange(values, leftSide, values.length);

//		System.out.println(Arrays.toString(leftValues));
//		System.out.println(Arrays.toString(rightValues));

		
		// rekursiv linken und rechten Teilbaum splitten
		int[] sortedLeftValues = mergeSort(leftValues);
		int[] sortedRightValues = mergeSort(rightValues);

		
		// Links und rechts zusammenfügen
		int[] sortedValues = new int[values.length];

		int indexLeft = 0;
		int indexRight = 0;
		
		
		for (int i = 0; i < sortedValues.length; i++)
		{
			// wenn links keine Werte mehr drin stehen, nimm nur mehr die Rechten
			if(indexLeft >= sortedLeftValues.length)
			{
				sortedValues [i] = sortedRightValues[indexRight];
				indexRight++;
				continue;
			}
			
			if(indexRight >= sortedRightValues.length)
			{
				sortedValues [i] = sortedLeftValues[indexLeft];
				indexLeft++;
				continue;
			}	
			
			if (sortedLeftValues[indexLeft] < sortedRightValues[indexRight])
			{
				sortedValues [i] = sortedLeftValues [indexLeft];
				indexLeft++;
			}
			else
			{
				sortedValues [i] = sortedRightValues[indexRight];
				indexRight++;
			}
		}

		return sortedValues;
	}

}
