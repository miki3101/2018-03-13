package Sortieralgorithmen;

public class SimpleBubblesort
{
	public static void main(String[] args)
	{
		int[] values = new int[]
		{ 7, 5, 4, 3, 1 };

		System.out.println("Vor dem Sortieren: ");

		for (int value : values)
		{
			System.out.print(value + ", ");
		}

		System.out.println();
		System.out.println();

		bubbleSortRekursiv(values, 0);

		System.out.println();
		System.out.println("Nach dem Sortieren:");

		for (int value : values)
		{
			System.out.print(value + ", ");
		}

	}

	public static void bubbleSort2(int[] values)
	{
		for (int k = 1; k < values.length; k++)
		{
			boolean swap = false;
			System.out.println("Durchlauf: " + k);

			for (int i = 0; i < values.length - k; i++) // Laufzeit wird erh�ht, indem ich das k aus der �u�eren
														// Schleife nehme
			{
				System.out.println(values[i] + ">" + values[i + 1]);

				if (values[i] > values[i + 1])
				{
					int temp = values[i];
					values[i] = values[i + 1];
					values[i + 1] = temp;
					swap = true;
				}
			}
			if (!swap)
			{
				return;
			}

		}

	}

	public static void bubbleSort(int[] values)
	{
		boolean swap;

		int k = 1;

		do
		{
			System.out.println("Durchlauf: " + k);
			
			swap = false;
			
			for (int i = 0; i < values.length - k; i++) // Laufzeit wird erh�ht, indem ich das k aus der �u�eren
														// Schleife nehme
			{
				System.out.println(values[i] + ">" + values[i + 1]);

				if (values[i] > values[i + 1])
				{
					int temp = values[i];
					values[i] = values[i + 1];
					values[i + 1] = temp;
					swap = true;
				}
			}
			
			k++;
		} while (swap);

	}
	
	public static void bubbleSortRekursiv(int[] values, int x)
	{
		outerBubbleSort(values, 1);
	}
	
	public static void outerBubbleSort(int []values, int k)
	{
		System.out.println("Durchlauf: " + k);
		
		boolean swap = innerBubbleSort(values, 0, k);
		k++;
		
		if(swap)
		{
			outerBubbleSort(values, k);
		}
	}
	
	public static boolean innerBubbleSort (int[] values, int x, int k)
	{

		if(x >= values.length-k)
			return false;
		
		System.out.println(values[x] + ">" + values[x + 1]);
		
		boolean swap = false;
		
		if (values[x] > values[x + 1])
		{
			int temp = values[x];
			values[x] = values[x + 1];
			values[x + 1] = temp;
			swap = true;
		}
		x++;
		
		innerBubbleSort(values, x, k);
		return swap;
	}
}
