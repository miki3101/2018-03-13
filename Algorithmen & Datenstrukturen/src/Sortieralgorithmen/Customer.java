package Sortieralgorithmen;

public class Customer implements Comparable<Customer>
{
	
	public Integer id;
	public String name;

	public Customer(Integer id, String name)
	{
		this.id = id;
		this.name = name;
	}

	public int getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String toString()
	{
		return id + ", Name:" + name;
	}

	public int compareTo(Customer other)
	{
		 return this.id.compareTo(other.id);	
//		return this.name.compareTo(other.name);
	}


	


	
	
	

	
	
}
