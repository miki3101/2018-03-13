package Tree;

public class TreeNode
{

	private String value;
	private TreeNode left;
	private TreeNode right;

	public TreeNode(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

	public void add(String newValue)
	{
		// newValue < value => links;
		// newValue > value => rechts;

		if (newValue.compareTo(value) < 0) // links
		{
			if (left == null)
			{
				left = new TreeNode(newValue);
			} else
			{
				left.add(newValue); // rekursiv aufrufen
			}

		} else if (newValue.compareTo(value) > 0) // rechts
		{
			if (right == null)
			{
				right = new TreeNode(newValue);
			} else
			{
				right.add(newValue);
			}
		}
	}

	public TreeNode getLeft()
	{
		return left;
	}

	public TreeNode getRight()
	{
		return right;
	}

	public void preOrder()
	{
		System.out.println("Node: " + value);
		
		if (left != null)
		{
			left.preOrder();
		}
		if (right != null)
		{
			right.preOrder();
		}
	}

	public boolean searchPreOrder(String pattern)
	{
		System.out.println("Node: " + value);

		if (value.equals(pattern))
		{
			return true;
		}

		if (left != null)
		{
			boolean success = left.searchPreOrder(pattern);

			if (success == true)
				return true;
		}
		if (right != null)
		{
			boolean success = right.searchPreOrder(pattern);

			if (success == true)
				return true;
		}
		return false;
	}

	public void inOrder()
	{
		if (left != null)
		{
			left.inOrder();
		}
		
		System.out.println("Node: " + value);

		if (right != null)
		{
			right.inOrder();
		}
	}

	public String toString()
	{
		return "Node:" + value + " (left: " + left + ", right: " + right + ")";
	}

}
