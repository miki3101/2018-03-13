package Tree;

public class DemoTree
{

	public static void main(String[] args)
	{
		MyTree tree = new MyTree();
		
		tree.add("D");
		tree.add("F");
		tree.add("C");
		tree.add("E");
		tree.add("A");
		tree.add("B");
		
		System.out.println("PreOrder: \n");
		
		tree.preOrder();
		
		System.out.println("--------------");
			
		System.out.println("Search PreOrder: \n");
		tree.searchPreOrder("F");
		
		System.out.println("-------------");
		
		System.out.println("InOrder: \n");
		
		tree.inOrder();
		
		System.out.println("-------------");
		
		
		
		
		
	}
}
