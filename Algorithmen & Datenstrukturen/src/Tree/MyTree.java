package Tree;

public class MyTree
{
	private TreeNode root;

	public void add(String value)
	{
		if (root == null)
		{
			root = new TreeNode(value);
			return;
		}
		root.add(value);
	}

	public void preOrder()
	{
		if (root != null)
		{
			root.preOrder();
		}
	}

	public boolean searchPreOrder(String value)
	{
		if (root != null)
		{
			root.searchPreOrder(value);
			return true;
		}
		return false;
	}
	
	public void inOrder()
	{
		if(root != null)
		{
			root.inOrder();
		}
	}
}
