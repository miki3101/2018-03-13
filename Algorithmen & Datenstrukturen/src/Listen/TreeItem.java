package Listen;

public class TreeItem
{
	private String value;
	private TreeItem next;
	
	public TreeItem(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{	
		return value;
	}
	
	public void setNext(TreeItem next)
	{
		this.next = next;
	}

	public TreeItem getNext()
	{
		return next;
	}
	
}
