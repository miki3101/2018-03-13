package Listen;

public class DemoArrayList {

	public static void main(String[] args) {
		
		MyArrayList list = new MyArrayList();
		
		list.add("Eintrag 1");
		list.add("Eintrag 2");
		list.add("Eintrag 3");
		list.add("Eintrag 4");
		list.add("Eintrag 5");
		list.add("Eintrag 6");
		list.add("Eintrag 7");
		list.add("Eintrag 8");
		list.add("Eintrag 9");
		list.add("Eintrag 10");
		
		for(int i = 0; i<list.getCount(); i++)
		{
			System.out.println(list.get(i));
		}
		System.out.println("---------");
		System.out.println("L�nge der Liste: " + list.getCount());
		System.out.println("----------");
		
		System.out.println("Eintrag an Index 3 wird gel�scht, alle anderen r�cken vor");
		list.remove(3);
		list.add("new value");
		
		for(int i = 0; i<=list.getCount()-1; i++)
		{
			System.out.println(list.get(i));
		}
		
		System.out.println("---------");
		
		list.insert(3, "insert Value");
		
		for(int i = 0; i<=list.getCount(); i++)
		{
			System.out.println(list.get(i));
		}
		
		
		

		

		
	}

}
