package Listen;

import java.util.ArrayList;

public class PerformanceDemo
{

	public static void main(String[] args)
	{
		ArrayList<String>list = new ArrayList<>();
		
		int count = 2000000;
		long startTime = System.nanoTime(); // aktuelle Systemzeit in Nanosekunden (LinuxZeit)
	
		
		for(int i=0; i<count; i++)
		{
			list.add("intem" + i);
		}
		
		long endTime = System.nanoTime();
		double diff = (endTime-startTime) / 1000000;
		
		System.out.println("JavaArrayList mit 1.000.000 Zahlen bef�llen dauert: " + diff + "ms");
		
		MyArrayList liste = new MyArrayList();
		
		
		long startTime1 = System.nanoTime(); // aktuelle Systemzeit in Nanosekunden (LinuxZeit)
	
		
		for(int i=0; i<count; i++)
		{
			liste.add("intem" + i);
		}
		
		long endTime1 = System.nanoTime();
		double diff1 = (endTime1-startTime1) / 1000000;
		
		System.out.println("MyArrayList mit 2.000.000 Strings bef�llen dauert: " + diff1 + "ms");
		
		
		
		
	}

}
