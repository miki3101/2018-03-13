package Listen;

public class DemoLinkedList {

	public static void main(String[] args)
	{
		MyLinkedList ll = new MyLinkedList();
		
		ll.add("Eintrag 1");
		ll.add("Eintrag 2");
		ll.add("Eintrag 3");
		ll.add("Eintrag 4");
		
		ll.printout();
		System.out.println("---------");

		ll.remove(2);
		System.out.println("Nach remove: ");
		ll.printout();
		System.out.println("--------");
		ll.remove(0);
		System.out.println("Nach remove von Stelle 0");
		ll.printout();
		System.out.println("--------");
		
	}

}
