package Listen;

public class MyLinkedList

{
	private TreeItem firstItem;
	private TreeItem lastItem;

	public void add(String value)
	{
		if (firstItem == null)
		{
			firstItem = new TreeItem(value);
			lastItem = firstItem;
		} else
		{
			TreeItem newItem = new TreeItem(value);

			lastItem.setNext(newItem);
			lastItem = newItem;
		}
	}

	public void printout()
	{
		TreeItem next = firstItem;
		while (next != null)
		{
			System.out.println(next.getValue());
			next = next.getNext();
		}
	}

	public void remove(int index)
	{
		if(index == 0)
		{
			TreeItem next = firstItem.getNext();
			firstItem.setNext(null);
			firstItem = next;
		}
		
		TreeItem next = firstItem;
		TreeItem vorgaenger = null;

		
		if(vorgaenger == lastItem)
		{
			lastItem = next;
		}
		
		
		int count = 0;

		while (next != null)
		{
			count++;
			next = next.getNext();
			if (count == index - 1)
			{
				vorgaenger = next.getNext();
				next.setNext(vorgaenger.getNext());
				break;
			}
		}
	}
}
