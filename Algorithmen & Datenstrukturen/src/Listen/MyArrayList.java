package Listen;

import java.util.Arrays;

public class MyArrayList {
	
		private int count;
		private String [] values;
	
		public MyArrayList()
		{
			values = new String[5];
			count = 0;
		}
		
		public void add(String value)
		{
			extendetArrayNeeded();
				values[count] = value;
				count++;
		}
		
		public void extendetArrayNeeded()
		{
			if(count >= values.length)
			{
				String[] newValues = Arrays.copyOf(values, values.length * 2);
				values = newValues;
				
//				System.out.println("new length: " + values.length);
			}		
		}
		
		public String get(int index)
		{
			return values[index];
		}
		
		public int getCount()
		{
			return count;
		}
		
		public void remove(int index)
		{		
			for(int i = index; i<values.length-1; i++)
			{
				values[i] = values[i+1];	
			}
			values[values.length - 1] = null;
			count--;
		}
		
		public void insert(int index, String value)
		{
			extendetArrayNeeded();
			
			for(int i = values.length-1; i>index; i--)
			{
				values[i] = values[i-1];	
			}
			values[index] = value;
			count++;	
		}
}
