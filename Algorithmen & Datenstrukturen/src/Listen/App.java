package Listen;

import java.util.ArrayList;

public class App {

	public static void main(String[] args)
	{
	
		// legt im Hintergrund eine ArrayList an mit fixem Speicherplatz
		ArrayList<String> list = new ArrayList<>();
		
		list.add("Hallo"); 

		// alle Eintr�ge werden gel�scht, aber Gr��e wird nicht ver�ndert
		list.clear(); 
		list.add("Hallo");
		
		// l�scht einen einzelnen Eintrag
		list.remove(0); // index oder wert
		
		
 
	}

}
