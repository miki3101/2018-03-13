package Map;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class HashMapDemo
{

	public class InnerMain implements BiConsumer<Integer, String>	// Anonyme Klasse (f�r ForEach)
	{

		@Override
		public void accept(Integer t, String u)		// dazugeh�rige anonyme Methode
		{
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	public static void main(String[] args)
	{
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		map.put(3, "Hello");
		map.put(1, "World");
		map.put(4, "!");
		
		map.replace(1, "Replace");	// Ersetze beim Key "" den Wert ""
		
		
		for (int key : map.keySet())	// kann mir Keys und die dazugeh�rigen Values holen
		{
			System.out.println("Key: " + key);	// gibt alle Key zur�ck
			
			String value = map.get(key);	// gibt alle Values zur�ck
			System.out.println("Value: " + value);
		}
		
		System.out.println("---------------");
		
		for (String value : map.values())
		{
			System.out.println("Value: " + value);	// gibt mir alle Values zur�ck
		}
		
		System.out.println("-------------");
		System.out.println("Entry Set: \n");
		
		for(Map.Entry<Integer, String> entry : map.entrySet())	// gibt eine Liste von Key mit Value zur�ck
		{
			System.out.println("Entry: " + entry);
		}
	
		System.out.println("-------------");
		
		// Anonyme Klasse und anonyme Method
		
		// haben den Vorteil, dass ich nicht immer eine neue Klasse erstellen muss und importieren muss
		// event - man gibt jemanden eine Methode, dass, wenn die Klasse verwendet wird, wird diese Methode aufgerufen
		// wird f�r jeden Wert aufgerufen
		// man kann auf Variablen au�erhalb der Klasse nicht zugreifen, man m�sste eine globale Variable erstellen
	
		
		
		int count = 0;		// auf diese Variable, kann in der anonymen Methode nicht zugegriffen werden
		System.out.println("Count: " + count);
	
		map.forEach(new BiConsumer<Integer, String>()		
		{

			@Override
			public void accept(Integer key, String value)		// man verwendet eine anonyme Methode um auf eine anonyme Klasse zuzugreifen
			{
				System.out.println("ForEach: " + key +  " - " + value);
			}
			
		});
		
		
		
	}

}
