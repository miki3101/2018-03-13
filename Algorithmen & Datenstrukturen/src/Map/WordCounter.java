package Map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class WordCounter
{

	// File Einlesen und die Anzahl der W�rter in eine Map speichern

	public static void main(String[] args)
	{

		String filePath = "WordCounter.txt";
		count(filePath);
	}

	public static Map<String, Integer> count(String filePath)
	{

		HashMap<String, Integer> map = new HashMap<>();

		try (FileReader file = new FileReader(filePath); // liest das File als Text ein
				BufferedReader reader = new BufferedReader(file); // Reader kann jetzt zeilenweise lesen
		)

		{

			String text = null;

			while ((text = reader.readLine()) != null) // liest zeilenweise ein, bis die Zeile zu Ende ist
			{
				String[] fields = text.split(" "); // splitet zwischen den W�rtern und gibt es in eine Liste

				for (String string : fields)	 // geht die Liste durch
				{
					if (!map.containsKey(string)) // wenn das Wort noch nicht drinnen ist
					{
						map.put(string, 1);		// gibt es als Key mit dem Value 0 rein
					} else
					{
						int x = map.get(string); // wenn es schon drinnen ist hol dir den Key
						x++; 					// erh�he den Counter um eins
						map.put(string, x);
					}
				}

				for (Entry<String, Integer> eintrag : map.entrySet()) // iteriere durch die HashMap
				{
					System.out.print("Wort '" + eintrag.getKey() + "'"); // gibt die Keys aus
					System.out.println("\t" + eintrag.getValue() + "x"); // gibt die Values aus
				}

			}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return map;
	}

}
