package Map;

public class SimpleMapDemo
{

	public static void main(String[] args) throws Exception
	{
		SimpleMap sm = new SimpleMap();
		
		sm.put("Hello", 4);
		sm.put("World", 3);
		sm.put("Wie", 1);
		
		
		System.out.println("Value von 'Hello': " + sm.get("Hello"));
		
		System.out.println("Ist 'Hello' enthalten: " + sm.containsKey("Hello"));
		
		sm.replace("Hello", 5);
		System.out.println("Neuer Value von 'Hello': " + sm.get("Hello"));
		
		sm.remove("Hello");
		System.out.println("Ist 'Hello' enthalten: " + sm.containsKey("Hello"));
	
		System.out.println("\n" + "Values:");
		for (String key : sm.keySet())
		{
			int value = sm.get(key);
			System.out.println(value);
		}	
	}
	
	
}
