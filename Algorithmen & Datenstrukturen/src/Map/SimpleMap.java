package Map;

import java.util.ArrayList;
import java.util.List;

public class SimpleMap
{

	private ArrayList<String> keys = new ArrayList<>();
	private ArrayList<Integer> values = new ArrayList<>();

		
	public void put(String key, int value) throws Exception
	{
		if(keys.contains(key))
		{
			throw new Exception("Key schon vorhanden!");
		}
		keys.add(0, key);
		values.add(0, value);
	}

	public int get(String key) throws Exception
	{
		int index = keys.indexOf(key);
		
		if(index == -1)
		{
			throw new Exception("Key nicht vorhanden!");
		}
		return values.get(index);
	}

	public void remove(String key)
	{
		int index = keys.indexOf(key);
		keys.remove(index);
		values.remove(index);
	}

	public boolean containsKey(String key)
	{
		return keys.contains(key);
	}

	public void replace(String key, int value)
	{
		int index = keys.indexOf(key);	
		values.set(index, value);
	}
	
	public List<String> keySet()
	{
		return keys;
	}
}
