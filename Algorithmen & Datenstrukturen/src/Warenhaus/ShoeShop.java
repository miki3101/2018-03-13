package Warenhaus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

public class ShoeShop {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			List<Shoe> allShoes = new ArrayList<Shoe>();

			Warehouse warehouse = new Warehouse();
			warehouse.getAllShoes(allShoes);

			ArrayBlockingQueue<Shoe> queue = new ArrayBlockingQueue<>(10);
			
			System.out.println("Shoes in Queue: " + queue.size());
			
			for(int i = 0; i < 10; i++){
				Shoe myShoe = allShoes.get(i);
				queue.add(myShoe);
				
				//if (queue.offer(myShoe) == false)
				//	System.out.println("Queue full!");
			}
			
			System.out.println("Shoes in Queue: " + queue.size());
			
			while(queue.isEmpty() == false){
				
				Shoe myShoe = queue.poll();
				System.out.println(myShoe);
			}
			
			System.out.println("Shoes in Queue: " + queue.size());			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
