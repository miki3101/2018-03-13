package Hashtable;

public class SimpleHashtable
{

	private Hashbucket[] values;

	public SimpleHashtable()
	{
		values = new Hashbucket[7];
	}

	public void add(String value)
	{
		int hashCode = Math.abs(value.hashCode()); // damit keine Negativwerte zurückkommen
		int index = hashCode % values.length;

		Hashbucket bucket = values[index];

		if (bucket == null)
		{
			bucket = new Hashbucket();
			values[index] = bucket;
		}

		System.out.println("Hashcode von " + value + ": " + hashCode);
		System.out.println("'" + value + "' wird auf den Index '" + index + "' gelegt");

		bucket.add(value);
		System.out.println();
	}

	public int getSize()
	{
		int size = 0;

		for (Hashbucket hashbucket : values)
		{
			if (hashbucket != null)
			{
				size += hashbucket.getSize();
			}
		}

		return size;
	}

	public int size()
	{
		int anzahl = 0;

		for (Hashbucket hashbucket : values)
		{
			if (hashbucket != null)
			{
				anzahl++;
			}
		}
		return anzahl;
	}

	public void remove(String value)
	{
		int hashCode = Math.abs(value.hashCode()); // damit keine Negativwerte zurückkommen
		int index = hashCode % values.length;
		Hashbucket bucket = values[index];

		if (bucket == null)
			return;

		System.out.println("'" + value + "' vom index '" + index + "' wird gelöscht");
		bucket.removeValue(value);
	}

	public int length()
	{
		return values.length;
	}

	public String getElemente()
	{
		String elements = null;

		for (Hashbucket hashbucket : values)
		{
			if (hashbucket != null)
			{
				hashbucket.getElemente();
			}
		}
		return elements;
	}
}
