package Hashtable;

public class HashtableDemo
{

	public static void main(String[] args)
	{
		
		SimpleHashtable hashtable = new SimpleHashtable();
		
		hashtable.add("Hund");
		hashtable.add("Katze");
		hashtable.add("Maus");
		hashtable.add("Elefant");
		hashtable.add("Tiger");
		hashtable.add("Schildkröte");
		hashtable.add("Tiger");
		
		
		System.out.println("--------------");
		System.out.println("Gesamtanzahl der Hashbuckets in der SimpleHashtable: " + hashtable.length());
		System.out.println("Anzahl der belegten Hashbuckets in der SimpleHashtable: " + hashtable.size());
		System.out.println("Elemente in der SimpleHashtable: " + hashtable.getSize());
		System.out.println("--------------");
		
		System.out.println(hashtable.getElemente());
		hashtable.remove("Tiger");
		
		System.out.println();
		
		System.out.println("Elemente in der SimpleHashtable: " + hashtable.getSize());
		System.out.println(hashtable.getElemente());
		
		
	}

}
