package Hashtable;

import java.util.ArrayList;

public class Hashbucket
{
	
	private ArrayList<String> list;

	public Hashbucket()
	{
		this.list = new ArrayList<>();
	}

	public void add(String value)
	{
		list.add(value);
		System.out.println("Objekte in diesem Hashbucket: " + list.size());
	}

	public int getSize()
	{
		return list.size();
	}

	public void removeValue(String value)
	{
		list.remove(value);
	}

	public void getElemente()
	{
		for (String string : list)
		{
			System.out.print(string + ", ");
		}
	}
}
