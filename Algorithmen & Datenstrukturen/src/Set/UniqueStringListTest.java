package Set;

public class UniqueStringListTest
{

	public static void main(String[] args)
	{
		UniqueStringList list = new UniqueStringList();
		
		System.out.println("Hund wird hinzugefügt: " + list.add("Hund"));
		System.out.println("Katze wird hinzugefügt: " + list.add("Katze"));
		System.out.println("Maus wird hinzugefügt: " + list.add("Maus"));
		System.out.println("Hund wird hinzugefügt: " + list.add("Hund"));
		System.out.println("-------------");
		
		for (String string : list) // funktioniert weil das Interface Iterable implementiert wurde
		{
			System.out.println(string);
		}
		
		
		System.out.println("-------------");
		System.out.println("Liste enthält:" + list);
		
		list.remove("Katze");
		System.out.println("Katze wird entfernt");
		System.out.println("Liste enthält:" + list);
		System.out.println("-------------");
		
		System.out.println("Ist 'Maus' in der Liste: " + list.contains("Maus"));
		System.out.println("Ist 'Katze' in der Liste: " + list.contains("Katze"));
		
		
		
	}

}
