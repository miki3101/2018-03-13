package Set;

import java.util.HashSet;

public class HashSets
{

	public static void main(String[] args)
	{
		HashSet<String> set = new HashSet<String>();

		set.add("Hund");
		set.add("Maus");
		set.add("Katze");
		set.add("Hund");

		System.out.println("String Set: ");

		for (String string : set)
		{
			System.out.println(string);
		}

		System.out.println("----------");
		System.out.println("Tiere Set: ");

		HashSet<Haustier> set2 = new HashSet<>();

		set2.add(new Haustier("Hund", 1));
		set2.add(new Haustier("Maus", 3));
		set2.add(new Haustier("Katze", 3));
		set2.add(new Haustier("Hund", 4));
		set2.add(new Haustier("Hund", 1));
		

		for (Haustier haustier : set2)
		{
			System.out.println(haustier);
		}
		
		System.out.println("-------------");

		Haustier hund = new Haustier("hund", 1);
		Haustier hund2 = new Haustier("hund", 3);
		Haustier hund3 = hund2;
		Haustier katze = new Haustier("katze", 4);
		
		System.out.println("Hund:   " + hund.hashCode());
		System.out.println("Hund 2: " + hund2.hashCode());
		System.out.println("Hund 3: " + hund3.hashCode());
		System.out.println("-------------");
		
		
		
		String hundString = "hund";
		String hundString2 = "hund";

		System.out.println("StringHund:   " + hundString.hashCode());
		System.out.println("StringHund 2: " + hundString2.hashCode());
		
		
		System.out.println("------------");
		

		if (hund.equals(katze))
		{
			System.out.println("Hund ist keine Katze");
		}

		if (hund.equals(hund))
		{
			System.out.println("Hund ist ein Hund");
		}

		if (hund.equals(hund2))
		{
			System.out.println("Hund ist ein Hund 2");
		}

	}

}
