package Set;

import java.util.ArrayList;
import java.util.Iterator;

public class UniqueStringList implements Iterable<String>
{
	private ArrayList<String> liste;
	
	public UniqueStringList()
	{
		liste = new ArrayList<String>();
	}
	
	public boolean add(String value)
	{
		if(!liste.contains(value))
		{
			liste.add(value);
			return true;
		}
		
		return false;
	}
	
	public void remove(String value)
	{
		liste.remove(value);
	}
	
	public boolean contains(String value)
	{
		return liste.contains(value);
		
		//Variante 2 mit equals:
//		for (String string : liste)
//		{
//			if(string.equals(value))
//				return true;
//		}
		
		//Variante 3 mit HashCode:
//		int valueHashCode = value.hashCode();
//		
//		for (String string : liste)
//		{
//			if(string.hashCode() == valueHashCode)
//				return true;
//		}
//		return false;
	}
	
	public String toString()
	{
		String list = " ";
		
		for (String string : liste)
		{
			list += string + ", ";
		}
		return list;
	}

	@Override
	public Iterator<String> iterator()
	{
		UniqueIterator it = new UniqueIterator(this);
		return it;
	}

	public int size()
	{
		return liste.size();
	}

	public String get(int index)
	{
		return liste.get(index);
	}
	
}
