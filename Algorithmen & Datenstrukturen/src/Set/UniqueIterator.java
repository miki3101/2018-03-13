package Set;

import java.util.Iterator;

public class UniqueIterator implements Iterator<String>
{
	
	private UniqueStringList list;
	private int index;
	
	public UniqueIterator(UniqueStringList list)
	{
		this.list = list;
		this.index = 0;
	}

	@Override
	public boolean hasNext()
	{
		if(index < list.size() -1)
			return true;
		
		return false;
	}

	@Override
	public String next()
	{
		String value = list.get(index);
		index++;
		
		return value;
	}
	
}
