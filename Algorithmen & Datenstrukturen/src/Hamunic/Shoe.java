package Hamunic;

public class Shoe
{
	private String name;
	private int preis;
	private int groesse;
	
	
	public Shoe(String name, int preis)
	{
		super();
		this.name = name;
		this.preis = preis;
	}


	public String getName()
	{
		return name;
	}


	public void setName(String name)
	{
		this.name = name;
	}


	public int getPreis()
	{
		return preis;
	}


	public void setPreis(int preis)
	{
		this.preis = preis;
	}


	public int getGroesse()
	{
		return groesse;
	}


	public void setGroesse(int groesse)
	{
		this.groesse = groesse;
	}
	
	
	
}
