package Queue;

import java.util.ArrayDeque;
import java.util.PriorityQueue;

public class Queue
{
	public static void main(String[] args)
	{
		PriorityQueue<String> values = new PriorityQueue<String>();

		values.offer("Value 1");
		values.offer("Value 2");
		values.offer("Value 3");

		System.out.println("Size: " + values.size());

		values.peek(); // gibt das n�chste Element, dass aus der Lister herausgenommen wird (nimmt es
						// aber nicht heraus)
		String value = values.peek();

		System.out.println("Peek: " + value);

		value = values.poll(); // das �lteste Element wird herausgenommen und zur�ckgegeben
		System.out.println("Value: " + value);

		value = values.poll(); // das �lteste Element wird herausgenommen und zur�ckgegeben
		System.out.println("Value: " + value);

		System.out.println("Size: " + values.size());

		System.out.println("-------------");

		ArrayDeque<String> de = new ArrayDeque<>();
		
		de.addFirst(value); // Exception m�glich, wenn Liste voll
		de.addLast(value);
		de.getFirst(); // Excetpion m�glich, wenn etwas schief geht
		de.getLast();
		de.removeFirst(); // Exception m�glich, wenn Liste leer
		de.removeLast();
		
		// besser offer und poll statt add und remove verwenden
		
		de.offerFirst("Value 1"); // keine Exception m�glich, da es einen boolischen Wert zur�ckgibt
		de.offerFirst("Value 2");
		de.offerLast("Value 3");

		System.out.println("Peek First: " + de.peekFirst());
		System.out.println("Peek Last: " + de.peekLast());

		for (int i = 0; i < 3; i++)
		{
			String first = de.pollFirst();
			System.out.println("First: " + first);
		}
	}

}
