package Queue;

public class SimpleDequeTest
{

	public static void main(String[] args)
	{
		SimpleDeque de = new SimpleDeque();
		
		de.offerFirst("Value 1");
		de.offerFirst("Value 2");
		de.offerLast("Value 3");
		de.offerFirst("Value 4");
		
		System.out.println("First: " + de.pollFirst());
		System.out.println("Last: " + de.pollLast());

	}

}
