package Queue;

import java.util.ArrayList;

public class SimpleQueue
{
	private ArrayList<String> values;

	public SimpleQueue()
	{
		values = new ArrayList<String>();
	}

	public void offer(String value)
	{
		values.add(0, value);
	}

	public String poll()
	{
		if (values.isEmpty())
			return "IS EMPTY";
		
		return values.remove(0);
	}
	
	public String peek()
	{
		return values.get(0);
	}
	
	public int size()
	{
		return values.size();
	}

	public boolean isEmpty()
	{
		return values.isEmpty();
	}
}
