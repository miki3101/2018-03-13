package Queue;

import java.util.ArrayList;

public class SimpleDeque
{
	private ArrayList<String> values;

	public SimpleDeque()
	{
		values = new ArrayList<String>();
	}

	public void offerFirst(String value)
	{
		values.add(0, value);
	}
	
	public void offerLast(String value)
	{
		values.add(value);
	}

	public String pollFirst()
	{
		if (values.isEmpty())
			return "IS EMPTY";
		
		return values.remove(0);
	}
	
	public String pollLast()
	{
		if (values.isEmpty())
			return "IS EMPTY";

		return values.remove(values.size()-1);
	}
	
	public String peekFirst()
	{
		String first = values.get(0);
		return first;
	}

	public String peekLast()
	{
		String last = values.get(values.size()-1);
		return last;
	}

	public boolean isEmpty()
	{
		return values.isEmpty();
	}
}
