package Queue;

public class SimpleQueueTest
{

	public static void main(String[] args)
	{
		SimpleQueue queue = new SimpleQueue();
		
		queue.offer("Value 1");
		queue.offer("Value 2");
		queue.offer("Value 3");
		queue.offer("Value 4");

		while(!queue.isEmpty())
		{
			System.out.println(queue.poll());
		}
		
		String element = queue.poll();
		System.out.println(element);
		
		System.out.println("----------");
		

	}

}
