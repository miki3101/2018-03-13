package Rekursionen;

public class factorial
{

	public static void main(String[] args)
	{
		int n = 6;

		System.out.println("Factorial: " + fact(n));
		System.out.println("BunnyEars: " + bunnyEars(n));
		System.out.println("Fibonacci: " + fibonacci(n));
		System.out.println("BunnyEars2: " + bunnyEars2(n));
	}

	// n * (n-1) * (n-2)
	public static int fact(int n)
	{
		int result = 0;

		if (n == 1)
		{
			return 1;
		}

		result = n * (fact(n - 1));

		return result;
	}

	// z�hlen Sie alle Ohren von den Hasen
	public static int bunnyEars(int bunnies)
	{
		if (bunnies == 0)
		{
			return 0;
		}

		return bunnyEars(bunnies - 1) + 2;

	}

	// (f - 1) (f-2)
	public static int fibonacci(int n)
	{
		if (n == 0)
			return 0;
		if (n <= 2)
			return 1;

		return fibonacci(n - 1) + fibonacci(n - 2);
	}

	// jeder gerade Hase hat 2 Ohren jeder ungerade hat 3 Ohren // Ohren z�hlen
	public static int bunnyEars2(int bunnies)
	{
		if (bunnies == 0)
		{
			return 0;
		}
		if (bunnies % 2 == 0)
		{
			return bunnyEars2(bunnies - 1) + 3;
		}
		return bunnyEars2(bunnies - 1) + 2;
	}
	
	
}
