package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CombinedStreamsDemo {

	public static void main(String[] args) {
		try {
			FileOutputStream fos = new FileOutputStream("encrypted.txt");
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			EncryptionOutputStream eos = new EncryptionOutputStream(bos, 24);
			
			eos.write('a');
			eos.write('b');
			eos.write('c');
			eos.flush();
			eos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
