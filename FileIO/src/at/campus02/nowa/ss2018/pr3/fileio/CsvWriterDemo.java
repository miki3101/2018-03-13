package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;

public class CsvWriterDemo {

	public static void main(String[] args) {
		Reader in = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(in);
		Writer fw;
		try {
			fw = new FileWriter("noten.csv");
		} catch (IOException e1) {
			System.out.println("Konnte Datei nicht schreibend �ffnen: " + e1.getMessage());
			return;
		}
		String line;
		
		try {
			while ((line = br.readLine()) != null ) {
				if (line.equals("STOP")) {
					fw.close();
					return;
				}
				writeLine(line, fw);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void writeLine(String line, Writer pw) throws IOException {
		for (String i : line.split(" ")) {
			if (i.length() == 0) {
				continue;
			}
			if (i.charAt(i.length() -1) == ':') {
				pw.write(i.substring(0, i.length()-1));
			} else {
				pw.write(i);
			}
			pw.write(";");
		}
		pw.write("\r\n");
	}
	
	

}
