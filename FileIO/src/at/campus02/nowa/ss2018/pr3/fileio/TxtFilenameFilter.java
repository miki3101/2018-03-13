package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.File;
import java.io.FilenameFilter;

public class TxtFilenameFilter implements FilenameFilter {

	@Override
	public boolean accept(File arg0, String arg1) {
		return arg1.endsWith(".txt");
	}

}
