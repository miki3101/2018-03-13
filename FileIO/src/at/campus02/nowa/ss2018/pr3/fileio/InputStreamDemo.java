package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class InputStreamDemo {

	public static void main(String[] args) {
		try {
			File f = new File("output.txt");
			long length = f.length();
			System.out.println(length);
			InputStream inp = new FileInputStream("output.txt");
			FileOutputStream out = new FileOutputStream("output_copy.txt");
			int b;
			while ((b = inp.read()) != -1) {
				System.out.println(b);
				out.write(b);
			}
			inp.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
