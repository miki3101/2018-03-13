package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;

public class Application {

	public static void main(String[] args) throws LustigeException {

		File f = new File("test.txt");
		System.out.println(f.getAbsolutePath());
		
		for (String path : f.getAbsolutePath().split("\\" + File.separator)) {
			System.out.println(path);
		}
		
		/*
		 * Wenn nicht existent, dann erzeugen.
		 */
		if (!f.exists()) {
			//System.out.println(f.mkdirs());
			try {
				f.createNewFile();
			} catch (IOException e) {
				System.out.println("Fehler beim Erstellen der Datei: " + e.getMessage());
				e.printStackTrace();
				return;
			}
		}
		/*
		 * Wenn Verzeichnis, dann Inhalt auflisten.
		 */
		if (f.isDirectory()) {
			for (String l : f.list(new TxtFilenameFilter())) {
				System.out.println(l);
			}
		}
		
		System.out.println(f.toURI());
		System.out.println(f.pathSeparator);
		System.out.println(f.separator);
		
		try {
			FileInputStream inp = new FileInputStream("test.txt");
			
			int b;
			while ((b = inp.read()) != -1) {
				System.out.println(b);
				System.out.println(Character.toChars(b));
			}
			
			inp.close();
		
		} catch (FileNotFoundException e) {
			System.out.println("Datei wurde nicht gefunden: " + e.getMessage());
			return;
		} catch (IOException e) {
			System.out.println("Datei konnte nicht gelesen werden: " + e.getMessage());
			return;
		}
		
	}

}
