package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class AdapterStreamDemo {

	public static void main(String[] args) throws IOException {
		/* Datei als Byte-Stream �ffnen */
		InputStream ins = new FileInputStream("noten.csv");
		
		/* InputStream auf Reader �bersetzen */
		Reader isr = new InputStreamReader(ins);
		
		/* Reader f�r zeilenweises Lesen */ 
		BufferedReader br = new BufferedReader(isr);
		
		br.readLine();
		
		OutputStream out = new FileOutputStream("noten.csv");
		Writer wr = new OutputStreamWriter(out);
		wr.write("Zeile 1\r\n");
		
		

	}

}
