package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class WriterDemo {

	public static void main(String[] args) {
		System.out.println("Test");
		Writer out = new OutputStreamWriter(System.out);
		BufferedWriter bw = new BufferedWriter(out);
		try {
			bw.write("Dies ist ein test\r\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PrintWriter pw = new PrintWriter(bw);
		pw.println("ZWeiter test!");
		pw.flush();
		
}

}
