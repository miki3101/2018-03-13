package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class SystemInputReaderDemo {

	public static void main(String[] args) {
		Reader in  = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(in);
		
		String line;
		try {
			while ((line = br.readLine()) != null) {
				switch (line) {
				case "HELP":
					System.out.println("STOP: Stoppt die Anwendung");
					break;
				case "STOP":
					return;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
