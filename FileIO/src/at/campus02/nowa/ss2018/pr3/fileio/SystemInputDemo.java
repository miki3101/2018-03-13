package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.IOException;

public class SystemInputDemo {

	public static void main(String[] args) {
		int b;
		try {
			while ((b = System.in.read()) != -1) {
				char c = Character.toChars(b)[0];
				if (c == 'x' || c == 'X') {
					return;
				}
				System.out.print(b);
				System.out.println(c);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
