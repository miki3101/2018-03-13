package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.File;
import java.io.IOException;

public class ApplicationExceptionDemo {

	public static void main(String[] args) throws LustigeException {
		try {
			File f = new File("E:\\test.txt");
			System.out.println(f.getAbsolutePath());
			if (!f.exists()) {
				f.createNewFile();
				throw new LustigeException();
			}
		} catch (IOException e) {
			System.out.println("Wir haben einen Fehler festgestellt!");
			LustigeException le = new LustigeException();
			//le.addSuppressed(e);
			throw le;
		} catch (LustigeException e) {
			System.out.println("Wir haben einen lustigen Fehler festgestellt!");
			throw e;
		}

	}

}
