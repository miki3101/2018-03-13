package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileOutputDemo {
	public static void main(String[] args) {
		String text = "Dies ist mein Text, er ist nicht sehr interessant.";
		try {
			OutputStream out = new FileOutputStream("output.txt");
			for (char x : text.toCharArray()) {
				out.write(x);
			}
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
