package at.campus02.nowa.ss2018.pr3.fileio;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class EncryptionOutputStream extends FilterOutputStream {
	
	private int key;

	public EncryptionOutputStream(OutputStream arg0, int key) {
		super(arg0);
		this.key = key;
	}

	@Override
	public void write(int arg0) throws IOException {
		super.write(arg0 + key);
	}
	
	

}
