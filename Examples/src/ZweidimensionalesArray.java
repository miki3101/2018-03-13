//	Implementieren Sie die folgende Methode
//	puclic static double [][] generateIdentityMatrix (int size)
//	Methode soll mittels 2d double Array eine Einheitsmatrix erstellen, deren Gr��e �ber den Parameter size bestimmt ist
//	und diese zur�ckliefern. Die Diagonale der Matrix soll mit "1.0" initialisiert werden, die restlichen Werte sind "0.0"
//	erstellen Sie eine Methode um das zweidimensionale Array auszugeben

public class ZweidimensionalesArray
{	
	public static void main(String[] args)
	{
		int size = 5;									// gr��e wird definiert, reihen und stellen
		double [][] matrix1 = new double [size][size];	// wenn man nur durch die L�nge definiert, gibt er 
														// �berall den Wert 0.0 ein, aber die gr��e wird mit size gesetzt
		matrixFormatieren(size, matrix1);
		ausgabe(matrix1);
		
	}
	
	public static double [][] matrixFormatieren (int size, double [][] matrix1)
	{												
		for(int i = 0; i < matrix1.length; i++)			
		{
			matrix1[i][i] = 1.0;						// Reihe 0 und Stelle 0 wird Wert 1 eingesetzt
		}												// n�chste Runde in Reihe 1 und Stelle 1 wird der Wert 1 eingesetzt													// n�chste Runde in Reihe 2 und Stelle 2 wird der Wert 2 eingesetzt
		return matrix1;									// usw...
	}
	
	public static void ausgabe (double matrix1 [] [])
	{
		for(int i = 0; i < matrix1.length; i++)
		{
			for (int j = 0; j < matrix1.length; j++)
			{
				System.out.print(matrix1[i][j] + "\t");	// "\t" macht zwischen den Stellen einen Tabulator
			}
			System.out.println();						// damit nach jedem Array die Zeile gewechselt wird
		}
	}
}
