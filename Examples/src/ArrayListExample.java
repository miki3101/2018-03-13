import java.util.ArrayList;

public class ArrayListExample
{

	public static void main(String[] args)
	{
		ArrayList<String> namen = new ArrayList<>();	// erstellen einer leeren ArrayList
		
		namen.add("Susi");						// Elemente hinzuf�gen
		namen.add("ist");
		namen.add("3");
		namen.add("Jahre");
		namen.add("alt");
		
		System.out.println(namen);				// Ausgabe des ganzen Array
		System.out.println(namen.get(1));		// Ausgabe des Elementes an Stelle 1
		System.out.println(namen.size());		// Ausgabe der Anzahl der Elemente
		
		namen.add(2, "heute");					// hinzuf�gen eines Elements an Stelle 2
		System.out.println(namen);
		
		namen.set(5, "geworden");				// �ndern eines Elements an Stelle 5
		System.out.println(namen);
		
		namen.remove(2);						// entfernen eines Elements an Stelle 2
		System.out.println(namen);
		
	}
	

}
