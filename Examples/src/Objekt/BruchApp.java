package Objekt;

public class BruchApp
{

	public static void main(String[] args)
	{		
	
		Bruch bruch1 = new Bruch(2, 5);	// Erstellen eines neuen Objektes (Attribute werden gleich zugewiesen)
		Bruch bruch2 = new Bruch(4, 5);
	
		
		System.out.println(bruch1);			// Ausgeben des neuen Objektes
		System.out.println(bruch2);
		
		bruch2.summe(bruch1, bruch2);
		System.out.println(bruch2);
		
		
		
		
	}

}
