package Objekt;

public class Bruch
{
	private int zaehler;
	private int nenner;
	
	public Bruch (int zaehler, int nenner)
	{
		this.zaehler = zaehler;
		this.nenner = nenner;
	}
	public String toString()
	{
		return zaehler + "/" + nenner;
	}
	public int getZaehler()
	{
		return zaehler;
	}
	public void setZaehler(int zaehler)
	{
		this.zaehler = zaehler;
	}
	public int getNenner()
	{
		return nenner;
	}
	public void setNenner(int nenner)
	{
		this.nenner = nenner;
	}
	public void summe (Bruch bruch1, Bruch bruch2)
	{
		int zaehler1 = bruch1.getZaehler() * bruch2.getNenner() + bruch2.getZaehler() * bruch1.getNenner();
		int nenner1 = bruch1.getNenner() * bruch2.getNenner();
		
	}
	
	
}
