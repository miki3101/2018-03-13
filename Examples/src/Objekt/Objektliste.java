package Objekt;

import java.util.ArrayList;
import java.util.List;

public class Objektliste
{
	public static void main(String[] args)
	{
		Auto auto = new Auto("BMW");
		Person person = new Person("Max", 10);
		
		List<Object> objektliste = new ArrayList<>();
		
		objektliste.add(person);
		objektliste.add(auto);
		
		for (Object object : objektliste)
		{
			System.out.println(object);
		}
	}
}
