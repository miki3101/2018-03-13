package Objekt;

import java.util.Iterator;
import java.util.LinkedList;
import Objekt.Person;


public class LinkedList_Iterator
{
	public static void main(String[] args)
	{
		Person person1 = new Person("Max", 10);
		Person person2 = new Person("Susi", 60);
		Person person3 = new Person("Alex", 30);
		Person person4 = new Person("Maria", 20);
				
		LinkedList<Person> personen = new LinkedList<Person>();
		
		personen.add(person1);
		personen.add(person2);
		personen.add(person3);
		personen.add(person4);
		
		Iterator<Person> it = personen.iterator();
		
		while(it.hasNext())			// solange du einen Nachbarn hast, durchlaufe die Liste Personen
		{
			Person p = it.next();	// 												
			
			System.out.println("Zugriff mittels Objekt: " + p.getName());	
		}
		
		Person person5 = new Person("Ich bin erster", 50);
		
		personen.addFirst(person5);					// wird an die erste Stelle gesetzt
		
		System.out.println("-------------");
		
		it = personen.listIterator();				// listIterator() ist das gleiche wie iterator()
		
		while(it.hasNext())
		{
			Person p = it.next();
			System.out.println("Zugriff mittels Objekt: " + p.getName());
		}
	}
}
