
public class ForExample
{
//	For ist eine Z�hlschleife
	
//	verwendet man dann, wenn man etwas so lange ausf�hren m�chte, bis eine bestimmte Bedingung erf�llt ist
//	man kennt die Abbruchbedingung im Vorhinein
	
//	man ben�tigt daf�r einen Z�hler (i), der nur in der for Schleife zu sehen ist, der die Schleife dann solange wiederholt, bis die Bedingung erf�llt ist
//	Schleife setzt sich zusammen aus (Z�hler; Abbruchbedingung; rauf- oder runterz�hlen des Z�hlers)
	
//	Syntax: for(Start; Abbruchbedingung; Increment)		Schleife startet bei, wann endet die Schleife; z�hlt rauf oder runter
//				{										was soll wiederholt werden
//				}
	
	

	public static void main(String[] args)
	{
		int ergebnis = 0;					// Ergebnis auf 0 setzen;
		
		for (int i =0; i < 6; i++)			// Z�hler auf 0 setzen, Abbruchbedingung = Z�hler kleiner 5; Z�hler jede Runde um eines erh�hen
		{
			ergebnis = ergebnis + i;		// Z�hle das derzeitige Ergebnis zum Ergebnis der n�chsten Runde dazu 
		
		System.out.println(ergebnis);		// wenn ich nach jeder Runde das Ergebnis sehen m�chte
			
		}
		
		System.out.println(ergebnis);		// wenn ich das Ergebnis, erst NACH dem gesamten Durchlauf sehen m�chte
	}

}

