// http://michael.hahsler.net/JAVA/pdf/03_3Methoden.pdf
//http://www.javaschubla.de/2007/javaerst0130.html

public class MethodenExample
{
//	Methoden sind Programmteile, die mit Variablen von Objekten oder Klassen arbeiten
	
//	sollte man immer mit einem selbstsprechenden benennen (was macht diese Methode)
//	es ist egal, wo eine Methode in einer Klasse steht

//	Syntax:	Zugriffsregelung Aufrufoption R�ckgabewert Methodenname (Parametertyp Parameter)			
//			{																			was macht diese Methode
//				Variable definieren														nur wenn die Methode nicht void ist
//				return Variable															nur wenn die Methode nicht void ist
//			}

	public static void main(String[] args)				
	{
		int zahl1 = 10;
		int zahl2 = 20;
		int summe = machSumme(zahl1, zahl2);			// definiert die "summe" mit der Methode
		int i = 3;
		
		System.out.println(summe);						// gibt das Ergebnis der Methode "machSumme" aus

		mache5(i);										// ruft die Methode mache5 auf
		System.out.println(i);							// Variable i hat sich nicht ver�ndert

		
	}
	
	public static int machSumme(int zahl1, int zahl2)	// �ffentlich, statisch und mit r�ckgabewert; 
	{													// man gibt der Methode 2 Parameter von aussen mit
		int x = zahl1 + zahl2;							// definiert Variable x; diese hat die summe von zahl1 und zahl2
		
		return x;										// Variable x wird zur�ckgegeben
	}
	
	static void mache5(int i)
	  {
	    i = 5;
	  }
	
//	m�gliche Zugriffregelungen: "public" oder "private"				�ffentlich (kann auch in anderen Klassen verwendet werden)
//																	privat (kann nur in dieser Klasse verwendet werden)
//	m�gliche Aufrufoptionen:	"static" oder nichts				statisch (wenn die Methode nicht zu einem Objekt sondern einer Klasse geh�rt)
//	m�gliche R�ckgabewerte:		"void" oder nur Datentyp			void (wenn nichts zur�ckgegeben werden soll)
//																	jeder Datentyp ist m�glich, aber auch der selbe muss auch zur�ckgegeben werden
//	m�gliche Methodennamen		selbstdefinieren / selbstsprechend
	
//	wenn eine Variable au�erhalb einer Methode definiert ist, k�nnen alle Methoden diese Variable verwenden
//	statische Methoden k�nnen nur auf statische Variablen zugreifen deshalb immer statisch, au�er die Methode geh�rt zu einem Objekt
//	mann kann innerhalb einer Methode keine andere Methode definieren

//	eine Variable �ndert sich nicht, wenn sie als Parameter in eine Methode �bergeben wird und dort ver�ndert wird
//	
}
