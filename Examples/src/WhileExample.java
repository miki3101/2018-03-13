
public class WhileExample
{
//	While ist eine Abweisschleife
	
//	eine While Schleife verwendet man, wenn man etwas solange ausf�hren m�chte, bis die Bedingung "false" ist (die Bedingung nicht mehr erf�llt wird)
//	wird diese Bedingung gar nicht erf�llt, �berspringt er die while Schleife
	
//	Syntax: while (Bedingung)	solange die Bedingung true ist (boolean)
//			{					mach das
//			}

	public static void main(String[] args)
	{
		int zahl = 10;
		int x = 1;
		
		while (x < zahl)			// Solange x kleiner ist als zahl
		{
			x++;					// z�hle zu x 1 dazu
			
			System.out.println(x);	// wenn ich jede Runde sehen will, wie sich die Zahl erh�ht
		}
		
			System.out.println(x);	// wenn ich sehen will wie gro� x nach der Schleife ist
	}

}
