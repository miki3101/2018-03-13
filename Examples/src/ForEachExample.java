
public class ForEachExample
{
//	ForEach ist eine verk�rzte Z�hlschleife
	
//	ForEach verwendet man, wenn man Listen oder Arrays durchlaufen m�chte

//	ForEach Schleife setzt sich zusammen aus:
//	Datentyp (muss derselbe Datentyp sein, den das Element im Array hat), eine definierte Variable f�r das Element im Array und ein Array oder eine Liste
//	in der Schleife muss man definieren, was man jeden Schleifen Durchlauf machen m�chte
	
//	Syntax:	for(Datentyp Variable : Array)		jedes Feld im Array
//			{									was soll mit jedem Wert im Feld des Arrays gemacht werden
//			}

	

	public static void main(String[] args)
	{
		String[] namensliste = new String [] {"Susi", "Hugo", "Max"};	// erstellen eines neuen Array "namensliste" {Inhalt des Arrays}
		
		for (String name : namensliste)									// gehe jedes Element in der namensliste durch	
		{
			System.out.println(name);									// mach das; (schreib mir jeden Namen in die Console)
		}
	}

}
