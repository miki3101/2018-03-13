
public class Test�bungen
{
	// Erstellen Sie nun eine Methode greatestDividier die nach dem selben Prinzip
	// den Teiler einer Ganzzahl sucht
	// Die Ganzzahl wird als Parameter �bergeben und der gr��te Teiler als Ergebnis
	// zur�ck gegeben
	public static int greatestDividier(int zahl, int ergebnis)
	{

		for (int i = 2; i < zahl; i++) // gehe alle zahlen von 2 bis 41 durch
		{

			if (zahl % i == 0) // wenn eine der durchgegangenen zahlen ein Teiler von der Zahl (42) ist
			{
				ergebnis = i; // dann ist das neue ergebnis, diese Zahl
			}
		}

		return ergebnis;

	}

	// Schreiben Sie ein Programm, das den gr��ten Teiler (Modulo % ist in dem Fall
	// 0) einer Zahl sucht.
	// Suchen Sie zuerst in der main Methode den Teiler der Zahl 42
	public static void main(String[] args)
	{
		int zahl = 42; // gebe eine bestimmte Zahl an
		int ergebnis = 0; // definiere das Ergebnis

		for (int i = 2; i < zahl; i++) // gehe alle zahlen von 2 bis 41 durch
		{

			if (zahl % i == 0) // wenn eine der durchgegangenen zahlen ein Teiler von der Zahl (42) ist
			{
				ergebnis = i; // dann ist das neue ergebnis, diese Zahl
			}
		}
		System.out.println(ergebnis); // gib mir die Zahl aus, die du nach der for Schleife hast aus
									 // gr��ter Teiler

		int[] Array = new int[]		// ich initialisiere mein Array
		{ 16, 71, 123, 1024 };

		// Rufen Sie diese greatestDividier in der main Methode f�r die Zahlen 16, 71,
		// 123, und 1024 auf
		// und geben sie jeweils das Ergebnis auf der Console aus

		for (int i : Array)			//gehe jeden wert in diesem Array einmal durch
		{
			int u = greatestDividier(i, 0); // wende diese Methode bei jedem wert einmal an
			
			System.out.println(u);			// und gib ihn aus
		}

	}

}
