
public class SwitchExample
{
//	Switch ist eine Vergleichs Anweisung (a=b)
	
//  verwendet man, wenn man etwas miteinander vergleichen m�chte; Wert einer Variable "switch" wie definierter Wert "case";
//	Ergebnis ist ein boolean (true oder false)
//	ist nur mit den Datentypen "int" und "String" m�glich
	
//	Syntax: switch (Variable)	wenn (Hauptvergleichsobjekt)
//			{case Wert1:		Wert = Vergleichsobjekt 1
//								dann mach das
//			break;				um nicht weiter zu suchen
//			case Wert2:			Wert2 = Vergleichsobjekt 2
//								dann mach das
//			break;				um nicht weiter zu suchen
//			default:			wenn kein Vergleichsobjekt passt
//								dann mach das
//			break;}				beendet den Vergleich
	

	public static void main(String[] args)
	{
		int zahl1 = 14;
				
		switch (zahl1)										// wenn die (Variable);
		{
		case 10:											// der Wert "10" denselben Wert wie die Variable hat;
															// ( "..." muss denselben Datentyp haben wie die Variable; int/int)
			
			System.out.println("das ist richtig");			// dann mach das;
			
			break;											// muss man nach jedem Vergleich (case) schreiben;
															// damit die anderen cases nicht mehr �berpr�ft werden
			
			
		case 12:											// der Wert "12", denselben Wert wie die Variable hat
			
			System.out.println("das ist nicht richtig");	// dann mach das;
			
			break;											// um weitere Vergleiche zu verhindern
			
		default:											// wenn es mit keinem case �bereinstimmt
			
			System.out.println("so nicht");					// dann mach das;	
			break;
		}									
			

	}

}
