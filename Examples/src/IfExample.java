
public class IfExample
{
//	if ist eine Verzweigungs Anweisung
	
//	verwendet man, wenn ich etwas �berpr�fen will;
//	wenn das (...) ist, dann mach das, wenn nicht, dann mach etwas anderes;

//	Syntax: if(Bedingung)	wenn () richtig ist
//			{}				mach das
//			else			ansonsten
//			{}				mach das
	

	public static void main(String[] args)
	{
		int zahl1 = 10;
		int zahl2 = 12;
		
//		"if" ist immer boolean (richtig oder falsch)
		if(zahl1 == zahl2)												// wenn die Bedingung in der () true ist; 
		{
			System.out.println("Beide Zahlen sind gleich");				// dann mach das;
		}
		else															// wenn nicht; wenn es false ist
		{
			System.out.println("Die Zahlen stimmen nicht �berein");		// dann mach das;
		}
//		m�gliche Bedingungs Operatoren
//		==			vergleiche
//		< oder >	kleiner oder gr��er
//		&& 			und
//		||			oder
//		!			ist nicht

	}

}
