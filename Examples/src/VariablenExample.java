import java.util.ArrayList;

public class VariablenExample
{
	public static void main (String[] args)
	{
		int zahl = 123;							// Deklaration und Initialisierung einer Variable mit dem Typ Zahl 
		String wort = "Hallo";					// Deklaration und Initialisierung einer Variable mit dem Typ Zeichenkette
		double dezi = 0.5;						// Deklaration und Initialisierung einer Variable mit dem Typ Dezimalzahl
		boolean richtig = true;					// Deklaration und Initialisierung einer Variable mit dem Typ boolean (true oder false)
		char [] buchstabe = {'a','b','1','2'};	// Deklaration und Initialisierung einer Variable mit dem Typ Zeichen Arrays (Buchstaben oder Zahlen)
		int [] zahlenfeld = {1, 2, 3, 4};		// Deklaration und Initialisierung einer Variable mit dem Typ Zahlen Arrays
		String [] w�rterfeld = {"hallo", "Welt"};// Deklaration und Initialisierung einer Variable mit dem Typ Zeichenketten Arrays
		ArrayList<Integer> zahlen = new ArrayList<Integer>();// Deklaration einer Variable mit dem Typ Zahlen ArrayList
		zahlen.add (1);							// Initialisierung einer Varible mit dem Typ Zahlen ArrayList
		zahlen.add(2);
		ArrayList<String> w�rter = new ArrayList<>();	// Deklaration einer Variable mit dem Typ Zeichenketten ArrayList
		w�rter.add(wort);						// Initialisierung einer Variable mit dem Typ Zeichenketten Array
		w�rter.add("du");
		
		System.out.println(zahl);			// gibt den Wert der Variable zahl aus
		System.out.println(wort);			// gibt den Wert der Variable wort aus
		System.out.println(dezi);			// gibt den Wert der Variable dezi aus
		System.out.println(richtig);		// gibt den Wert der Variable richtig aus
		System.out.println(buchstabe);		// gibt den Wert der Variable buchstabe aus
		System.out.println(zahlenfeld[2]);	// gibt den Wert der Variable zahlenfeld an der Stelle 2 aus
		System.out.println(w�rterfeld[0]);	// gibt den Wert der Variable w�rterfeld an der Stelle 0 aus
		System.out.println(zahlen);			// gibt den Wert der Variable zahlen aus 
		System.out.println(w�rter);			// gibt den Wert der Variable w�rter aus	
	}
}
