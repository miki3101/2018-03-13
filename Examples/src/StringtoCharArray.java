
public class StringtoCharArray
{
	public static void main(String[] args)
	{
		String[] sArray = {"i", "love", "you"};
	    String s = "";
	    
	    for (String n:sArray)
	        s+= n;					// addiert den wert n zu der Variable s
	    
	    char[] c = s.toCharArray();	// f�gt den wert s zum Char Array hinzu
	    
	   for (char d : c)
	   {
		System.out.println(d);
	   }
	}
}
