
public class Array
{
	public static void main(String[] args)
	{
		String namen [] = {"Susi", "Max", "Hugo"};	// Deklaration und Initialisierung eines Array
		
		System.out.println(namen [1]);				// Ausgabe des Wertes an der Stelle 1
		
		
		for (int i =0; i< namen.length; i++)		
		{
			System.out.println(namen[i]);			// Ausgabe der Stellen des Array
		}
		
		for (String wort : namen)				
		{
			System.out.println(wort);				// Ausgabe jedes Wertes des Array
		}
	}
}
