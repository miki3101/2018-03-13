
public class MathApp
{

	public static void main(String[] args)
	{
		int zahl = 6223;
		int ergebnis = 0;
		int nummer = 2;

		ergebnis = summe(zahl);
		System.out.println(ergebnis);							// Quersumme der zahlen
		ergebnis = quer(zahl);
//		System.out.println(ergebnis);							// Quersumme der Zahlen

		int[] zahlen = new int[stellen(zahl)];

//		System.out.println(zahlen.length);						// Anzahl der Zahlen im Feld

		fuellen(zahl, zahlen);									// bef�llen des Array
		inhalt(zahlen);											// zahlen im array
		
		System.out.println(mittelwert(zahl, zahlen));			// erechnet den Mittelwert
		
		System.out.println(groessteZahl(zahlen));				// ermittelt die gr��te Zahl
		System.out.println(kleinsteZahl(zahlen));				// ermittelt die kleinste Zahl
		wieOft(zahlen, nummer);									// wie oft eine zahl vorkommt
	}

	public static int stellen(int zahl)
	{
		int stellen = 0;

		for (int i = zahl; i > 0; i = i / 10)
		{
			stellen++;
		}

		return stellen;
	}

	public static int summe(int zahl)
	{
		int ergebnis = 0;

		for (int i = zahl; i > 0; i = i / 10)
		{
			int rest = i % 10;

			ergebnis = ergebnis + rest;

//			System.out.println(rest);

		}
		return ergebnis;
	}

	// oder
	public static int quer(int zahl)
	{
		int ergebnis = 0;

		if (zahl < 10)
		{
			return zahl;
		}
		ergebnis = zahl % 10 + quer(zahl / 10);

		return ergebnis;
	}

	public static int[] fuellen(int zahl, int[] zahlen)
	{
		int z = 0;

		if (z < zahlen.length)
		{
			for (int i = zahl; i > 0; i = i / 10)
			{
				int rest = i % 10;

				zahlen[z] = rest;

				z++;
			}
		}
		return zahlen;
	}

	public static void inhalt(int[] zahlen)
	{
		for (int wert : zahlen)
		{
			System.out.println("[" + wert + "] ");
		}

	}
	
	public static double mittelwert (int zahl, int []zahlen)
	{
		
		double summe = 0;
		
		for (int wert : zahlen)
		{		
			summe = summe + wert;
		}
			
		return summe/zahlen.length;	
	}
	
	public static int groessteZahl (int []zahlen)
	{
		
		int g = 0;
		
		for (int i : zahlen)
		{	
			if (i>g)
			{
				g = i;
			}
		}
		return g;	
	}
	
	public static int kleinsteZahl (int []zahlen)
	{
		int k = zahlen [0];
		
		for (int i = 0; i<=zahlen.length; i++)
		{
			if (i<k)
			{
				i = k;
			}
		}
	
		return k;
	}
	
	public static void wieOft (int []zahlen, int nummer)
	{
		int zaehler = 0;
		
		for (int i : zahlen)
		{
			
			if(nummer == i)
			{
				zaehler++;
				
			}
		}	
		System.out.println("Die Zahl " + nummer + " ist " + zaehler + " mal vorgekommen");
	}

}
