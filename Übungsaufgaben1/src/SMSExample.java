
public class SMSExample
{

	public static void main(String[] args)
	{
		String zk1 = "Hansi75";
		String zk2 = "Susi902";
		String zk3 = "!Max!";

		char[] b1 = umwandeln(zk1);
		char[] b2 = umwandeln(zk2);
		char[] b3 = umwandeln(zk3);
		
		ausgeben(b1);
		ausgeben(b2);
		ausgeben(b3);
		
		System.out.println();
		
		zahlen(b1);
		zahlen(b2);
		zahlen(b3);
		
		ausgeben(b1);
		ausgeben(b2);
		ausgeben(b3);		
	}

	public static char[] umwandeln(String zk)
	{
		char[] buchstaben = zk.toCharArray();
		return buchstaben;
	}

	public static void ausgeben(char[] b)
	{
		for (char c : b)
		{
			System.out.print("[" + c + "]");
		}
		System.out.println();
	}

	public static char [] zahlen(char[] b)
	{	
		for (int c = 0;c<b.length;c++)
		{
			switch (b[c])
			{
			case '1':
				b[c] = '1';
			case 'a':
			case 'A':
			case 'b':
			case 'B':
			case 'c':
			case 'C':
			case '2':
				b[c] = '2';
				break;
			case 'd':
			case 'D':
			case 'e':
			case 'E':
			case 'f':
			case 'F':
			case '3':
				b[c] = '3';
				break;
			case 'g':
			case 'G':
			case 'h':
			case 'H':
			case 'i':
			case 'I':
			case '4':
				b[c] = '4';
				break;
			case 'j':
			case 'J':
			case 'k':
			case 'K':
			case 'l':
			case 'L':
			case '5':
				b[c] = '5';
				break;
			case 'm':
			case 'M':
			case 'n':
			case 'N':
			case 'o':
			case 'O':
			case '6':
				b[c] = '6';
				break;
			case 'p':
			case 'P':
			case 'q':
			case 'Q':
			case 'r':
			case 'R':
			case 's':
			case 'S':
			case '7':
				b[c] = '7';
				break;
			case 't':
			case 'T':
			case 'u':
			case 'U':
			case 'v':
			case 'V':
			case '8':
				b[c] = '8';
				break;
			case 'w':
			case 'W':
			case 'x':
			case 'X':
			case 'y':
			case 'Y':
			case 'z':
			case 'Z':
			case '9':
				b[c] = '9';
				break;
			case '0':
				b[c] = '0';
			default:
				b[c] = '_';
				break;
			}
		} return b;
	}

}
