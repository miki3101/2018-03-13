
public class DividierApp
{
	public static void main(String[] args)
	{
		int zahl = 42;
		double ergebnis = 0;
		
		for(double i = 0; i<zahl; i++)
		{
			if(zahl%i == 0)
			{
				ergebnis = i;
			}								
		}
		System.out.println(ergebnis);	
		
		System.out.println(greatestDividier(16));
		System.out.println(greatestDividier(71));
		System.out.println(greatestDividier(123));
		System.out.println(greatestDividier(1024));
	}
	
	public static double greatestDividier(int zahl)
	{
		double ergebnis = 0;
		
		for (double i = 0; i<zahl; i++)
		{
			if (zahl%i == 0)
			{
				ergebnis = i;
			}
		}
		return ergebnis;
	}
}
