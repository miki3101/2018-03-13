import Personen.Person;

public class PersonenApp
{

	public static void main(String[] args)
	{
		Person p1 = new Person("Luke","Skywalker");
		System.out.println(p1);
		
		Person p2 = new Person("Anakin","Skywalker");
		System.out.println(p2);
		
		Person p3 = new Person("Padme","Amidala");		
		Person p4 = new Person("Obiwan", "Kenobi");
	
		Person p5 = new Person("Mama","Amidala");	
		
		p3.setMutter(p5);
		p1.setVater(p2); //"Luke ich bin dein Vater *chh chhrh chh*"
		p1.setMutter(p3);
		
		
		System.out.println(p1);
		System.out.println(p4);
		
		Math.random(); // statische Methode der Klasse Math.
		
		// System.out  // statische öffentliche membervariable von System
		
		System.out.println(p1.getVater());
		
	}

}

