package Personen;

import java.util.ArrayList;

public class Person
{
	private String vorname;
	private String nachname;
	private Person mutter;
	private Person vater;
	
	private ArrayList<Person> kinder = new ArrayList<>();

	private static int anzahlPersonen;

	public Person(String vorname, String nachname)
	{
		this.vorname = vorname;
		this.nachname = nachname;
		anzahlPersonen++;
	}
	public void addKind(Person kind)
	{
		kinder.add(kind);
	}
	public void setVater(Person vater)
	{
		this.vater = vater;
		vater.addKind(this);
	}

	public void setMutter(Person mutter)
	{
		this.mutter = mutter;
		mutter.addKind(this);
	}
	
	public Person getVater()
	{
		return vater;
	}

	public String toString()
	{
		String kindName = "[";
		
		for (Person person : kinder)
		{
			kindName += person.vorname + " " + person.nachname;
		}
		
		kindName += "]";
		
		if(mutter == null && vater == null) {
			return String.format("(%s %s (? ?)) kinder %s %d", vorname, nachname, kindName, anzahlPersonen);
		}
		
		if(mutter == null)
		{
			return String.format("(%s %s (? %s)) kinder %s %d", vorname, nachname, vater, kindName, anzahlPersonen);
		}
		
		if(vater == null)
		{
			return String.format("(%s %s (%s ?)) kinder %s %d", vorname, nachname, mutter, kindName, anzahlPersonen);
		}
		
		return String.format("(%s %s (%s %s)) kinder %s %d", vorname, nachname, mutter, vater, kindName, anzahlPersonen);
	}

}

