package com.appradar.searches;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.http.ResponseEntity;

public class JsonResults
{
	private String query;
	private String country;
	private int count;
	private Date time = new Date();
	private ArrayList<JsonResult> results;
	private ResponseEntity<JsonResults> res;
	
	public JsonResults()
	{
		this.results = new ArrayList<JsonResult>();
	}

	public String getQuery()
	{
		return query;
	}
	
	public void setQuery(String query)
	{
		this.query = query;
	}
	
	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public String getCountry()
	{
		return country;
	}
	
	public void setCountry(String country)
	{
		this.country = country;
	}
	

	public ArrayList<JsonResult> getResults()
	{
		return results;
	}

	public void setResults(ArrayList<JsonResult> results)
	{
		this.results = results;
	}

	public void addJsonNode(JsonResult x)
	{
		results.add(x);
	}

	public ResponseEntity<JsonResults> getRes()
	{
		return res;
	}

	public void setRes(ResponseEntity<JsonResults> res)
	{
		this.res = res;
	}

	public Date getTime()
	{
		return time;
	}

	public void setTime(Date time)
	{
		this.time = time;
	}

	
	
	
	
	
}


