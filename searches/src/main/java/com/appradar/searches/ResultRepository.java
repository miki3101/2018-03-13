package com.appradar.searches;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ResultRepository extends MongoRepository<Result, String>
{

	public Result findByResultsTitel(String titel);
	public Result findByQuery(String q);
	public Result findByCountry(String country);
	public Result findByLang(String lang);
	public Result findByQueryAndCountryAndLang(String q, String c, String l);

	
}
