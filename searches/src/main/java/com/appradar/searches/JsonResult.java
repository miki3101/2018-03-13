package com.appradar.searches;

import com.fasterxml.jackson.databind.JsonNode;

public class JsonResult
{
	
	private JsonNode titel;
	private JsonNode developer;
	private JsonNode iconUrl;
	private JsonNode discription;

	
	public JsonNode getTitel()
	{
		return titel;
	}

	public void setTitel(JsonNode titel)
	{
		this.titel = titel;
	}

	public JsonNode getDeveloper()
	{
		return developer;
	}

	public void setDeveloper(JsonNode developer)
	{
		this.developer = developer;
	}

	public JsonNode getIconUrl()
	{
		return iconUrl;
	}

	public void setIconUrl(JsonNode iconUrl)
	{
		this.iconUrl = iconUrl;
	}

	public JsonNode getDiscription()
	{
		return discription;
	}

	public void setDiscription(JsonNode discription)
	{
		this.discription = discription;
	}

	
	
	
}
