package com.appradar.searches;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class Hello
{

	@RequestMapping(path = "/hello")
	public String index(@RequestParam String name)
	{
		return "Hello" + " " + name;
	}

	@RequestMapping(path = "/hello2")
	public String index2(@RequestParam String name)
	{
		return "Wie geht es dir" + " " + name;
	}

	@RequestMapping(path = "/test")
	public Test testen(@RequestParam String name, String country)
	{
		Test test1 = new Test(name, country);

		return test1;
	}

	@RequestMapping(path = "/search")
	public ResponseEntity<JsonResults> test(@RequestParam String q, String country, String lang)
	{
		JsonResults list = new JsonResults();
		JsonNode x;
		JsonNode y = null;
		URL u;
		URLConnection con = null;
		ObjectMapper mapper = new ObjectMapper();
		HttpStatus status = HttpStatus.BAD_REQUEST;

		try
		{
			u = new URL("https://itunes.apple.com/search?term=" + q + "&country=" + country + "&entity=software&lang="
					+ lang);
			if (lang == null)
			{
				u = new URL("https://itunes.apple.com/search?term=" + q + "&country=" + country
						+ "&entity=software&lang=en");

			} else
			{
				u = new URL("https://itunes.apple.com/search?term=" + q + "&country=" + country
						+ "&entity=software&lang=" + lang);
			}
			con = u.openConnection();

		} catch (IOException e1)
		{
			e1.printStackTrace();
		}

		if (country.length() != 2)
		{
			ResponseEntity<JsonResults> res = new ResponseEntity<>(status);

			return res;
		}

		try (InputStream in = con.getInputStream();)
		{

			x = mapper.readTree(in);
			y = x.get("results");

			Iterator<JsonNode> it = y.elements();

			while (it.hasNext())
			{

				JsonResult result3 = new JsonResult();
				JsonNode temp = it.next();
				result3.setTitel(temp.get("trackName"));
				result3.setDeveloper(temp.get("artistName"));
				result3.setIconUrl(temp.get("artworkUrl512"));
				result3.setDiscription(temp.get("description"));
				list.addJsonNode(result3);
			}

			list.setQuery(q);
			list.setCountry(country);
			list.setCount(x.get("resultCount").intValue());

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return ResponseEntity.ok(list);
	}

	HashMap<String, JsonResults> hm = new HashMap<>();
	ObjectMapper mapper = new ObjectMapper();

	@RequestMapping(path = "/search", method = RequestMethod.POST)
	public ResponseEntity<JsonResults> test2(@RequestBody Incoming incoming)
	{
		JsonResults list = new JsonResults();
		JsonNode x;
		JsonNode y = null;
		URL u;
		URLConnection con = null;
		HttpStatus status = HttpStatus.BAD_REQUEST;
		String query = "";
		String country = "";
		String lang = "";
		Date date = new Date();

		try
		{
			query = incoming.getQuery();
			country = incoming.getCountry();
			lang = incoming.getLanguage();

			System.out.println(hm.containsKey(query + country + lang));

			if (hm.containsKey(query + country + lang))
			{
				list = hm.get(query + country + lang);

				if ((date.getTime() - list.getTime().getTime()) < 10000)
				{
					System.out.println(date.getTime() - list.getTime().getTime());
					System.out.println("Cache");
					return ResponseEntity.ok(list);
				}
			}

			u = new URL("https://itunes.apple.com/search?term=" + query + "&country=" + country
					+ "&entity=software&lang=" + lang);
			if (lang == null)
			{
				u = new URL("https://itunes.apple.com/search?term=" + query + "&country=" + country
						+ "&entity=software&lang=en");

			} else
			{
				u = new URL("https://itunes.apple.com/search?term=" + query + "&country=" + country
						+ "&entity=software&lang=" + lang);
			}
			con = u.openConnection();

		} catch (IOException e1)
		{
			e1.printStackTrace();
		}

		if (country.length() != 2)
		{
			ResponseEntity<JsonResults> res = new ResponseEntity<>(status);

			return res;
		}

		try (InputStream in = con.getInputStream();)
		{
			x = mapper.readTree(in);
			y = x.get("results");

			Iterator<JsonNode> it = y.elements();

			while (it.hasNext())
			{
				JsonResult result3 = new JsonResult();
				JsonNode temp = it.next();
				result3.setTitel(temp.get("trackName"));
				result3.setDeveloper(temp.get("artistName"));
				result3.setIconUrl(temp.get("artworkUrl512"));
				result3.setDiscription(temp.get("description"));
				list.addJsonNode(result3);
			}

			list.setQuery(query);
			list.setCountry(country);
			list.setCount(x.get("resultCount").intValue());

			Date t = new Date();
			list.setTime(t);

			hm.put(query + country + lang, list);
			System.out.println("No Cache");

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return ResponseEntity.ok(list);
	}

}
