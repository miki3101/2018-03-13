package com.appradar.searches;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.Id;

public class Result
{
	private String query;
	private String country;
	@Id
	private String id;
	private int number;
	private int count;
	private String lang;
	private int update;
	private Date time = new Date();
	private ArrayList<Daten> results;
	
	public Result()
	{
		this.results = new ArrayList<>();
	}

	public String getQuery()
	{
		return query;
	}

	public void setQuery(String query)
	{
		this.query = query;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public ArrayList<Daten> getResults()
	{
		return results;
	}

	public void setResults(ArrayList<Daten> results)
	{
		this.results = results;
	}
	
	public void addDaten(Daten x)
	{
		results.add(x);
	}

	public String getLang()
	{
		return lang;
	}

	public void setLang(String lang)
	{
		this.lang = lang;
	}
	
	public Date getTime()
	{
		return time;
	}

	public void setTime(Date time)
	{
		this.time = time;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public int getNumber()
	{
		return number;
	}

	public void setNumber(int number)
	{
		this.number = number;
	}

	public int getUpdate()
	{
		return update;
	}

	public void setUpdate(int update)
	{
		this.update = update;
	}
	
	
	
	
}
