package com.appradar.searches;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpRequestExecutor;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.hibernate.validator.internal.util.privilegedactions.GetMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.model.UpdateOptions;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@RestController
public class StarterApp
{

	@RequestMapping(path = "/searches")
	public Result test(@RequestParam String q, String country, String lang)
	{

		Result result = new Result();
		Result dbResult = new Result();
		JsonNode input;
		JsonNode node = null;
		ObjectMapper mapper = new ObjectMapper();
		URL u = null;
		Date date = new Date();

		MongoClientURI uri = new MongoClientURI("mongodb://michaela:123456@mongo03-staging/search");

		try (MongoClient mongo = new MongoClient(uri);)
		{
			CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
					fromProviders(PojoCodecProvider.builder().automatic(true).build()));

			MongoDatabase database = mongo.getDatabase("search").withCodecRegistry(pojoCodecRegistry);
			MongoCollection<Result> collection = database.getCollection("Incoming", Result.class);

			FindIterable<Result> dbResults = collection
					.find(and(eq("query", q), eq("country", country), eq("lang", lang)));

			for (Result result2 : dbResults)
			{
				dbResult = result2;
			}

			if (dbResult.getQuery() != null)
			{
				if ((date.getTime() - dbResult.getTime().getTime()) < 10000)
				{
					System.out.println("Daten für " + q + " aus DB");
					for (Result result2 : dbResults)
					{
						dbResult = result2;
					}
					return dbResult;
				}
			}

			try
			{
				u = new URL("https://itunes.apple.com/search?term=" + q + "&country=" + country
						+ "&entity=software&lang=" + lang);
				if (lang == null)
				{
					u = new URL("https://itunes.apple.com/search?term=" + q + "&country=" + country
							+ "&entity=software&lang=en");
				}

			} catch (MalformedURLException e1)
			{
				e1.printStackTrace();
			}

			try (InputStreamReader br = new InputStreamReader(u.openStream());)
			{

				input = mapper.readTree(br);
				node = input.get("results");

				Iterator<JsonNode> it = node.elements();

				while (it.hasNext())
				{
					Daten daten = new Daten();
					JsonNode temp = it.next();
					daten.setTitel(temp.get("trackName").asText());
					daten.setDeveloper(temp.get("artistName").asText());
					daten.setIconUrl(temp.get("artworkUrl512").asText());
					daten.setDescription(temp.get("description").asText());
					result.addDaten(daten);

				}

				result.setQuery(q);
				result.setCountry(country);
				result.setCount(input.get("resultCount").intValue());
				result.setLang(lang);

				collection.replaceOne(and(eq("query", q), eq("country", country), eq("lang", lang)), result,
						new UpdateOptions().upsert(true));

				System.out.println("Daten für " + q + " von ITunes");

			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return result;
	}

	@Autowired
	private ResultRepository repository;
	private int counter;
	private int updated;


	@RequestMapping(path = "/searchesSpring")
	public Result test2(@RequestParam String q, String country, String lang)
	{

		Result result = new Result();
		Result dbResult = new Result();
		Date date = new Date();
		JsonNode input;
		JsonNode node = null;
		ObjectMapper mapper = new ObjectMapper();
		
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = null;
		

		dbResult = repository.findByQueryAndCountryAndLang(q, country, lang);

		if (dbResult != null)
		{
			counter = dbResult.getNumber();
			counter++;
			updated = dbResult.getUpdate();
			result.setUpdate(updated);
			dbResult.setNumber(counter);
			result.setNumber(counter);

			if ((date.getTime() - dbResult.getTime().getTime()) < 50000)
			{
				System.out.println("Daten für " + q + " aus DB");
				repository.save(dbResult);
				return dbResult;
			} else
			{
				result.setId(dbResult.getId());
				result.setUpdate(dbResult.getUpdate());
			}

		}

		HttpGet request = new HttpGet(
				"https://itunes.apple.com/search?term=" + q + "&country=" + country + "&entity=software&lang=" + lang);

		if (lang == null)
		{
			request = new HttpGet(
					"https://itunes.apple.com/search?term=" + q + "&country=" + country + "&entity=software&lang=en");
		}
		
		try
		{
			response = client.execute(request);
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
		try (InputStreamReader br = new InputStreamReader(response.getEntity().getContent());)
		{

			input = mapper.readTree(br);
			node = input.get("results");

			Iterator<JsonNode> it = node.elements();

			while (it.hasNext())
			{
				Daten daten = new Daten();
				JsonNode temp = it.next();
				daten.setTitel(temp.get("trackName").asText());
				daten.setDeveloper(temp.get("artistName").asText());
				daten.setIconUrl(temp.get("artworkUrl512").asText());
				daten.setDescription(temp.get("description").asText());
				daten.setTrackID(temp.get("trackId").asText());
				result.addDaten(daten);

			}

			result.setQuery(q);
			result.setCountry(country);
			result.setCount(input.get("resultCount").intValue());
			result.setLang(lang);

			repository.save(result);

			System.out.println("Daten für " + q + " von ITunes");

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return result;
	}
	
	
	@Async
	@Scheduled(fixedDelay = 30000)
	public void dbSync()
	{

		List<Result> results = repository.findAll();

		for (int i = 0; i < results.size(); i++)
		{
			Result result = results.get(i);

			updated = result.getUpdate();
			updated++;
			result.setUpdate(updated);
			System.out.println("Query: " + result.getQuery());
			System.out.println("Aufrufe: " + result.getNumber());
			System.out.println("Updates: " + result.getUpdate());
			System.out.println("----------");

			JsonNode input;
			JsonNode node = null;
			ObjectMapper mapper = new ObjectMapper();
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response = null;
			Date date = new Date();

			result.setTime(date);

			if (result.getNumber() > 5)
			{

				HttpGet request = new HttpGet(
						"https://itunes.apple.com/search?term=" + result.getQuery() + "&country=" + result.getCountry() + "&entity=software&lang=" + result.getLang());
	
				try
				{
					response = client.execute(request);
				} catch (IOException e1)
				{
					e1.printStackTrace();
				}
				try (InputStreamReader br = new InputStreamReader(response.getEntity().getContent());)
				{

					input = mapper.readTree(br);
					node = input.get("results");

					Iterator<JsonNode> it = node.elements();

					while (it.hasNext())
					{
						Daten daten = new Daten();
						JsonNode temp = it.next();
						daten.setTitel(temp.get("trackName").asText());
						daten.setDeveloper(temp.get("artistName").asText());
						daten.setIconUrl(temp.get("artworkUrl512").asText());
						daten.setDescription(temp.get("description").asText());
						daten.setTrackID(temp.get("trackId").asText());
						result.addDaten(daten);

					}

					result.setCount(input.get("resultCount").intValue());

					repository.save(result);

				} catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}
		}
	}

}
