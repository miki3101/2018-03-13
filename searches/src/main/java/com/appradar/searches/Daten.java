package com.appradar.searches;

public class Daten
{
	private String titel;
	private String developer;
	private String iconUrl;
	private String description;
	private String trackID;
	
	
	public String getTitel()
	{
		return titel;
	}
	public void setTitel(String titel)
	{
		this.titel = titel;
	}
	public String getDeveloper()
	{
		return developer;
	}
	public void setDeveloper(String developer)
	{
		this.developer = developer;
	}
	public String getIconUrl()
	{
		return iconUrl;
	}
	public void setIconUrl(String iconUrl)
	{
		this.iconUrl = iconUrl;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getTrackID()
	{
		return trackID;
	}
	public void setTrackID(String trackID)
	{
		this.trackID = trackID;
	}
	
	
	
	
}

